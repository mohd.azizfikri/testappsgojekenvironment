---
title: Adding Maven and Dependencies
weight: 2
---

### Add Maven in Gradle Project.

In order to be able to download [Identity Protection SDK](https://source.golabs.io/mobile/identity-protection-android-sdk) dependencies from Gojek Artifactory, you need to
add `http://artifactory-gojek.golabs.io/artifactory/gojek-jars` in `build.gradle.kts`.

{{< hint info>}}
**Make sure your computer are connected to VPN integration**\
If you doesn't have the profile yet, please ask your team lead and find the VPN profile from [Gate Gojek](http://gate.gojek.co.id/)
{{< /hint >}}

**Example**
```kotlin
buildscript {
    repositories {
        maven { setUrl("http://artifactory-gojek.golabs.io/artifactory/gojek-jars") }
    }

    dependencies {
        ...
    }
}
```

### Add Dependencies in Module

In your `build.gradle.kts` *this can be your app or your library module*, add necessary dependencies that
you need in order to use the Identity Protection SDK properly.   

##### Release SDK ⛑️

```kotlin
// Identity Protection Release SDK
implementation "com.gojek.android:identity-protection:$version"
```

##### Debug SDK ⛑️

```kotlin
// Identity Protection Debug SDK
implementation "com.gojek.android:identity-protection-dev:$version"
```

##### NO-OP SDK ⛑️

```kotlin
// Identity Protection NO-OP SDK
implementation "com.gojek.android:identity-protection-no-op:$version"

