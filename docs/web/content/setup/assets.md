---
title: What's Provided
weight: 1
---

You should have the following files ready before you start the integration.

* crypto_ta.bin
* firmware
* manifest_<FUID>.json
* profile
* sig_cfs
* signature.vky 
* vkeylicensepack 
* voscodesign.vky 

Please refer to [SOP for integration with new application.](https://go-jek.atlassian.net/wiki/spaces/DEVX/pages/1949829736/SOP+Integrating+new+app+with+IdentityProtection+SDK)

{{< hint info>}}
**Note:**\
*profile* and *sig_cfs* are not included in the assets package but they are required in the *assets/* folder of your project. 
It can be generated from V-OS App Protection Server. 
If you do not have the V-OS App Protection Server, please reach out to **@mobile-security-devs**.
 
*sig_cfs* is only required if you are using the V-OS Threat Intelligence feature and when the V-Key Servers version is 4.8 and above.
{{< /hint >}}
