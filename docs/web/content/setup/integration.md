---
title: Integration
weight: 3
---

### Do the following steps to integrate Identity Protection SDK into your application.

{{< hint info>}}
**Note:**\
If you are using third-party SDKs, such as FirebaseApp, that can only be initialized in the main process, 
make sure that you have implemented logic to confirm the current process is the main process before initializing the third-party app. 
See [Firebase App Issue Workaround](/appendix/FirebaseAppIssueWorkaround/) for more details.
{{< /hint >}}

1. Copy the following [asset files](/setup/assets/) to the **app/src/main/assets/** folder of your app project.
All the assets are based on your app flavor. So, you have to add the following assets in the right directory.
You can see the example assets in the [here](https://source.golabs.io/mobile/identity-protection-android-sdk/-/wikis/FAQ/Where-to-store-the-assets)

2. Create a subclass of application
    ```kotlin
    class HostApp : Application() {
    
        override fun onCreate() {
            super.onCreate()
        }
    }
    ```

3. In **onCreate()** of custom application, call [IdentityProtection](https://source.golabs.io/mobile/identity-protection-android-sdk/-/blob/master/identity-protection-sdk-dev/src/main/kotlin/identity/protection/model/IdentityProtection.kt)
    ```kotlin
    IdentityProtection.config = IdentityProtection.config.copy(
        isEnabled = true,
        isDebuggable = true,
        strictMode = true,
        virtualSpaceDetection = true,
        timeOutInMillis = 10_000L,
        criticalThreats = emptyList(),
        cryptoSignatureModel = IdentityProtection.CryptoSignatureModel(
            context = this,
            storeKey = "Gojek_CA_RSA_v1_20201116",
            hashType = HashType.SHA256
        ),
        onProtectionAnalyzerListener = object : OnProtectionAnalyzerListener {
            override fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
                /** NO-OP **/
            }
        }
    )
    ```
    For ***criticalThreats***, you can find out the contract of the threat type [here](https://go-jek.atlassian.net/wiki/spaces/DEVX/pages/1952483073/App+Protection#Threat-Categories) 
    and the APIs provided by IdentityProtection SDK [here](/api)

4. You need to disable Auto Backup feature by setting **allowBackup** to false in Android Manifest file.
Allowing auto backup will cause the [Identity Protection SDK](https://source.golabs.io/mobile/identity-protection-android-sdk) 
related files to be backed up to the cloud and will result in issues while the users try to use the app again after an app reinstallation.

{{< hint info >}}
> **Pro tips**

See example of Identity Protection SDK configuration in the **Application** class from the [demo app](https://source.golabs.io/mobile/identity-protection-android-sdk/-/blob/master/identity-protection-demo-consumer-app/src/main/kotlin/com/gojek/app/HostApp.kt). 
{{< /hint >}}
