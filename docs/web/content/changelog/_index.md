---
title: Change Log
weight: 4
---

Please see the change log on the [releases](https://source.golabs.io/mobile/identity-protection-android-sdk/-/releases)