---
title: Firebase App Issue Workaround
weight: 1
---

Based on the result from the internal investigations, it has been confirmed that FirebaseApp will cause app crashes 
if it is initialized in the application's **onCreate()** method while running in an isolated process. 
The app crashes are likely caused by lacking permission in the isolated process. 
This finding is also backed by the Firebase official documentation which requires the FirebaseApp initialization to occur only in the main process of the app.

V-OS App Protection uses the isolated process feature from Android for some of its protection mechanisms. 
In Android, when an isolated process is created, it proceeded to create an app process that will invoke the application's **onCreate()** method. 
Different from a normal app process, an isolated process will not have any permission attached. Therefore, 
FirebaseApp cannot acquire the permissions it needs which eventually leads to an app crash.

To prevent such an issue, the best approach is to implement a logic to check if the current app process is an 
isolated process and only initialize the FirebaseApp (or other third-party apps) when the process is a normal app process. 
You can refer to the following snippet for implementing such logic:

{{< hint info>}}
**Note:**\
A Similar approach should be implemented for any third-party SDKs also.
{{< /hint >}}

```kotlin
...
private var firebaseApp: FirebaseApp? = null
...

override fun onCreate() {
    super.onCreate()

    if (isMainProcess()) {
        if (firebaseApp == null) {
            firebaseApp = FirebaseApp.initializeApp(getApplicationContext());
        }
    }
}

private fun isMainProcess(context: Context): Boolean {
    return try {
        val manager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        val info = manager?.runningAppProcesses?.first { it.pid == Process.myPid() }

        context.packageName == info?.processName
    } catch (e: Exception) {
        false
    }
}
```