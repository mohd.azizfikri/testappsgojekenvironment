---
title: Dependencies
weight: 2
---

The [Identity Protection SDK](https://source.golabs.io/mobile/identity-protection-android-sdk) is available in several artifacts.
* [Release SDK](https://mobile.pages.golabs.io/identity-protection-android-sdk/api/identity-protection-sdk/identity-protection-sdk/index.html).
A module that using V-OS Release SDK for release variant.
* [Debug SDK](https://mobile.pages.golabs.io/identity-protection-android-sdk/api/identity-protection-sdk-dev/identity-protection-sdk-dev/index.html).
A module that using V-OS Debug SDK for development purpose.
* [NO-OP SDK](https://mobile.pages.golabs.io/identity-protection-android-sdk/api/identity-protection-no-op-sdk/identity-protection-no-op-sdk/index.html).
A module that has no operation (DO NOTHING).

#### Latest Version

Latest version and change log can be found [Identity Protection SDK Change Log](https://source.golabs.io/mobile/identity-protection-android-sdk/-/releases)

```kotlin
val version = x.y.z
```

##### Release SDK ⛑️

```kotlin
// Identity Protection Release SDK
implementation "com.gojek.android:identity-protection:$version"
```

##### Debug SDK ⛑️

```kotlin
// Identity Protection Debug SDK
implementation "com.gojek.android:identity-protection-dev:$version"
```

##### NO-OP SDK ⛑️

```kotlin
// Identity Protection NO-OP SDK
implementation "com.gojek.android:identity-protection-no-op:$version"
```

{{< hint info >}}
> **Pro tips**

See example of integrated Identity Protection SDK in the [demo app](https://source.golabs.io/mobile/identity-protection-android-sdk/-/tree/master/identity-protection-demo-consumer-app). 
{{< /hint >}}