---
title: Overview
weight: 0
---

[Identity Protection SDK](https://source.golabs.io/mobile/identity-protection-android-sdk) has knowledge of the Android Lifecycle 
([BlackBox Lifecycle](https://source.golabs.io/mobile/identity-protection-android-sdk/-/blob/master/identity-protection-sdk/src/main/kotlin/identity/protection/internal/BlackBoxLifecycle.kt)).
This ability is necessary for V-OS App Protection to finish the activity safely.


**Example App** can be found here 
[demo-app](https://source.golabs.io/mobile/identity-protection-android-sdk/-/tree/master/identity-protection-demo-consumer-app) 
and below is a screenshots of the demo app.

|   Threat Detected   |   Profile Updated
|:--------------:|:-----------:|
| <img src="/images/Threat_Detected.png" alt="Threat Detected" width="350"/>  | <img src="/images/Profile_Updated.png" alt="Profile Updated" width="350"/>
| The threat dialog will be shown when the V-OS App Protection found the threats | The profile update dialog will be shown if there's an update |

[Get started!](/gettingstarted/dependency/)
