---
title: Identity Protection SDK
---

{{< hint info >}}
[Identity Protection SDK](https://source.golabs.io/mobile/identity-protection-android-sdk) is a wrapper to encapsulate and provide a high-level implementation of all V-Key SDK. 
{{< /hint >}}

## Introduction
The SDK was created as a versatile solution to ensure both security and convenience to end-users. 
Built on top of V-Key’s patented V-OS and V-OS Application Protection as the security foundation, 
the solution is modular, enabling the mobile app to be threat-aware and self-defending, 
built with in-depth multi-layered protection against advanced threats that target the app, 
critical data, and identity theft.

### Purpose
Overview of the Functional description of the Identity Protection SDK 
that will be used to implement the App Protection, Signature Verification and Strong Identifier to Gojek Mobile Apps.

### Project Scope
The Scope of the Project is to implement Identity Protection SDK to Gojek Mobile App as a client-side solution. 
The functionalities of the features can be found [here](https://go-jek.atlassian.net/wiki/spaces/DEVX/pages/1952745134/Features)
