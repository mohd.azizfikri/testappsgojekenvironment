## What is Identity-protection-android-sdk?
A pragmatic lightweight configurations framework for application integrity protection.

Written in pure Kotlin

### Getting Help 🚒

# Documentation: 📚
* [WIKI](https://source.golabs.io/mobile/identity-protection-android-sdk/-/wikis/home)

Any question about Identity Protection Android SDK usage? 
- Come talk on slack [#devx-mobile-security](https://gojek.slack.com/messages/devx-mobile-security/)

# Open Issue 🔔
Open Issue or Request from [Issue Template](https://source.golabs.io/mobile/identity-protection-android-sdk/-/issues/new)
and choose a template.

# Local Development Scripts 📃
For contribute to development please refer to [Contributing](https://source.golabs.io/mobile/identity-protection-android-sdk/-/wikis/Contributing) page 

# FAQ ❓
For list of FAQs please refer to [FAQ Section](https://source.golabs.io/mobile/identity-protection-android-sdk/-/wikis/home) page

License ✅
--------
    Copyright 2020 GOJEK

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
