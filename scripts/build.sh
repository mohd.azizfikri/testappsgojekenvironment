#!/bin/sh

echo "Running assemble debug..."

./gradlew clean :identity-protection-demo-consumer-app:assembleDebug \
:identity-protection-sdk:assembleDebug \
:identity-protection-no-op-sdk:assembleDebug \
--parallel --daemon

status=$?
if [ "$status" = 0 ] ; then
    echo "Assemble debug found no problems."
    exit 0
else
    echo 1>&2 "Assemble debug violations! Fix them before pushing your code!"
    exit 1
fi
