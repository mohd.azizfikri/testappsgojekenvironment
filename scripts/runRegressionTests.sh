#!/bin/sh

echo "Running Regression Tests..."

./gradlew clean \
:identity-protection-demo-consumer-app:testDebugUnitTest \
:identity-protection-demo-consumer-app:cAT \
:identity-protection-sdk:testDebugUnitTest \
:identity-protection-no-op-sdk:testDebugUnitTest \
--parallel --daemon

status=$?
if [ "$status" = 0 ] ; then
    echo "Regression Tests found no problems."
    exit 0
else
    echo 1>&2 "Regression Tests violations! Fix them before pushing your code!"
    exit 1
fi
