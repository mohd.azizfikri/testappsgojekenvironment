#!/bin/sh

echo "Publishing Dokka..."

./gradlew dokkaHtmlMultiModule --parallel --daemon

status=$?
if [ "$status" = 0 ] ; then
    echo "Publishing Dokka found no problems."
    exit 0
else
    echo 1>&2 "Publishing Dokka violations! Fix them before pushing your code!"
    exit 1
fi
