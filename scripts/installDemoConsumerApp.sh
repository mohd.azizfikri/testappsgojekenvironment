#!/bin/sh

echo "Installing Demo Consumer App..."

./scripts/build.sh && \
./gradlew :identity-protection-demo-consumer-app:installDebug

status=$?
if [ "$status" = 0 ] ; then
    echo "Installing Demo Consumer App found no problems."
    exit 0
else
    echo 1>&2 "Installing Demo Consumer App violations! Fix them before installing!"
    exit 1
fi
