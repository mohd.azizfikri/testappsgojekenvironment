#!/bin/sh

echo "Publishing Libraries to Maven Local..."

./gradlew :identity-protection-sdk:assembleRelease -PIS_CI=:$1 && \
./gradlew :identity-protection-no-op-sdk:assembleRelease -PIS_CI=:$1 && \
./gradlew :identity-protection-sdk:publishToMavenLocal && \
./gradlew :identity-protection-no-op-sdk:publishToMavenLocal

status=$?
if [ "$status" = 0 ] ; then
    echo "Publishing Libraries found no problems."
    exit 0
else
    echo 1>&2 "Publishing Libraries violations! Fix them before pushing your code!"
    exit 1
fi
