import plugin.AndroidLibraryConfigurationPlugin

plugins {
    id("com.android.library")
    kotlin("android")
}

apply<AndroidLibraryConfigurationPlugin>()
apply("$rootDir/gradle/publish-artifact-task.gradle")
//apply("$rootDir/gradle/script-ext.gradle")

ext {
    set("name", "identity-protection-no-op")
    set("publish", true)
    set("version", /*ext.get("gitVersionName")*/"")
}

android {
    buildTypes {
        getByName("debug") {
            isTestCoverageEnabled = true
            isDebuggable = true
        }

        getByName("release") {
            isTestCoverageEnabled = false
            isDebuggable = false
        }
    }

    lintOptions {
        isAbortOnError = false
    }
}

dependencies {
    implementation(deps.kotlin.stdlib.core)
    implementation(deps.android.gson)

    compileOnly(deps.android.support.annotation)

    testImplementation(deps.android.test.junit)
}
