package identity.protection

/**
 * [ScanAnalysis] is contain an information for Identity Protection SDK when it found an error during
 * the initialization triggered by [VOS_READY]
 * or when the scanning process is completed triggered by [ACTION_SCAN_COMPLETE]
 * or when virtual space detected triggered by [VGUARD_VIRTUAL_SPACE_DETECTED]
 * or when the request scan is timeout
 *
 * There will be multiple [ScanAnalysis] coming due to the continuous scanning process
 *
 * @param processTimeMillis [Long] time taken to complete the process for initialization
 * that causing an error triggered by [VOS_READY]
 * or when every scan is completed triggered by [ACTION_SCAN_COMPLETE]
 * or when virtual space detected triggered by [VGUARD_VIRTUAL_SPACE_DETECTED]
 * or when the request scan is timeout
 *
 * @param message [String] an error message or list of threat categories
 *
 * @see [IdentityProtectionResponse.ResponseModel]
 */
sealed class ScanAnalysis(
    open val processTimeMillis: Long,
    open val message: String
) {

    /**
     * [Error] is used when Identity Protection SDK found an error triggered by [VOS_READY]
     *
     * @param processTimeMillis [Long] time taken to complete the process for initialization
     * that causing an error triggered by [VOS_READY]
     *
     * @param message [String] an error message
     *
     * @see [IdentityProtectionResponse.ResponseModel]
     */
    data class Error(
        override val processTimeMillis: Long,
        override val message: String
    ) : ScanAnalysis(processTimeMillis, message)

    /**
     * [ScanCompleted] is used when Identity Protection SDK receive an event triggered by
     * [ACTION_SCAN_COMPLETE]
     *
     * There will be multiple [ScanCompleted] coming due to the continuous scanning process
     *
     * @param processTimeMillis [Long] time taken when scanning process is completed
     * @param isSuccess [Boolean] true when Identity Protection SDK did not found
     * any error or exception during the initialization, otherwise false
     * @param iteration [String] number of scanning process (eg. ACTION_SCAN_COMPLETE_1, ACTION_SCAN_COMPLETE_2)
     * @param isVosLocked [Boolean] true when critical threats detected, otherwise false
     * @param typeOfThreats list of [String] from critical threats
     *
     * @see [IdentityProtectionResponse.ResponseModel]
     */
    data class ScanCompleted(
        override val processTimeMillis: Long,
        val isSuccess: Boolean,
        val iteration: String,
        val isVosLocked: Boolean,
        val typeOfThreats: List<String>
    ) : ScanAnalysis(processTimeMillis, typeOfThreats.joinToString { it })

    /**
     * [VirtualSpaceDetected] is used when Identity Protection SDK receive an event triggered by
     * [VGUARD_VIRTUAL_SPACE_DETECTED]
     *
     * @param processTimeMillis [Long] time taken when virtual space detected
     * triggered by [VGUARD_VIRTUAL_SPACE_DETECTED]
     *
     * @param isSuccess [Boolean] true when Identity Protection SDK did not found
     * any error or exception during the initialization, otherwise false
     *
     * @param isVosLocked [Boolean] true when virtual space detected, otherwise false
     * @param message [String] an error message
     */
    data class VirtualSpaceDetected(
        override val processTimeMillis: Long,
        override val message: String,
        val isSuccess: Boolean,
        val isVosLocked: Boolean
    ) : ScanAnalysis(processTimeMillis, message)

    /**
     * [ScanTimeOut] is used when Identity Protection SDK trying to scan but
     * the time is exceeded the threshold
     *
     * @param processTimeMillis [Long] time taken to complete the process request scan
     *
     * @param message [String] an error message
     */
    data class ScanTimeOut(
        override val processTimeMillis: Long,
        override val message: String,
    ) : ScanAnalysis(processTimeMillis, message)

    /**
     * [ProfileLoaded] is used when Identity Protection SDK is safe to used
     *
     * @param processTimeMillis [Long] time taken to get [ACTION_PROFILE_LOADED]
     * from [BlackBoxBroadcastReceiver]
     *
     * @param isSuccess [Boolean] true when Identity Protection SDK did not found
     * any error or exception during the initialization, otherwise false
     */
    data class ProfileLoaded(
        override val processTimeMillis: Long,
        val isSuccess: Boolean,
    ) : ScanAnalysis(processTimeMillis, "ACTION_PROFILE_LOADED")
}
