package identity.protection

/**
 * An interface class for IdentityProtectionSDK
 * Every UI Controller which extend to [ProtectionIdentifier] will become an entry point which
 * allow the IdentityProtectionSDK to scan the threats
 */
interface ProtectionIdentifier
