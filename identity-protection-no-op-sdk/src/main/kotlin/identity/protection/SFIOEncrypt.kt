package identity.protection

import identity.protection.SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataFailed
import identity.protection.SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataSuccess
import identity.protection.SFIOEncrypt.EncryptData.EncryptDataFailed
import identity.protection.SFIOEncrypt.EncryptData.EncryptDataSuccess
import identity.protection.SFIOEncrypt.EncryptFile.EncryptFileFailed
import identity.protection.SFIOEncrypt.EncryptFile.EncryptFileSuccess
import identity.protection.SFIOEncrypt.EncryptString.EncryptStringFailed
import identity.protection.SFIOEncrypt.EncryptString.EncryptStringSuccess
import identity.protection.exception.IdentityProtectionSFIOException

/**
 * [SFIOEncrypt] is contain an information for Identity Protection SDK when the encryption process
 * using V-Key Secure File IO Feature is success or not
 *
 * There will be multiple type of encryption
 *
 * @see [EncryptBlockOfData]
 * @see [EncryptString]
 * @see [EncryptData]
 * @see [EncryptFile]
 * @see [WriteToEncryptedFile]
 */
sealed class SFIOEncrypt {

    /**
     * [EncryptBlockOfData] is the output of the encryption process whether it is success or not
     *
     * @see [EncryptBlockOfDataSuccess]
     * @see [EncryptBlockOfDataFailed]
     */
    sealed class EncryptBlockOfData : SFIOEncrypt() {

        /**
         * [EncryptBlockOfDataSuccess] triggered when encryption process is success
         *
         * @param result [ByteArray] is cipher value
         */
        data class EncryptBlockOfDataSuccess(
            val result: ByteArray?
        ) : EncryptBlockOfData() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as EncryptBlockOfDataSuccess

                if (result != null) {
                    if (other.result == null) return false
                    if (!result.contentEquals(other.result)) return false
                } else if (other.result != null) return false

                return true
            }

            override fun hashCode(): Int {
                return result?.contentHashCode() ?: 0
            }
        }

        /**
         * [EncryptBlockOfDataFailed] triggered when encryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class EncryptBlockOfDataFailed(
            val exception: IdentityProtectionSFIOException
        ) : EncryptBlockOfData()
    }

    /**
     * [EncryptString] is the output of the encryption process whether it is success or not
     *
     * @see [EncryptStringSuccess]
     * @see [EncryptStringFailed]
     */
    sealed class EncryptString : SFIOEncrypt() {

        /**
         * [EncryptStringSuccess] triggered when encryption process is success
         */
        object EncryptStringSuccess : EncryptString()

        /**
         * [EncryptStringFailed] triggered when encryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class EncryptStringFailed(
            val exception: IdentityProtectionSFIOException
        ) : EncryptString()
    }

    /**
     * [EncryptData] is the output of the encryption process whether it is success or not
     *
     * @see [EncryptDataSuccess]
     * @see [EncryptDataFailed]
     */
    sealed class EncryptData : SFIOEncrypt() {

        /**
         * [EncryptDataSuccess] triggered when encryption process is success
         */
        object EncryptDataSuccess : EncryptData()

        /**
         * [EncryptDataFailed] triggered when encryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class EncryptDataFailed(
            val exception: IdentityProtectionSFIOException
        ) : EncryptData()
    }

    /**
     * [EncryptFile] is the output of the encryption process whether it is success or not
     *
     * @see [EncryptFileSuccess]
     * @see [EncryptFileFailed]
     */
    sealed class EncryptFile : SFIOEncrypt() {

        /**
         * [EncryptFileSuccess] triggered when encryption process is success
         */
        object EncryptFileSuccess : EncryptFile()

        /**
         * [EncryptFileFailed] triggered when encryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class EncryptFileFailed(
            val exception: IdentityProtectionSFIOException
        ) : EncryptFile()
    }

    /**
     * [WriteToEncryptedFile] is the output of the writing to the encrypted file
     * whether it is success or not
     *
     * @see [WriteToEncryptedFileSuccess]
     * @see [WriteToEncryptedFileFailed]
     */
    sealed class WriteToEncryptedFile : SFIOEncrypt() {

        /**
         * [WriteToEncryptedFileSuccess] triggered when when writing
         * to the encrypted file is success
         */
        object WriteToEncryptedFileSuccess : WriteToEncryptedFile()

        /**
         * [WriteToEncryptedFileFailed] triggered when writing to the encrypted file is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class WriteToEncryptedFileFailed(
            val exception: IdentityProtectionSFIOException
        ) : WriteToEncryptedFile()
    }
}
