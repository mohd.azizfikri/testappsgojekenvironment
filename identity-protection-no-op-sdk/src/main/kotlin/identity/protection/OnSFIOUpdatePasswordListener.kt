package identity.protection

import identity.protection.exception.IdentityProtectionSFIOException

/**
 * An interface class for Secure File IO updating password for encrypted file APIs
 */
interface OnSFIOUpdatePasswordListener {

    /**
     * [onSuccess] Called when updating password is success
     */
    fun onSuccess()


    /**
     * [onError]  Called when updating password is failed due to exceptions
     *
     * @param e [IdentityProtectionSFIOException]
     */
    fun onError(e: IdentityProtectionSFIOException)
}
