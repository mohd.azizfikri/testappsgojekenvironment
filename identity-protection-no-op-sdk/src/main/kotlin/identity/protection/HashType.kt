package identity.protection

/**
 * Collection of SHA Type to be used for signing the message
 *
 * @param rawValue [Int]
 */
enum class HashType(val rawValue: Int) {
    SHA1(1),
    SHA256(2)
}
