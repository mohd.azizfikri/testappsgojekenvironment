package identity.protection

/**
 * A class to define the VOS is ready to use or not
 */
enum class Firmware {
    /**
     * [UNDEFINED] is used when initialization is not complete yet
     */
    UNDEFINED,

    /**
     * [NEGATIVE] means [VOS_FIRMWARE_RETURN_CODE_KEY] < 0
     */
    NEGATIVE,

    /**
     * [POSITIVE] means [VOS_FIRMWARE_RETURN_CODE_KEY] >= 0
     */
    POSITIVE
}

