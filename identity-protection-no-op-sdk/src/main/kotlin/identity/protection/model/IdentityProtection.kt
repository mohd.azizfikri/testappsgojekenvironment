package identity.protection.model

import android.content.Context
import identity.protection.HashType
import identity.protection.OnProtectionAnalyzerListener
import identity.protection.OnRequestScanListener
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIODeleteFileListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.OnSFIOUpdatePasswordListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.exception.IdentityProtectionSFIOException
import identity.protection.log.BlackBoxLog
import java.io.IOException

private const val VOS_NO_OP = "NO-OP"

/**
 * A class that provide sets of APIs for
 *
 * - Gets the [troubleshootingId] of the device from V-OS
 * - [sign]ing mechanism for the given message
 * - [requestProfileChecks] to starts V-OS App Protection profile loaded when conditions are met
 * - [destroy] to perform any final cleanup
 */
object IdentityProtection {

    /**
     * [Config] is used to control the usage of Identity Protection SDK
     *
     * @param isEnabled [Boolean] set to true to enable the Identity Protection SDK
     * @param isDebuggable [Boolean] set to true for development purpose, you have to disable it for production app
     * @param strictMode [Boolean] set to true for strict release mode, this will lock VOS when critical threat detected
     * @param virtualSpaceDetection [Boolean] set to true if you expect to blocked the clone app of your application
     * @param allowsArbitraryNetworking [Boolean] set whether to allow arbitrary networking by enabling/disabling SSL pinning
     * @param timeOutInMillis [Long] threshold-time for Identity Protection SDK to trigger the callback to upstream when [requestProfileChecks] is called and time is exceeded
     * @param criticalThreats List of [Int] critical threat id, set to empty list if you won't blocked any threat detected. Depends on the value of [strictMode] param
     * @param cryptoSignature [CryptoSignature] required only when your application want to enable Signature Verification feature
     * @param onProtectionAnalyzerListener [OnProtectionAnalyzerListener] an interface class, used for analytics if required
     */
    data class Config(
        val isEnabled: Boolean = false,
        val isDebuggable: Boolean = false,
        val strictMode: Boolean = false,
        val virtualSpaceDetection: Boolean = false,
        val allowsArbitraryNetworking: Boolean = false,
        val timeOutInMillis: Long = 10_000L,
        val criticalThreats: List<Int>? = emptyList(),
        val cryptoSignature: CryptoSignature? = null,
        val onProtectionAnalyzerListener: OnProtectionAnalyzerListener? = null
    )

    /**
     * [CryptoSignature] is used to enable Signature Verification feature
     *
     * @param context [Context]
     * @param storeKey [String] Predefine alias that VKey provided along with assets generation
     * @param trustedTimeServerUrl [String] Predefine alias that VKey provided along with assets generation
     * @param hashType [HashType] SHA Type to be used for signing the message
     */
    data class CryptoSignature(
        val context: Context,

        /**
         * Predefine alias that VKey provided along with assets generation
         */
        val storeKey: String,

        /**
         * Predefine alias that VKey provided along with assets generation
         */
        val trustedTimeServerUrl: String? = null,

        /**
         * SHA Type to be used for signing the message
         */
        val hashType: HashType
    )

    /**
     * IdentityProtection configuration data class. Properties can be updated via copy
     *
     * **Example**
     * ```kotlin
     * IdentityProtection.config = IdentityProtection.config.copy(
     *      isEnabled = true,
     *      isDebuggable = false,
     *      strictMode = false,
     *      virtualSpaceDetection = true,
     *      timeOutInMillis = 10_000L,
     *      criticalThreats = listOf(1000, 3000),
     *      cryptoSignature = IdentityProtection.CryptoSignature(
     *              context = this,
     *              storeKey = "MY_STORE_KEY",
     *              hashType = HashType.SHA256
     *      ),
     *      onProtectionAnalyzerListener = object : OnProtectionAnalyzerListener {
     *           override fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
     *               TODO("your requirement")
     *           }
     *      }
     * )
     * ```
     *
     * @return config [Config]
     */
    @Volatile
    var config: Config = Config()
        set(newConfig) {
            val previousConfig = field
            field = newConfig
            logConfigChange(previousConfig, newConfig)
        }

    /**
     * NO-OP
     */
    fun getVKeySDKVersion(): String = VOS_NO_OP

    /**
     * NO-OP
     */
    fun getFirmwareVersion(context: Context): String = VOS_NO_OP

    /**
     * NO-OP
     */
    fun getProcessorVersion(context: Context): String = VOS_NO_OP

    /**
     * NO-OP
     */
    fun isVOSStarted(): Boolean = false

    /**
     * NO-OP
     */
    fun getFirmwareReturnCode(): Long = -1

    /**
     * NO-OP
     */
    fun encrypt(
        input: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptBlockOfData>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIOEncrypt.EncryptBlockOfData::EncryptBlockOfDataFailed)
            .let(onSFIOEncryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun decrypt(
        cipher: ByteArray,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataFailed)
            .let(onSFIODecryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun encryptString(
        input: String,
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptString>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIOEncrypt.EncryptString::EncryptStringFailed)
            .let(onSFIOEncryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun decryptString(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptString>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIODecrypt.DecryptString::DecryptStringFailed)
            .let(onSFIODecryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    @Throws(IOException::class)
    fun encryptData(
        input: String,
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptData>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIOEncrypt.EncryptData::EncryptDataFailed)
            .let(onSFIOEncryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun decryptFile(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataFailed)
            .let(onSFIODecryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun encryptFile(
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptFile>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIOEncrypt.EncryptFile::EncryptFileFailed)
            .let(onSFIOEncryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun rewriteToEncryptedFile(
        filePath: String,
        input: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.WriteToEncryptedFile>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIOEncrypt.WriteToEncryptedFile::WriteToEncryptedFileFailed)
            .let(onSFIOEncryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun readFromEncryptedFile(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.ReadFromDecryptedFile>
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(SFIODecrypt.ReadFromDecryptedFile::ReadFromDecryptedFileFailed)
            .let(onSFIODecryptListener::onCompleted)
    }

    /**
     * NO-OP
     */
    fun updatePassword(
        filePath: String,
        newPassword: String,
        oldPassword: String,
        onSFIOUpdatePasswordListener: OnSFIOUpdatePasswordListener
    ) {
        IdentityProtectionSFIOException(VOS_NO_OP)
            .let(onSFIOUpdatePasswordListener::onError)
    }

    /**
     * NO-OP
     */
    fun deleteFile(
        path: String,
        onSFIODeleteFileListener: OnSFIODeleteFileListener
    ) {
        onSFIODeleteFileListener.onFailed()
    }

    /**
     * NO-OP
     */
    fun troubleshootingId(): String {
        return ""
    }

    /**
     * NO-OP
     */
    fun sign(message: String): ByteArray? {
        return null
    }

    /**
     * This API starts V-OS App Protection profile loaded when conditions are met
     *
     * The results are returned through broadcast with the broadcast intent
     * of [ACTION_PROFILE_LOADED]
     *
     * @param context [Context]
     * @param callback [OnRequestScanListener]
     *
     * @see [Config]
     * @see [VGuard]
     * @see [CryptoSignature]
     * @see [BlackBoxSignature]
     */
    fun requestProfileChecks(context: Context, callback: OnRequestScanListener) {
        /** NO-OP **/
        callback.onCompleted()
    }

    /**
     * Perform any final cleanup. This can happen either because the activity is finishing
     * or because the system is temporarily destroying this instance of the activity to save space.
     */
    fun destroy() {
        /** NO-OP **/
    }

    private fun logConfigChange(previousConfig: Config, newConfig: Config) {
        BlackBoxLog.d {
            val changedFields = mutableListOf<String>()
            Config::class.java.declaredFields.forEach { field ->
                field.isAccessible = true
                val previousValue = field[previousConfig]
                val newValue = field[newConfig]
                if (previousValue != newValue) {
                    changedFields += "${field.name}=$newValue"
                }
            }
            val changesInConfig = if (changedFields.isNotEmpty()) {
                changedFields.joinToString(", ")
            } else {
                "no changes"
            }

            "Updated config: Config($changesInConfig)"
        }
    }
}
