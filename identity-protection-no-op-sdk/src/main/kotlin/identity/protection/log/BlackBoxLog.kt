package identity.protection.log

import identity.protection.log.internal.BlackBoxDefaultLog

/**
 * Central Logger for all Shark artifacts. Set [Logger] to change where these logs go.
 */
object BlackBoxLog {

    /**
     * @see BlackBoxLog
     */
    interface Logger {

        /**
         * Logs a debug message formatted with the passed in arguments.
         */
        fun d(message: String)

        /**
         * Logs a [Throwable] and debug message formatted with the passed in arguments.
         */
        fun d(throwable: Throwable, message: String)
    }

    @Volatile
    var logger: Logger = BlackBoxDefaultLog()

    /**
     * @see Logger.d
     */
    inline fun d(message: () -> String) {
        logger.d(message.invoke())
    }

    /**
     * @see Logger.d
     */
    inline fun d(throwable: Throwable, message: () -> String) {
        logger.d(throwable, message.invoke())
    }
}
