package identity.protection

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Process
import androidx.annotation.RequiresApi
import java.lang.reflect.Method

/**
 * An helper class to check whether the current process is running as main process
 */
object ProcessUtils {

    /**
     * Get the current process name
     *
     * @param context [Context]
     *
     * @return the name of this application's package [String]
     */
    fun getCurrentProcessName(context: Context): String? =
        getCurrentProcessNameByService(context) ?: fallBackGetProcessName()

    private fun fallBackGetProcessName(): String? {
        return if (Build.VERSION.SDK_INT >= VERSION_CODES.P) {
            getCurrentProcessNameByApplication()
        } else {
            getCurrentProcessNameByActivityThread()
        }
    }

    private fun getCurrentProcessNameByService(context: Context): String? {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        val info = manager?.runningAppProcesses?.first { it.pid == Process.myPid() }
        return info?.processName
    }

    @RequiresApi(VERSION_CODES.P)
    private fun getCurrentProcessNameByApplication(): String? =
        Application.getProcessName()

    // Using the same technique as Application.getProcessName() for older devices
    // Using reflection since ActivityThread is an internal API
    @SuppressLint("DiscouragedPrivateApi", "PrivateApi")
    private fun getCurrentProcessNameByActivityThread(): String? {
        return try {
            val declaredMethod: Method = Class.forName("android.app.ActivityThread", false, Application::class.java.classLoader)
                .getDeclaredMethod("currentProcessName", *arrayOfNulls<Class<*>?>(0))
            declaredMethod.isAccessible = true
            val invoke: Any? = declaredMethod.invoke(null, arrayOfNulls<Any>(0))
            invoke as? String

        } catch (e: Throwable) {
            null
        }
    }
}
