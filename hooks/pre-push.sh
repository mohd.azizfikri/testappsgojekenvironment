#!/bin/sh

echo "Running pre push..."

./scripts/build.sh && \
./scripts/runUnitTests.sh && \
./scripts/runRegressionTests.sh && \
./scripts/publishDokka.sh \

./gradlew apiDump && \
./gradlew apiCheck
