****************************************************************************************************
****************************************************************************************************
***********************************     V-Key Assets Package     ***********************************
****************************************************************************************************
****************************************************************************************************

This V-Key license pack is generated with the following information:

BundleID/Package Name        :	com.gojek.app.dev
Manageability                :	1
MD5 of vkeylicensepack       :	f1c837d5bad98fc659e79845f5542f5d

The MD5 checksum and expiration date of the certificates are as follows:

Android-Debug.der (25 October 2043)                :	2d2238863bf8b6beb04ee2d0313543f8
Android-Development.der (07 August 2050)           :	3d253fac85dae321b519092c0c8ee1ef
Android-Distribution.der (12 December 2039)        :	6f533f5330774351afba8c94d6cab506
Driver-Output.der (23 June 2045)                   :	7b397dba55fb03162ecca2a6f13d4426
iOS-Development.der (28 July 2022)                 :	ec403e6d3975e69a62bbeb5d5c5d574d
iOS-Distribution (non-prod).der (31 May 2022)      :	53d60f3cc7516c981e511db3bce04d1e
internal_cert.der (30 April 2051)                  :	a8fbb0228973da494c234e88a69c9fb4
gojek_signed_mtls.p12 (Certificate is verified)    :	491621f66533f854ca5af8a1ca1a4df2

****************************************************************************************************
****************************************************************************************************
*******************     Grant of Limited License / Disclaimer of Warranties     ********************
****************************************************************************************************
****************************************************************************************************

V-Key Inc (V-Key) hereby grants the Licensee a non-transferable and non-exclusive right (the
"License") for the duration of the Subscription Period(s) ordered and paid for to: (i) install, set-
up and process, or otherwise interact with the server components of the Software to generate soft
token firmware, seeds, APIN and related asset files as part of threat intelligence, token
management, authentication, messaging and provisioning; and (ii) allow end users who are customers
of the Licensee (via sub-license) to install and use client application components on mobile devices
for the purposes of tamper protection, identity authentication and transaction authorisation.
Software contains third party machine-readable codes e.g. in the form of open-source (Apache or BSD-
style) and/or for-commercial-use libraries. Such third party terms and conditions are made available
for reference as part of the Software installation package.

V-KEY AND ITS APPROVED SOURCES PROVIDE THE V-KEY SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR
NON-INFRINGEMENT. FURTHER, V-KEY DOES NOT WARRANT RESULTS OF USE OR FREEDOM FROM BUGS OR
UNINTERRUPTED USE OR ACCESS. NOTWITHSTANDING THE FOREGOING, V-KEY WARRANTS THAT V-KEY SOFTW ARE DO
NOT AND WILL NOT CONTAIN ANY VIRUS, TROJAN HORSE, WORM, LOGIC BOMB, OR OTHER SOFTWARE ROUTINE
DESIGNED TO PERMIT UNAUTHORISED ACCESS, TO DISABLE, ERASE OR OTHERWISE HARM SOFTWARE, HARDWARE OR
DATA, OR TO PERFORM ANY SUCH ACTIONS.

Note: The above may be subject to additional terms and conditions negotiated between V-Key and the
License.