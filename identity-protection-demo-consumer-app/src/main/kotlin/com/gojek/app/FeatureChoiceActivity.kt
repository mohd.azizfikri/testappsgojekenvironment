package com.gojek.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gojek.app.blockdata.EncryptDecryptBlockDataActivity
import com.gojek.app.blockdataXfile.BaseEncryptDecryptBlockDataToFromFileActivity
import com.gojek.app.blockdataXfile.EncryptDecryptBlockDataToFromFileActivity
import com.gojek.app.cryptota.CryptoTAActivity
import com.gojek.app.encryptfile.EncryptDecryptExistingFileActivity
import com.gojek.app.stringXfile.BaseEncryptDecryptStringToFromFileActivity
import com.gojek.app.stringXfile.EncryptDecryptStringToFromFileActivity
import kotlinx.android.synthetic.main.activity_feature_choice.*

class FeatureChoiceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feature_choice)

        ctaCryptoTA()
        ctaSecureFileIO()
    }

    private fun ctaCryptoTA() {
        btnCryptoTA.setOnClickListener {
            startActivity(Intent(this, CryptoTAActivity::class.java))
        }
    }

    private fun ctaSecureFileIO() {
        btnEncryptDecryptBlockData.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptBlockDataActivity::class.java))
        }

        btnEncryptDecryptStringFile.setOnClickListener {
            startActivity(Intent(this, BaseEncryptDecryptStringToFromFileActivity::class.java))
        }

        btnEncryptDecryptBlockFile.setOnClickListener {
            startActivity(Intent(this, BaseEncryptDecryptBlockDataToFromFileActivity::class.java))
        }

        btnEncryptingExistingFile.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptExistingFileActivity::class.java))
        }
    }
}