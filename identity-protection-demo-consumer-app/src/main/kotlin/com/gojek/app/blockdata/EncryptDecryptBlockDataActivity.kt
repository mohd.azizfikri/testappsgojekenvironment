package com.gojek.app.blockdata

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gojek.app.R
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.model.IdentityProtection
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.*

class EncryptDecryptBlockDataActivity : AppCompatActivity() {

    private var cipherText: ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encrypt_decrypt_block_data)

        init()
    }

    private fun init() {
        testScenario()
        ctaEncrypt()
        ctaDecrypt()
    }

    private fun testScenario(){
        btn_txt.setOnClickListener {
            inputMessage.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        }
        btn_num.setOnClickListener {
            inputMessage.setText("12345678901234567890")
        }
        btn_tenum.setOnClickListener {
            inputMessage.setText("Test123Test123Test123")
        }
        btn_json.setOnClickListener {
            inputMessage.setText("{\"menu\":{\"id\":\"file\",\"value\":\"File\",\"popup\":{\"menuitem\":[{\"value\":\"New\"}]}}}")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaEncrypt() {
        btnEncrypt.setOnClickListener {
            txtErrorMessageEncrypt.visibility = View.GONE
            txtCipherText.visibility = View.GONE

            val input = inputMessage.text.toString()
            if (input.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input something to encrypt"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                layoutDecrypt.visibility = View.GONE
                txtCipherText.visibility = View.GONE
                return@setOnClickListener
            }

            txtErrorMessageEncrypt.visibility = View.GONE
            IdentityProtection.encrypt(
                input,
                object : OnSFIOEncryptListener<SFIOEncrypt.EncryptBlockOfData> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptBlockOfData) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataSuccess -> {
                                cipherText = sFioEncrypt.result
                                txtCipherText.text =
                                    getString(R.string.cipher_text) + " ${cipherText.toString()}"
                                layoutDecrypt.visibility = View.VISIBLE
                                txtCipherText.visibility = View.VISIBLE
                                txtDecryptText.visibility = View.GONE
                            }
                            is SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataFailed -> {
                                txtErrorMessageEncrypt.text =
                                    "Failed to encrypt: ${sFioEncrypt.exception.message}"
                                txtErrorMessageEncrypt.visibility = View.VISIBLE
                                layoutDecrypt.visibility = View.GONE
                            }
                        }
                    }
                })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaDecrypt() {
        btnDecrypt.setOnClickListener {
            txtErrorMessageDecrypt.visibility = View.GONE
            txtDecryptText.visibility = View.GONE

            if (cipherText == null) {
                txtErrorMessageDecrypt.text = "No text to decrypt"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            IdentityProtection.decrypt(
                cipherText!!,
                object : OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData> {
                    override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptBlockOfData) {
                        when (sFioDecrypt) {
                            is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess -> {
                                val output = sFioDecrypt.result
                                txtDecryptText.text =
                                    getString(R.string.decrypted_text) + " $output"
                                txtDecryptText.visibility = View.VISIBLE
                                txtErrorMessageDecrypt.visibility = View.GONE
                            }
                            is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed -> {
                                txtErrorMessageDecrypt.text =
                                    "Failed to decrypt: ${sFioDecrypt.exception.message}"
                                txtErrorMessageDecrypt.visibility = View.VISIBLE
                                txtDecryptText.visibility = View.GONE
                            }
                        }
                    }
                })
        }
    }
}
