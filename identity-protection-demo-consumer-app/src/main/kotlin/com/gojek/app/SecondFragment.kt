package com.gojek.app

import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import identity.protection.exception.IdentityProtectionIllegalAccessException
import identity.protection.exception.IdentityProtectionNullPointerException
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection
import java.io.UnsupportedEncodingException
import kotlinx.android.synthetic.main.fragment_second.btnSignMessage
import kotlinx.android.synthetic.main.fragment_second.inputMessage
import kotlinx.android.synthetic.main.fragment_second.txtSignedMessage

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

        btnSignMessage.setOnClickListener {
            txtSignedMessage.text = getSignature(inputMessage.text.toString())
        }
    }

    private fun getSignature(message: String): String {
        return try {
            IdentityProtection.sign(message)?.let {
                Base64.encodeToString(it, Base64.NO_WRAP)
            } ?: "Failed to sign the message -> NULL"
        } catch (e: IdentityProtectionIllegalAccessException) {
            BlackBoxLog.d(e) { "SecondFragment#Failed to sign the message" }
            e.message.orEmpty()
        } catch (e: IdentityProtectionNullPointerException) {
            BlackBoxLog.d(e) { "SecondFragment#Failed to sign the message" }
            e.message.orEmpty()
        } catch (e: UnsupportedEncodingException) {
            BlackBoxLog.d(e) { "SecondFragment#Failed to sign the message" }
            e.message.orEmpty()
        }
    }
}
