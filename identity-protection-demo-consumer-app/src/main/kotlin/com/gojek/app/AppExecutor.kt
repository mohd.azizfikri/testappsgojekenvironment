package com.gojek.app

import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * An interface for composition concrete implementation of executors
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
internal interface AppExecutor {

    /**
     * An [Executor] that will return [Executors.newSingleThreadExecutor]
     */
    val io: Executor

    /**
     * An [Executor] that will return [AsyncTask.THREAD_POOL_EXECUTOR]
     */
    val default: Executor

    /**
     * An [Executor] that will return [AppExecutorImpl.MainThreadExecutor]
     */
    val ui: Executor
}

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 *
 * ```
 * val appExecutor = AppExecutorImpl()
 *
 * appExecutor.io.execute {
 *    apiService.foo(object : Callback<FooResponse> {
 *       override fun onSuccess(data: FooResponse) {
 *           appExecutor.mainThread.execute {
 *             ...
 *           }
 *       }
 *
 *       override fun onError(error: Throwable) {
 *           appExecutor.mainThread.execute {
 *             ...
 *           }
 *       }
 *    })
 * }
 * ```
 *
 * @param diskIO [Executor] an object that represent [Executors.newFixedThreadPool]
 * @param networkIO [Executor] an object that represent [AsyncTask.THREAD_POOL_EXECUTOR]
 * @param mainThread [Executor] an object that represent [MainThreadExecutor]
 */
internal class AppExecutorImpl(
    private val diskIO: Executor,
    private val networkIO: Executor,
    private val mainThread: Executor
) : AppExecutor {

    constructor() : this(
        Executors.newFixedThreadPool(
            (Runtime.getRuntime().availableProcessors() - 1).coerceAtLeast(1)
        ),
        AsyncTask.THREAD_POOL_EXECUTOR,
        MainThreadExecutor
    )

    /**
     * An [Executor] that will return [Executors.newFixedThreadPool]
     */
    override val io: Executor
        get() = diskIO

    /**
     * An [Executor] that will return [AsyncTask.THREAD_POOL_EXECUTOR]
     */
    override val default: Executor
        get() = networkIO

    /**
     * An [Executor] that will return [MainThreadExecutor]
     */
    override val ui: Executor
        get() = mainThread

    private object MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}
