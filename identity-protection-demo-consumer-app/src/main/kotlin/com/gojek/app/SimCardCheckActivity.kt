package com.gojek.app

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import com.google.gson.internal.bind.DateTypeAdapter
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection
import java.util.Date
import kotlinx.android.synthetic.main.activity_sim_card_check.customViewSimCardIdentifier
import kotlinx.android.synthetic.main.activity_sim_card_check.txtMessageSIMCardError
import java.util.concurrent.atomic.AtomicInteger

class SimCardCheckActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sim_card_check)

        val subscriptionManager = this.getSystemService(
            Context.TELEPHONY_SUBSCRIPTION_SERVICE
        ) as? SubscriptionManager
        simCardCheck(subscriptionManager)
    }

    @SuppressLint("MissingPermission")
    private fun simCardCheck(subscriptionManager: SubscriptionManager?) {
        subscriptionManager?.let {
            if (subscriptionManager.activeSubscriptionInfoList.size > 0) {
                txtMessageSIMCardError.visibility = View.GONE
            }

            val indexSIMCard = AtomicInteger(0)
            subscriptionManager.activeSubscriptionInfoList
                .map {
                    printLog(it)
                    it.toSimSubInfo()
                }
                .forEach { initChild(it, indexSIMCard) }
        } ?: run {
            txtMessageSIMCardError.visibility = View.VISIBLE
        }
    }

    private fun initChild(simSubInfo: SimSubInfo, indexSIMCard: AtomicInteger) {
        val view: View = LayoutInflater.from(this)
            .inflate(R.layout.view_sim_card_identifier, null)
        val txtSubscriptionId = view.findViewById<TextView>(R.id.txtSubscriptionId)
        txtSubscriptionId.text = simSubInfo.subscriptionId.toString()

        val layoutDetail = view.findViewById<RelativeLayout>(R.id.layoutSimCardIdentifierDetail)
        layoutDetail.visibility = View.GONE

        val btnExpandCollapse = view.findViewById<Button>(R.id.btnExpandOrCollapse)
        btnExpandCollapse.text = getString(R.string.expand)
        btnExpandCollapse.setOnClickListener {
            if (layoutDetail.visibility == View.GONE) {
                layoutDetail.visibility = View.VISIBLE
                btnExpandCollapse.text = getString(R.string.collapse)
            } else {
                layoutDetail.visibility = View.GONE
                btnExpandCollapse.text = getString(R.string.expand)
            }
        }

        /**
         * Detect SIM Change
         */
        val txtSimCardNewOrRegistered =
            view.findViewById<TextView>(R.id.txtSimCardNewOrRegistered)
        txtSimCardNewOrRegistered.visibility = View.GONE

        val layoutSimCardIdentifierRegistered =
            view.findViewById<RelativeLayout>(R.id.layoutSimCardIdentifierRegistered)
        layoutSimCardIdentifierRegistered.visibility = View.GONE

        val layoutSimCardIdentifierNotRegistered =
            view.findViewById<RelativeLayout>(R.id.layoutSimCardIdentifierNotRegistered)
        layoutSimCardIdentifierNotRegistered.visibility = View.GONE

        val prefs: SharedPreferences =
            getSharedPreferences("POCSecureStorage", MODE_PRIVATE)
        val listOfRegisteredSIMCardFromLocalStorage =
            prefs.getStringSet("registeredSIMCard", emptySet()) ?: emptySet()
        if (listOfRegisteredSIMCardFromLocalStorage.isEmpty()) {
            layoutSimCardIdentifierNotRegistered.visibility = View.VISIBLE
        } else {
            val isSIMCardRegistered =
                listOfRegisteredSIMCardFromLocalStorage.contains(simSubInfo.subscriptionId.toString())

            if (isSIMCardRegistered) {
                txtSimCardNewOrRegistered.text =
                    getString(R.string.we_detect_old_sim_card_have_been_inserted)
                txtSimCardNewOrRegistered.visibility = View.VISIBLE
                layoutSimCardIdentifierRegistered.visibility = View.VISIBLE
                layoutSimCardIdentifierNotRegistered.visibility = View.GONE
            } else {
                txtSimCardNewOrRegistered.text =
                    getString(R.string.we_detect_new_sim_card_have_been_inserted)
                txtSimCardNewOrRegistered.visibility = View.VISIBLE
                layoutSimCardIdentifierNotRegistered.visibility = View.VISIBLE
            }
        }

        val btnRegisterSimCard = view.findViewById<Button>(R.id.btnRegisterSimCard)
        btnRegisterSimCard.setOnClickListener { _ ->
            val newListOfRegisteredSIMCardFromLocalStorage =
                prefs.getStringSet("registeredSIMCard", emptySet()) ?: emptySet()
            val finalRegisteredSIMCard =
                newListOfRegisteredSIMCardFromLocalStorage.plus(simSubInfo.subscriptionId.toString())
            val editor = getSharedPreferences("POCSecureStorage", MODE_PRIVATE).edit()
            editor.putStringSet("registeredSIMCard", finalRegisteredSIMCard)
            editor.apply()

            layoutSimCardIdentifierRegistered.visibility = View.VISIBLE
            layoutSimCardIdentifierNotRegistered.visibility = View.GONE
        }

        /**
         * END Detect SIM Change
         */

        /**
         * V-KEY Implementation
         */
        // subscriptionID + mcc + mnc
        // eg: 5@510@10
        // regex pasword: P@ssw0rd
        // final password: P@ss5@510@10w0rd
        val fileName = "trustedSimCard${indexSIMCard.getAndIncrement()}.txt"
        val password = "P@ss${simSubInfo.subscriptionId}@${simSubInfo.mccCode}@${simSubInfo.mncCode}w0rd"
        val input = simSubInfo.toString()

        val layoutSimCardIdentifierNonEncrypted =
            view.findViewById<RelativeLayout>(R.id.layoutSimCardIdentifierNonEncrypted)
        layoutSimCardIdentifierNonEncrypted.visibility = View.GONE

        val layoutSimCardIdentifierEncrypted =
            view.findViewById<RelativeLayout>(R.id.layoutSimCardIdentifierEncrypted)
        layoutSimCardIdentifierEncrypted.visibility = View.GONE

        val btnEncryptSimCard = view.findViewById<Button>(R.id.btnEncryptSimCard)
        btnEncryptSimCard.setOnClickListener { _ ->
            IdentityProtection.encryptString(
                input,
                this.filesDir.absolutePath + "/$fileName",
                password,
                object : OnSFIOEncryptListener<SFIOEncrypt.EncryptString> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptString) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.EncryptString.EncryptStringSuccess -> {
                                layoutSimCardIdentifierNonEncrypted.visibility = View.GONE
                                layoutSimCardIdentifierEncrypted.visibility = View.VISIBLE
                                Toast.makeText(
                                    applicationContext,
                                    "SIM Card have been saved to V-Key Secure Storage",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            is SFIOEncrypt.EncryptString.EncryptStringFailed -> {
                                Toast.makeText(
                                    applicationContext,
                                    "Failed to save sim card " +
                                            "(subscriptionID: ${simSubInfo.subscriptionId}) " +
                                            "-> ${sFioEncrypt.exception.message}",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            )
        }

        val absoluteFileName: String = this.filesDir.absolutePath + "/$fileName"
        IdentityProtection.decryptString(
            absoluteFileName,
            password,
            object : OnSFIODecryptListener<SFIODecrypt.DecryptString> {
                override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptString) {
                    when (sFioDecrypt) {
                        is SFIODecrypt.DecryptString.DecryptStringSuccess -> {
                            layoutSimCardIdentifierEncrypted.visibility = View.VISIBLE
                        }
                        is SFIODecrypt.DecryptString.DecryptStringFailed -> {
                            layoutSimCardIdentifierNonEncrypted.visibility = View.VISIBLE
                            layoutSimCardIdentifierEncrypted.visibility = View.GONE
                        }
                    }
                }
            }
        )
        /**
         * END V-KEY Implementation
         */

        val txtCarrierName = view.findViewById<TextView>(R.id.txtCarrierName)
        txtCarrierName.text = simSubInfo.carrierName

        val txtCountryISO = view.findViewById<TextView>(R.id.txtCountryISO)
        txtCountryISO.text = simSubInfo.carrierName

        val txtDataRoaming = view.findViewById<TextView>(R.id.txtDataRoaming)
        txtDataRoaming.text = simSubInfo.dataRoaming

        val txtDisplayName = view.findViewById<TextView>(R.id.txtDisplayName)
        txtDisplayName.text = simSubInfo.displayName

        val txtICCID = view.findViewById<TextView>(R.id.txtICCID)
        txtICCID.text = simSubInfo.iccId

        val txtIconTint = view.findViewById<TextView>(R.id.txtICCTint)
        txtIconTint.text = simSubInfo.iconTint

        val txtMCCCode = view.findViewById<TextView>(R.id.txtMCCCode)
        txtMCCCode.text = simSubInfo.mccCode

        val txtMNCCode = view.findViewById<TextView>(R.id.txtMNCCode)
        txtMNCCode.text = simSubInfo.mncCode

        val txtNumber = view.findViewById<TextView>(R.id.txtNumber)
        txtNumber.text = simSubInfo.number

        val txtSimSlotIndex = view.findViewById<TextView>(R.id.txtSimSlotIndex)
        txtSimSlotIndex.text = simSubInfo.simSlotIndex

        val txtCardId = view.findViewById<TextView>(R.id.txtCardId)
        txtCardId.text = simSubInfo.cardId

        val txtCarrierId = view.findViewById<TextView>(R.id.txtCarrierId)
        txtCarrierId.text = simSubInfo.carrierId

        val txtGroupUUID = view.findViewById<TextView>(R.id.txtGroupUUID)
        txtGroupUUID.text = simSubInfo.groupUuid

        val txtIsEmbedded = view.findViewById<TextView>(R.id.txtIsEmbedded)
        txtIsEmbedded.text = simSubInfo.isEmbedded

        val txtIsOpportunistic = view.findViewById<TextView>(R.id.txtIsOpportunistic)
        txtIsOpportunistic.text = simSubInfo.isOpportunistic

        val txtSubscriptionType = view.findViewById<TextView>(R.id.txtSubscriptionType)
        txtSubscriptionType.text = simSubInfo.subscriptionType

        customViewSimCardIdentifier.addView(view)
    }

    private fun printLog(subscriptionInfo: SubscriptionInfo) {
        BlackBoxLog.d { "-------" }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            BlackBoxLog.d { "simInfo| ### Requires API Level 29 ###" }
            BlackBoxLog.d { "simInfo| cardId: ${subscriptionInfo.cardId}" }
            BlackBoxLog.d { "simInfo| carrierId: ${subscriptionInfo.carrierId}" }
            BlackBoxLog.d { "simInfo| groupUuid: ${subscriptionInfo.groupUuid}" }
            BlackBoxLog.d { "simInfo| isEmbedded: ${subscriptionInfo.isEmbedded}" }
            BlackBoxLog.d { "simInfo| isOpportunistic: ${subscriptionInfo.isOpportunistic}" }
            BlackBoxLog.d { "simInfo| mccString: ${subscriptionInfo.mccString}" }
            BlackBoxLog.d { "simInfo| subscriptionType: ${subscriptionInfo.subscriptionType}" }
            BlackBoxLog.d { "simInfo| mncString: ${subscriptionInfo.mncString}" }
            BlackBoxLog.d { "simInfo| ###############" }
        }

        BlackBoxLog.d { "simInfo| subscriptionId: ${subscriptionInfo.subscriptionId}" }
        BlackBoxLog.d { "simInfo| Carrier name: ${subscriptionInfo.carrierName}" }
        BlackBoxLog.d { "simInfo| countryIso: ${subscriptionInfo.countryIso}" }
        BlackBoxLog.d { "simInfo| dataRoaming: ${subscriptionInfo.dataRoaming}" }
        BlackBoxLog.d { "simInfo| displayName: ${subscriptionInfo.displayName}" }
        BlackBoxLog.d { "simInfo| iccId: ${subscriptionInfo.iccId}" }
        BlackBoxLog.d { "simInfo| iconTint: ${subscriptionInfo.iconTint}" }
        BlackBoxLog.d { "simInfo| mcc: ${subscriptionInfo.mcc}" }
        BlackBoxLog.d { "simInfo| mnc: ${subscriptionInfo.mnc}" }
        BlackBoxLog.d { "simInfo| number: ${subscriptionInfo.number}" }
        BlackBoxLog.d { "simInfo| simSlotIndex: ${subscriptionInfo.simSlotIndex}" }

        BlackBoxLog.d { "simInfo| simSlotIndex: $subscriptionInfo" }
    }
}

@SuppressLint("NewApi")
internal fun SubscriptionInfo.toSimSubInfo() =
    SimSubInfo(
        subscriptionId = subscriptionId,
        carrierName = carrierName?.toString().orEmpty(),
        countryIso = countryIso.orEmpty(),
        dataRoaming = dataRoaming.toString(),
        displayName = displayName?.toString().orEmpty(),
        iccId = iccId.orEmpty(),
        iconTint = iconTint.toString(),
        number = number.orEmpty(),
        simSlotIndex = simSlotIndex.toString()
    ).apply {
        /*
         Need to use mccString and mncString >= API 29 as the latter is deprecated
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            mccCode = mccString.orEmpty()
            mncCode = mncString.orEmpty()
            cardId = getCardId().toString()
            carrierId = getCarrierId().toString()
            groupUuid = getGroupUuid().toString()
            isEmbedded = isEmbedded().toString()
            isOpportunistic = isOpportunistic().toString()
            subscriptionType = getSubscriptionType().toString()
        } else {
            mccCode = mcc.toString()
            mncCode = mnc.toString()
            cardId = "Requires API Level 29"
            carrierId = "Requires API Level 29"
            groupUuid = "Requires API Level 29"
            isEmbedded = "Requires API Level 29"
            isOpportunistic = "Requires API Level 29"
            subscriptionType = "Requires API Level 29"
        }
    }

internal class SimSubInfo(
    @SerializedName("subscriptionId")
    val subscriptionId: Int,
    @SerializedName("carrierName")
    val carrierName: String?,
    @SerializedName("countryIso")
    val countryIso: String?,
    @SerializedName("dataRoaming")
    val dataRoaming: String?,
    @SerializedName("displayName")
    val displayName: String?,
    @SerializedName("iccId")
    val iccId: String?,
    @SerializedName("iconTint")
    val iconTint: String?,
    @SerializedName("mccCode")
    var mccCode: String? = null,
    @SerializedName("mncCode")
    var mncCode: String? = null,
    @SerializedName("number")
    var number: String?,
    @SerializedName("simSlotIndex")
    val simSlotIndex: String?,
    @SerializedName("cardId")
    var cardId: String? = null,
    @SerializedName("carrierId")
    var carrierId: String? = null,
    @SerializedName("groupUuid")
    var groupUuid: String? = null,
    @SerializedName("isEmbedded")
    var isEmbedded: String? = null,
    @SerializedName("isOpportunistic")
    var isOpportunistic: String? = null,
    @SerializedName("subscriptionType")
    var subscriptionType: String? = null
) {
    override fun equals(other: Any?): Boolean {
        return subscriptionId == (other as? SimSubInfo)?.subscriptionId
    }

    override fun hashCode(): Int {
        return subscriptionId
    }
}

internal data class SimSubData(
    @SerializedName("subData")
    val subInfoList: List<SimSubInfo>
) : JSONConvertable

interface JSONConvertable {
    fun toJSON(): String = gson.toJson(this)
}

val gson by lazy {
    GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        .registerTypeAdapter(Date::class.java, DateTypeAdapter())
        .create()
}
