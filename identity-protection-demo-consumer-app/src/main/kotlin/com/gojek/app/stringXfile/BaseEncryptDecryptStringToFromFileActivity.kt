package com.gojek.app.stringXfile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gojek.app.R

class BaseEncryptDecryptStringToFromFileActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_encrypt_decript_string_x_file)
        setSupportActionBar(findViewById(R.id.toolbar))

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
