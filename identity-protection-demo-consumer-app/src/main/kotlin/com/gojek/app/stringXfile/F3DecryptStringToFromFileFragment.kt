package com.gojek.app.stringXfile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gojek.app.R
import identity.protection.OnSFIODecryptListener
import identity.protection.SFIODecrypt
import identity.protection.model.IdentityProtection
import kotlinx.android.synthetic.main.activity_crypto_ta.*

class F3DecryptStringToFromFileFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f3_decrypt_string_x_file, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.f1).setOnClickListener {
            findNavController().navigate(R.id.action_DecryptSTF_to_EncryptSTF)
        }

        view.findViewById<TextView>(R.id.f2).setOnClickListener {
            findNavController().navigate(R.id.action_DecryptSTF_to_ChangePassSTF)
        }

        ctaUpdatePassword(view)
    }

    @SuppressLint("SetTextI18n")
    private fun ctaUpdatePassword(view: View) {
        /*
        * Define view on fragment
        */
        val inputDecryptFileName = view.findViewById<EditText>(R.id.inputDecryptFileName)
        val inputDecryptPassword = view.findViewById<EditText>(R.id.inputDecryptPassword)
        val txtErrorMessageDecrypt = view.findViewById<TextView>(R.id.txtErrorMessageDecrypt)
        val btnDecrypt = view.findViewById<Button>(R.id.btnDecrypt)
        val txtInfoAja = view.findViewById<TextView>(R.id.txtInfoAja)
        val txtDecryptText = view.findViewById<TextView>(R.id.txtDecryptText)


        /*
         * Function
         */
        btnDecrypt.setOnClickListener {
            txtErrorMessageDecrypt.visibility = View.GONE
            txtDecryptText.visibility = View.GONE
            txtInfoAja.visibility = View.GONE

            val fileName = inputDecryptFileName.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageDecrypt.text = "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageDecrypt.text = "Make sure your file name using .txt extension"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputDecryptPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageDecrypt.text = "Please input password"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = requireActivity().filesDir.absolutePath + "/$fileName"
            IdentityProtection.decryptString(
                absoluteFileName,
                password,
                object : OnSFIODecryptListener<SFIODecrypt.DecryptString> {
                    override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptString) {
                        when (sFioDecrypt) {
                            is SFIODecrypt.DecryptString.DecryptStringSuccess -> {
                                val output = sFioDecrypt.result
                                txtDecryptText.text = getString(R.string.file_content) + " $output"
                                txtDecryptText.visibility = View.VISIBLE
                                txtErrorMessageDecrypt.visibility = View.GONE
                                txtInfoAja.visibility = View.VISIBLE
                            }
                            is SFIODecrypt.DecryptString.DecryptStringFailed -> {
                                txtErrorMessageDecrypt.text = "Failed to decrypt\n\n ErrorLog: ${sFioDecrypt.exception.message}"
                                txtErrorMessageDecrypt.visibility = View.VISIBLE
                                txtDecryptText.visibility = View.GONE
                                txtInfoAja.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            )
        }
    }
}