package com.gojek.app.cryptota

import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gojek.app.AppExecutor
import com.gojek.app.AppExecutorImpl
import com.gojek.app.R
import identity.protection.exception.IdentityProtectionIllegalAccessException
import identity.protection.exception.IdentityProtectionNullPointerException
import identity.protection.model.IdentityProtection
import kotlinx.android.synthetic.main.activity_crypto_ta.*
import kotlinx.android.synthetic.main.activity_crypto_ta.inputMessage
import java.io.UnsupportedEncodingException
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.*
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.btn_json
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.btn_num
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.btn_tenum
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data.btn_txt

class CryptoTAActivity : AppCompatActivity() {

    private val appExecutor: AppExecutor = AppExecutorImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crypto_ta)

        init()
    }

    private fun init() {
        testScenario()
        ctaSignedMessage()
    }

    private fun testScenario(){
        btn_txt.setOnClickListener {
            inputMessage.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        }
        btn_num.setOnClickListener {
            inputMessage.setText("12345678901234567890")
        }
        btn_tenum.setOnClickListener {
            inputMessage.setText("Test123Test123Test123")
        }
        btn_json.setOnClickListener {
            inputMessage.setText("{\"menu\":{\"id\":\"file\",\"value\":\"File\",\"popup\":{\"menuitem\":[{\"value\":\"New\"}]}}}")
        }
    }

    private fun ctaSignedMessage() {
        btnSignMessage.setOnClickListener {
            loading.visibility = View.VISIBLE
            txtInfoAja.visibility = View.GONE

            txtErrorSignedMessage.visibility = View.GONE
            txtSignedMessage.visibility = View.GONE
            generateSignature(inputMessage.text.toString())
        }
    }

    private fun generateSignature(message: String) {
        try {
            appExecutor.io.execute {
                val result: String? = IdentityProtection.sign(message)?.let {
                    Base64.encodeToString(it, Base64.NO_WRAP)
                }

                appExecutor.ui.execute {
                    if(!result.isNullOrEmpty()) {
                        txtSignedMessage.text = result
                        txtSignedMessage.visibility = View.VISIBLE
                    } else {
                        txtErrorSignedMessage.visibility = View.VISIBLE
                        txtErrorSignedMessage.text = getString(R.string.no_message)
                    }
                    loading.visibility = View.GONE
                    txtInfoAja.visibility = View.VISIBLE
                }
            }

        } catch (e: IdentityProtectionIllegalAccessException) {
            txtSignedMessage.visibility = View.GONE
            txtErrorSignedMessage.visibility = View.VISIBLE
            txtErrorSignedMessage.text = e.message
            loading.visibility = View.GONE
            txtInfoAja.visibility = View.VISIBLE
        } catch (e: IdentityProtectionNullPointerException) {
            txtSignedMessage.visibility = View.GONE
            txtErrorSignedMessage.visibility = View.VISIBLE
            txtErrorSignedMessage.text = e.message
            loading.visibility = View.GONE
            txtInfoAja.visibility = View.VISIBLE
        } catch (e: UnsupportedEncodingException) {
            txtSignedMessage.visibility = View.GONE
            txtErrorSignedMessage.visibility = View.VISIBLE
            txtErrorSignedMessage.text = e.message
            loading.visibility = View.GONE
            txtInfoAja.visibility = View.VISIBLE
        }
    }
}
