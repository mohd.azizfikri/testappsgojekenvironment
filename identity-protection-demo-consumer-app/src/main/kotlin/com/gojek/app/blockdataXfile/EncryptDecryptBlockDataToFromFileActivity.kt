package com.gojek.app.blockdataXfile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gojek.app.R
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.OnSFIOUpdatePasswordListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.exception.IdentityProtectionSFIOException
import identity.protection.model.IdentityProtection
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.btnDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.btnEncrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.btnUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.dividerUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputDecryptFileName
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputDecryptPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputFileName
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputFileNameUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputMessage
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputNewPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputOldPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.inputPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.layoutDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.layoutUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtCipherText
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtDecryptText
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtErrorMessageDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtErrorMessageEncrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtErrorMessageUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_block_data_to_from_file.txtUpdatePasswordSuccess

class EncryptDecryptBlockDataToFromFileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encrypt_decrypt_block_data_to_from_file)

        init()
    }

    private fun init() {
        ctaEncrypt()
        ctaUpdatePassword()
        ctaDecrypt()
    }

    @SuppressLint("SetTextI18n")
    private fun ctaEncrypt() {
        btnEncrypt.setOnClickListener {
            dividerUpdatePassword.visibility = View.GONE
            layoutDecrypt.visibility = View.GONE
            layoutUpdatePassword.visibility = View.GONE
            txtErrorMessageEncrypt.visibility = View.GONE
            txtCipherText.visibility = View.GONE

            val input = inputMessage.text.toString()
            if (input.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input something to encrypt"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val fileName = inputFileName.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageEncrypt.text = "Make sure your file name using .txt extension"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input password"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            IdentityProtection.encryptData(
                input,
                this.filesDir.absolutePath + "/$fileName",
                password,
                object : OnSFIOEncryptListener<SFIOEncrypt.EncryptData> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptData) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.EncryptData.EncryptDataSuccess -> {
                                txtCipherText.text = "Response: Success, file name: $fileName"
                                txtCipherText.visibility = View.VISIBLE
                                layoutDecrypt.visibility = View.VISIBLE
                                layoutUpdatePassword.visibility = View.VISIBLE
                                dividerUpdatePassword.visibility = View.VISIBLE
                            }
                            is SFIOEncrypt.EncryptData.EncryptDataFailed -> {
                                txtErrorMessageEncrypt.text = "Failed to encrypt: ${sFioEncrypt.exception.message}"
                                txtErrorMessageEncrypt.visibility = View.VISIBLE
                                txtCipherText.visibility = View.GONE
                                layoutDecrypt.visibility = View.GONE
                                layoutUpdatePassword.visibility = View.GONE
                                dividerUpdatePassword.visibility = View.GONE
                            }
                        }
                    }
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaUpdatePassword() {
        btnUpdatePassword.setOnClickListener {
            txtErrorMessageDecrypt.visibility = View.GONE
            txtDecryptText.visibility = View.GONE
            txtUpdatePasswordSuccess.visibility = View.GONE
            txtErrorMessageUpdatePassword.visibility = View.GONE

            val fileName = inputFileNameUpdatePassword.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageUpdatePassword.text =
                    "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageUpdatePassword.text = "Make sure your file name using .txt extension"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val oldPassword = inputOldPassword.text.toString()
            if (oldPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input old password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val newPassword = inputNewPassword.text.toString()
            if (newPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input new password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = this.filesDir.absolutePath + "/$fileName"
            IdentityProtection.updatePassword(
                absoluteFileName,
                newPassword,
                oldPassword,
                object : OnSFIOUpdatePasswordListener {
                    override fun onSuccess() {
                        txtUpdatePasswordSuccess.text = "Response: Success update password"
                        txtUpdatePasswordSuccess.visibility = View.VISIBLE
                    }

                    override fun onError(e: IdentityProtectionSFIOException) {
                        txtErrorMessageUpdatePassword.text = "Failed to update password: ${e.message}"
                        txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                    }
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaDecrypt() {
        btnDecrypt.setOnClickListener {
            txtErrorMessageDecrypt.visibility = View.GONE
            txtDecryptText.visibility = View.GONE

            val fileName = inputDecryptFileName.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageDecrypt.text = "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageDecrypt.text = "Make sure your file name using .txt extension"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputDecryptPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageDecrypt.text = "Please input password"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = this.filesDir.absolutePath + "/$fileName"
            IdentityProtection.decryptFile(
                absoluteFileName,
                password,
                object : OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData> {
                    override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptBlockOfData) {
                        when (sFioDecrypt) {
                            is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess -> {
                                val output = sFioDecrypt.result
                                txtDecryptText.text = getString(R.string.file_content) + " $output"
                                txtDecryptText.visibility = View.VISIBLE
                                txtErrorMessageDecrypt.visibility = View.GONE
                            }
                            is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed -> {
                                txtErrorMessageDecrypt.text = "Failed to decrypt: ${sFioDecrypt.exception.message}"
                                txtErrorMessageDecrypt.visibility = View.VISIBLE
                                txtDecryptText.visibility = View.GONE
                            }
                        }
                    }
                }
            )
        }
    }
}
