package com.gojek.app.blockdataXfile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gojek.app.R
import identity.protection.OnSFIOUpdatePasswordListener
import identity.protection.exception.IdentityProtectionSFIOException
import identity.protection.model.IdentityProtection

class F2ChangePasswordBlockDataToFromFileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f2_change_pass_block_data_x_file, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.f1).setOnClickListener {
            findNavController().navigate(R.id.action_ChangePassSTF_to_EncryptSTF)
        }

        view.findViewById<TextView>(R.id.f3).setOnClickListener {
            findNavController().navigate(R.id.action_ChangePassSTF_to_DecryptSTF)
        }

        ctaUpdatePassword(view)
    }

    @SuppressLint("SetTextI18n")
    private fun ctaUpdatePassword(view: View) {
        /*
        * Define view on fragment
        */
        val inputFileNameUpdatePassword = view.findViewById<EditText>(R.id.inputFileNameUpdatePassword)
        val inputOldPassword = view.findViewById<EditText>(R.id.inputOldPassword)
        val inputNewPassword = view.findViewById<EditText>(R.id.inputNewPassword)
        val txtErrorMessageUpdatePassword = view.findViewById<TextView>(R.id.txtErrorMessageUpdatePassword)
        val btnUpdatePassword = view.findViewById<Button>(R.id.btnUpdatePassword)
        val txtUpdatePasswordSuccess = view.findViewById<TextView>(R.id.txtUpdatePasswordSuccess)


        /*
         * Function
         */
        btnUpdatePassword.setOnClickListener {
            txtUpdatePasswordSuccess.visibility = View.GONE
            txtErrorMessageUpdatePassword.visibility = View.GONE

            val fileName = inputFileNameUpdatePassword.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageUpdatePassword.text =
                    "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageUpdatePassword.text = "Make sure your file name using .txt extension"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val oldPassword = inputOldPassword.text.toString()
            if (oldPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input old password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val newPassword = inputNewPassword.text.toString()
            if (newPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input new password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = requireActivity().filesDir.absolutePath + "/$fileName"
            IdentityProtection.updatePassword(
                absoluteFileName,
                newPassword,
                oldPassword,
                object : OnSFIOUpdatePasswordListener {
                    override fun onSuccess() {
                        txtUpdatePasswordSuccess.text = "Response: Success update password"
                        txtUpdatePasswordSuccess.visibility = View.VISIBLE
                        txtErrorMessageUpdatePassword.visibility = View.GONE
                    }

                    override fun onError(e: IdentityProtectionSFIOException) {
                        txtErrorMessageUpdatePassword.text = "Failed to update password\n\n ErrorLog: ${e.message}"
                        txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                    }
                }
            )
        }
    }
}