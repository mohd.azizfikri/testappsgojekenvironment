package com.gojek.app.blockdataXfile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gojek.app.R
import identity.protection.OnSFIOEncryptListener
import identity.protection.SFIOEncrypt
import identity.protection.model.IdentityProtection

class F1EncryptBlockDataToFromFileFragment: Fragment() {
    var isOpen: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f1_encrypt_block_data_x_file, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.f2).setOnClickListener {
            findNavController().navigate(R.id.action_EncryptSTF_to_ChangePassSTF)

        }

        view.findViewById<TextView>(R.id.f3).setOnClickListener {
            findNavController().navigate(R.id.action_EncryptSTF_to_DecryptSTF)
        }

        ctaEncrypt(view)
    }

    @SuppressLint("SetTextI18n")
    private fun ctaEncrypt(view: View) {
        /*
         * Define view on fragment
         */
        val inputMessage = view.findViewById<EditText>(R.id.inputMessage)
        val inputFileName = view.findViewById<EditText>(R.id.inputFileName)
        val inputPassword = view.findViewById<EditText>(R.id.inputPassword)
        val txtErrorMessageEncrypt = view.findViewById<TextView>(R.id.txtErrorMessageEncrypt)
        val btnEncrypt = view.findViewById<Button>(R.id.btnEncrypt)
        val txtCipherText = view.findViewById<TextView>(R.id.txtCipherText)


        /*
         * Function
         */
        btnEncrypt.setOnClickListener {
            txtErrorMessageEncrypt.visibility = View.GONE
            txtCipherText.visibility = View.GONE

            val input = inputMessage.text.toString()
            if (input.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input something to encrypt"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val fileName = inputFileName.text.toString()
            if (fileName.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input file name (eg: encryptedFile.txt)"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            if (fileName.contains(".txt").not()) {
                txtErrorMessageEncrypt.text = "Make sure your file name using .txt extension"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageEncrypt.text = "Please input password"
                txtErrorMessageEncrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            IdentityProtection.encryptData(
                input,
                requireActivity().filesDir.absolutePath + "/$fileName",
                password,
                object : OnSFIOEncryptListener<SFIOEncrypt.EncryptData> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptData) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.EncryptData.EncryptDataSuccess -> {
                                txtCipherText.text = "Response: Success, file name: $fileName"
                                txtCipherText.visibility = View.VISIBLE
                                txtErrorMessageEncrypt.visibility = View.GONE
                                isOpen = true
                            }
                            is SFIOEncrypt.EncryptData.EncryptDataFailed -> {
                                txtErrorMessageEncrypt.text = "Failed to encrypt\n\n ErrorLog: ${sFioEncrypt.exception.message}"
                                txtErrorMessageEncrypt.visibility = View.VISIBLE
                                txtCipherText.visibility = View.GONE
                                isOpen = false
                            }
                        }
                    }
                }
            )
        }
    }
}