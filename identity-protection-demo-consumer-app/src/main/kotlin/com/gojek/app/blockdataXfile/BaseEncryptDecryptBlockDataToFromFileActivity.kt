package com.gojek.app.blockdataXfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gojek.app.R

class BaseEncryptDecryptBlockDataToFromFileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_encrypt_decrypt_block_data_x_file)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}