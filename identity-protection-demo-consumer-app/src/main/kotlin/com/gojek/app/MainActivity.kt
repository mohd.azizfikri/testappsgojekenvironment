package com.gojek.app

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import identity.protection.OnRequestScanListener
import identity.protection.ProtectionIdentifier
import identity.protection.ScanAnalysis
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection

class MainActivity : AppCompatActivity(), ProtectionIdentifier {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            BlackBoxLog.d { "MainActivity clicked" }

            IdentityProtection.requestProfileChecks(this, object : OnRequestScanListener {
                override fun onCompleted(scanAnalysis: ScanAnalysis?) {
                    BlackBoxLog.d { "MainActivity#requestScan#onCompleted -> $scanAnalysis" }
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
