package com.gojek.app

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import identity.protection.exception.IdentityProtectionExceptionMessage
import identity.protection.exception.IdentityProtectionIllegalAccessException
import identity.protection.exception.IdentityProtectionNullPointerException
import identity.protection.model.IdentityProtection
import kotlinx.android.synthetic.main.activity_demo.*

class DemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        ctaRefresh()
        showContent()
    }

    private fun ctaRefresh() {
        btnRefresh.setOnClickListener { showContent() }
    }

    /*
     * Function to make Automatic Test in Katalon easier
     * to know that TID is successfully detected
     *
     */
    private fun tidOke() {
        val baseString =  getVKeyTID()
        if(baseString.contains("is not initialize")){
            txtInfoTID.text = "Error"
            txtInfoTID.setTextColor(Color.parseColor("#ffff0000"))
        }else{
            txtInfoTID.text = "TID CONNECTED"
            txtInfoTID.setTextColor(Color.parseColor("#32CD32"))
        }
    }

    /*
     * Get the content from identity-protection-sdk
     * Show content in UI Demo Activity
     *
     */
    @SuppressLint("SetTextI18n")
    private fun showContent() {
        txtVkeyTID.text = getVKeyTID()
        tidOke()
        txtVkeySDKVersion.text = IdentityProtection.getVKeySDKVersion()
        txtFirmwareVersion.text = IdentityProtection.getFirmwareVersion(this)
        txtProcessorVersion.text = IdentityProtection.getProcessorVersion(this)
        showVOSStatus()
        txtFirmwareReturnCode.text = IdentityProtection.getFirmwareReturnCode().toString()
        ctaSimCardCheck()
//        ctaCryptoTA()
//        ctaSecureFileIO()
        ctaFeatureTest()

        if (IdentityProtection.isVOSStarted()) {
            btnRefresh.visibility = View.GONE
            txtSimCardIdentifier.visibility = View.GONE // Make Gone First by ZeeTest
            btnSimCardIdentifier.visibility = View.GONE // Make Gone First by ZeeTest
            dividerSimCardIdentifier.visibility = View.VISIBLE
//            txtCryptoTA.visibility = View.VISIBLE
//            btnCryptoTA.visibility = View.VISIBLE
//            dividerCryptoTA.visibility = View.VISIBLE
//            txtSecureFileIO.visibility = View.VISIBLE
//            btnEncryptDecryptBlockData.visibility = View.VISIBLE
//            btnEncryptDecryptStringFile.visibility = View.VISIBLE
//            btnEncryptDecryptBlockFile.visibility = View.VISIBLE
//            btnEncryptingExistingFile.visibility = View.VISIBLE
            btnOpenFeature.visibility = View.VISIBLE
        } else {
            btnRefresh.visibility = View.VISIBLE
            txtSimCardIdentifier.visibility = View.GONE
            btnSimCardIdentifier.visibility = View.GONE
            dividerSimCardIdentifier.visibility = View.GONE
//            txtCryptoTA.visibility = View.GONE
//            btnCryptoTA.visibility = View.GONE
//            dividerCryptoTA.visibility = View.GONE
//            txtSecureFileIO.visibility = View.GONE
//            btnEncryptDecryptBlockData.visibility = View.GONE
//            btnEncryptDecryptStringFile.visibility = View.GONE
//            btnEncryptDecryptBlockFile.visibility = View.GONE
//            btnEncryptingExistingFile.visibility = View.GONE
            btnOpenFeature.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showVOSStatus() {
        if (IdentityProtection.isVOSStarted()) {
            txtVOSStatus.text = "Running"
            txtVOSStatus.setTextColor(Color.parseColor("#32CD32"))
        } else {
            txtVOSStatus.text = "Not Running"
            txtVOSStatus.setTextColor(Color.parseColor("#ffff0000"))
        }
    }

    private fun ctaSimCardCheck() {
        btnSimCardIdentifier.setOnClickListener {
            startActivity(Intent(this, SimCardCheckActivity::class.java))
        }
    }

    /*private fun ctaCryptoTA() {
        btnCryptoTA.setOnClickListener {
            startActivity(Intent(this, CryptoTAActivity::class.java))
        }
    }

    private fun ctaSecureFileIO() {
        btnEncryptDecryptBlockData.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptBlockDataActivity::class.java))
        }

        btnEncryptDecryptStringFile.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptStringToFromFileActivity::class.java))
        }

        btnEncryptDecryptBlockFile.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptBlockDataToFromFileActivity::class.java))
        }

        btnEncryptingExistingFile.setOnClickListener {
            startActivity(Intent(this, EncryptDecryptExistingFileActivity::class.java))
        }
    }*/

    private fun ctaFeatureTest(){
        btnOpenFeature.setOnClickListener {
            startActivity(Intent(this, FeatureChoiceActivity::class.java))
        }
    }

    private fun getVKeyTID(): String {
        return try {
            IdentityProtection.troubleshootingId()
        } catch (e: IdentityProtectionIllegalAccessException) {
            e.message?.let { errorMessage ->
                when (errorMessage) {
                    IdentityProtectionExceptionMessage.DISABLE.message -> {
                        "V-Key is disabled"
                    }
                    IdentityProtectionExceptionMessage.VOS_NOT_READY.message -> {
                        "V-OS is not ready"
                    }
                    IdentityProtectionExceptionMessage.ACTION_PROFILE_NOT_LOADED.message -> {
                        "Profile is not loaded"
                    }
                    else -> "Generic Exception"
                }
            } ?: "Generic Exception"
        } catch (e: IdentityProtectionNullPointerException) {
            e.message?.let { errorMessage ->
                when (errorMessage) {
                    IdentityProtectionExceptionMessage.NOT_INITIALIZE.message -> {
                        "V-OS is not initialize"
                    }
                    else -> "Generic Exception"
                }
            } ?: "Generic Exception"
        } catch (e: Exception) {
            "Generic Exception"
        }
    }
}
