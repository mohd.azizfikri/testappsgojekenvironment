package com.gojek.app

import android.app.Application
import android.content.Context
import android.os.StrictMode
import identity.protection.HashType
import identity.protection.OnProtectionAnalyzerListener
import identity.protection.ProcessUtils
import identity.protection.ScanAnalysis
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection

internal var isProfileLoaded: Boolean = false

class HostApp : Application() {

    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {
        fun isMainProcess(context: Context): Boolean {
            return try {
                context.packageName == ProcessUtils.getCurrentProcessName(context)
            } catch (e: Exception) {
                false
            }
        }

        fun setupConfig() {
            IdentityProtection.config = IdentityProtection.config.copy(
                isEnabled = true,
                isDebuggable = true,
                strictMode = false,
                virtualSpaceDetection = false,
                allowsArbitraryNetworking = true,
                timeOutInMillis = 10_000L,
                criticalThreats = emptyList(),
                cryptoSignature = IdentityProtection.CryptoSignature(
                    context = this,
                    storeKey = "Gojek_CA_RSA_v1_20201116",
                    hashType = HashType.SHA256
                ),
                onProtectionAnalyzerListener = object : OnProtectionAnalyzerListener {
                    override fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
                        BlackBoxLog.d { "HostApp -> $scanAnalysis" }

                        if (scanAnalysis is ScanAnalysis.ProfileLoaded) {
                            isProfileLoaded = true
                        }
                    }
                }
            )
        }

        fun enableStrictMode() {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build()
            )
        }

        if (isMainProcess(this)) {
//            enableStrictMode()
            setupConfig()
        }
    }
}
