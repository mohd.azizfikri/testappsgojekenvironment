package com.gojek.app

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import identity.protection.ProtectionIdentifier
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.util.concurrent.atomic.AtomicInteger

private const val DELAY: Long = 1000 // 1 second
private const val COUNT_THRESHOLD: Long = 10

class SplashScreenActivity : AppCompatActivity(), ProtectionIdentifier {

    private val handler: Handler = Handler()
    private var runnable: Runnable? = null
    private var count: AtomicInteger = AtomicInteger(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
    }

    override fun onResume() {
        super.onResume()

        object : Runnable {
            @SuppressLint("SetTextI18n")
            override fun run() {
                handler.postDelayed(this, DELAY)

                if (isProfileLoaded) {
                    moveToNextScreen(false)
                } else {
                    txtStartingVOS.text = getString(R.string.starting_v_os) + "... (#${count.getAndIncrement()})"
                }

                // 10 is threshold to let host app move to the next screen
                if (count.get() > COUNT_THRESHOLD) {
                    moveToNextScreen(true)
                }
            }
        }.also {
            runnable = it
            handler.postDelayed(it, DELAY)
        }
    }

    private fun moveToNextScreen(isReachedThreshold: Boolean) {
        handler.removeCallbacks(runnable!!)
        runnable = null

        if (isReachedThreshold.not()) {
            Toast.makeText(
                this, "V-OS has started, enjoy!",
                Toast.LENGTH_SHORT
            ).show()
        }

        startActivity(Intent(this, DemoActivity::class.java))
        finish()
    }
}
