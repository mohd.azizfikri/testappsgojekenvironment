package com.gojek.app.encryptfile

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gojek.app.R
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.OnSFIOUpdatePasswordListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.exception.IdentityProtectionSFIOException
import identity.protection.model.IdentityProtection
import java.io.IOException
import java.io.OutputStreamWriter
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.btnDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.btnEncryptExistingFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.btnRewriteToEncryptedFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.btnUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputDecryptPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputMessageRewriteToEncryptedFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputNewPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputOldPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputPassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.inputPasswordRewriteToEncryptedFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.layoutDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.layoutRewriteToEncryptedFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.layoutUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtDecryptText
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtEncryptDecryptExistingFileDesc
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtEncryptDecryptExistingFileDesc2
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtErrorMessageDecrypt
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtErrorMessageEncryptFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtErrorMessageUpdatePassword
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtMessageRewriteToEncryptedFileError
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtMessageRewriteToEncryptedFileSuccess
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtSuccessMessageEncryptFile
import kotlinx.android.synthetic.main.activity_encrypt_decrypt_existing_file.txtUpdatePasswordSuccess

class EncryptDecryptExistingFileActivity : AppCompatActivity() {

    private var isFileHasBeenCreated: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encrypt_decrypt_existing_file)

        init()
    }

    private fun init() {
        writeInitialFile()
        ctaEncrypt()
        ctaRewriteToEncryptedFile()
        ctaUpdatePassword()
        ctaDecrypt()
    }

    @SuppressLint("SetTextI18n")
    private fun writeInitialFile() {
        try {
            val fileName = "nonEncryptedFile.txt"
            val string = "GoJek_V-Key_POC_Text"

            val fos = openFileOutput(fileName, Context.MODE_PRIVATE)
            val outputWriter = OutputStreamWriter(fos)
            outputWriter.write(string)
            outputWriter.close()
            txtEncryptDecryptExistingFileDesc.visibility = View.VISIBLE
            txtEncryptDecryptExistingFileDesc2.visibility = View.VISIBLE
            isFileHasBeenCreated = true
        } catch (e: IOException) {
            txtErrorMessageEncryptFile.text = "Failed to write initial file"
            txtErrorMessageEncryptFile.visibility = View.VISIBLE
            isFileHasBeenCreated = false
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaEncrypt() {
        btnEncryptExistingFile.setOnClickListener {
            txtErrorMessageEncryptFile.visibility = View.GONE
            txtSuccessMessageEncryptFile.visibility = View.GONE
            layoutRewriteToEncryptedFile.visibility = View.GONE
            layoutUpdatePassword.visibility = View.GONE
            layoutDecrypt.visibility = View.GONE

            if (isFileHasBeenCreated.not()) {
                txtErrorMessageEncryptFile.text = "File is missing, can not continue"
                txtErrorMessageEncryptFile.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageEncryptFile.text = "Please input password"
                txtErrorMessageEncryptFile.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absolutePathFile = this.filesDir.absolutePath + "/nonEncryptedFile.txt"
            IdentityProtection.encryptFile(
                absolutePathFile,
                password,
                object : OnSFIOEncryptListener<SFIOEncrypt.EncryptFile> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptFile) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.EncryptFile.EncryptFileSuccess -> {
                                txtSuccessMessageEncryptFile.text =
                                    "Response: Success, file have been encrypted"
                                txtSuccessMessageEncryptFile.visibility = View.VISIBLE
                                layoutRewriteToEncryptedFile.visibility = View.VISIBLE
                                layoutUpdatePassword.visibility = View.VISIBLE
                                layoutDecrypt.visibility = View.VISIBLE
                            }
                            is SFIOEncrypt.EncryptFile.EncryptFileFailed -> {
                                txtErrorMessageEncryptFile.text =
                                    "Failed to encrypt: ${sFioEncrypt.exception.message}"
                                txtErrorMessageEncryptFile.visibility = View.VISIBLE
                                txtSuccessMessageEncryptFile.visibility = View.GONE
                                layoutRewriteToEncryptedFile.visibility = View.GONE
                                layoutUpdatePassword.visibility = View.GONE
                                layoutDecrypt.visibility = View.GONE
                            }
                        }
                    }
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaRewriteToEncryptedFile() {
        btnRewriteToEncryptedFile.setOnClickListener {
            txtMessageRewriteToEncryptedFileError.visibility = View.GONE
            txtMessageRewriteToEncryptedFileSuccess.visibility = View.GONE

            val input = inputMessageRewriteToEncryptedFile.text.toString()
            if (input.isEmpty()) {
                txtMessageRewriteToEncryptedFileError.text =
                    "Please input message to append or rewrite to the encrypted file"
                txtMessageRewriteToEncryptedFileError.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val password = inputPasswordRewriteToEncryptedFile.text.toString()
            if (password.isEmpty()) {
                txtMessageRewriteToEncryptedFileError.text = "Please input password"
                txtMessageRewriteToEncryptedFileError.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absolutePathFile = this.filesDir.absolutePath + "/nonEncryptedFile.txt"
            IdentityProtection.rewriteToEncryptedFile(
                absolutePathFile,
                input,
                password,
                object : OnSFIOEncryptListener<SFIOEncrypt.WriteToEncryptedFile> {
                    override fun onCompleted(sFioEncrypt: SFIOEncrypt.WriteToEncryptedFile) {
                        when (sFioEncrypt) {
                            is SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileSuccess -> {
                                txtMessageRewriteToEncryptedFileSuccess.text =
                                    "Response: Success writing to encrypted file"
                                txtMessageRewriteToEncryptedFileSuccess.visibility = View.VISIBLE
                            }
                            is SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileFailed -> {
                                txtMessageRewriteToEncryptedFileError.text = "Failed to writing " +
                                        "to encrypted file: ${sFioEncrypt.exception.message}"
                                txtMessageRewriteToEncryptedFileError.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaUpdatePassword() {
        btnUpdatePassword.setOnClickListener {
            txtUpdatePasswordSuccess.visibility = View.GONE
            txtErrorMessageUpdatePassword.visibility = View.GONE

            val oldPassword = inputOldPassword.text.toString()
            if (oldPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input old password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val newPassword = inputNewPassword.text.toString()
            if (newPassword.isEmpty()) {
                txtErrorMessageUpdatePassword.text = "Please input new password"
                txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = this.filesDir.absolutePath + "/nonEncryptedFile.txt"
            IdentityProtection.updatePassword(
                absoluteFileName,
                newPassword,
                oldPassword,
                object : OnSFIOUpdatePasswordListener {
                    override fun onSuccess() {
                        txtUpdatePasswordSuccess.text = "Response: Success update password"
                        txtUpdatePasswordSuccess.visibility = View.VISIBLE
                    }

                    override fun onError(e: IdentityProtectionSFIOException) {
                        txtErrorMessageUpdatePassword.text =
                            "Failed to update password: ${e.message}"
                        txtErrorMessageUpdatePassword.visibility = View.VISIBLE
                    }
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ctaDecrypt() {
        btnDecrypt.setOnClickListener {
            txtErrorMessageDecrypt.visibility = View.GONE
            txtDecryptText.visibility = View.GONE

            val password = inputDecryptPassword.text.toString()
            if (password.isEmpty()) {
                txtErrorMessageDecrypt.text = "Please input password"
                txtErrorMessageDecrypt.visibility = View.VISIBLE
                return@setOnClickListener
            }

            val absoluteFileName: String = this.filesDir.absolutePath + "/nonEncryptedFile.txt"
            IdentityProtection.readFromEncryptedFile(
                absoluteFileName,
                password,
                object : OnSFIODecryptListener<SFIODecrypt.ReadFromDecryptedFile> {
                    override fun onCompleted(sFioDecrypt: SFIODecrypt.ReadFromDecryptedFile) {
                        when (sFioDecrypt) {
                            is SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileSuccess -> {
                                val output = sFioDecrypt.result
                                txtDecryptText.text = getString(R.string.file_content) + " $output"
                                txtDecryptText.visibility = View.VISIBLE
                                txtErrorMessageDecrypt.visibility = View.GONE
                            }
                            is SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileFailed -> {
                                txtErrorMessageDecrypt.text =
                                    "Failed to decrypt: ${sFioDecrypt.exception.message}"
                                txtErrorMessageDecrypt.visibility = View.VISIBLE
                                txtDecryptText.visibility = View.GONE
                            }
                        }
                    }
                }
            )
        }
    }
}
