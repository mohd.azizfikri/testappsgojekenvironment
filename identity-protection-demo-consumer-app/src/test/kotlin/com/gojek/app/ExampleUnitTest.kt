package com.gojek.app

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    fun cobaKalikan(satu: Int, dua:Int): Int{
        return satu * dua
    }

    fun cobaKalikan1(satu: Int, dua:Int): Int{
        return satu * dua
    }


    @Test
    fun addition_isCorrect() {
        assertEquals(10, cobaKalikan(2,5))
        assertEquals(14, cobaKalikan(2,7))
        assertEquals(10, cobaKalikan(5,5))
        assertEquals(10, cobaKalikan1(5,3))
    }

    @Test
    fun addition_isCorrect1() {
        assertEquals(10, cobaKalikan(2,5))
        assertEquals(14, cobaKalikan(2,7))
        assertEquals(10, cobaKalikan(5,5))
        assertEquals(10, cobaKalikan1(5,3))
    }
}