plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
}

//apply("$rootDir/gradle/script-ext.gradle")

android {
    compileSdkVersion(deps.android.build.targetSdkVersion)

    defaultConfig {
        applicationId = "com.gojek.app.dev"
        minSdkVersion(deps.android.build.minSdkVersion)
        targetSdkVersion(deps.android.build.targetSdkVersion)
        consumerProguardFiles("$rootDir/proguard/proguard-rules.pro")
        multiDexEnabled = true
        vectorDrawables.useSupportLibrary = true
        versionCode = 1
        versionName =  "demo-app-1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        // testInstrumentationRunner = "com.gojek.app.ui.main.TestRunnerApp"
        // The following argument makes the Android Test Orchestrator run its
        // "pm clear" command after each test invocation. This command ensures
        // that the app's state is completely cleared between tests.
        //testInstrumentationRunnerArguments = mapOf("clearPackageData" to "true")
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file("debug.keystore")
        }
    }

    sourceSets {
        getByName("main").java.srcDir("src/main/kotlin")
        getByName("test").java.srcDir("src/test/kotlin")
        getByName("androidTest").java.srcDir("src/androidTest/kotlin")
    }

    buildTypes.getByName("debug") {
        isTestCoverageEnabled = true
        isDebuggable = true
    }

    buildTypes.getByName("release") {
        isTestCoverageEnabled = false
        isDebuggable = false
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    lintOptions {
        isAbortOnError = false
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(deps.kotlin.stdlib.core)
    implementation(deps.android.core.coreKtx)
    implementation(deps.android.gson)
    implementation(deps.android.google.playServiceAuth)

    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.2")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.2")

    implementation(files("libs/protection.aar"))
    implementation(files("libs/cryptota.aar"))
    implementation(files("libs/securefileio.aar"))
    implementation(files("libs/processor.aar"))

    implementation(project(":identity-protection-sdk"))

    compileOnly(deps.android.support.annotation)

    testImplementation(deps.android.test.junit)
    androidTestImplementation("androidx.test.ext:junit:1.1.2")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0")
}
