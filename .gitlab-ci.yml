.publish_build_job_template: &publish_module_template
  when: always
  only:
    - tags
  except:
    - branches

stages:
  - pr-check
  - code-quality
  - build
  - publish
  - announce
  - documentation
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  RUBY_IMAGE: artifactory-gojek.golabs.io:6555/cx-ruby:2.7.0
  ANDROID_IMAGE: asia.gcr.io/systems-0001/ubuntu-android-ci-sdk:ubuntu

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches/modules-2

pr-check:
  image: $RUBY_IMAGE
  stage: pr-check
  before_script:
    - gem install bundler
    - gem install danger-checkstyle_format
    - bundle install
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - bundle exec danger --dangerfile=./Dangerfile
  when: always
  tags:
    - k8s
    - android-build
  only:
    - merge_requests

detekt:
  image: $ANDROID_IMAGE
  stage: code-quality
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - ./gradlew -g /cache/.gradle clean && ./gradlew -g check --daemon --parallel -s
  allow_failure: false
  when: always
  tags:
    - k8s
    - android-build

unitTest:
  image: $ANDROID_IMAGE
  stage: code-quality
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-demo-consumer-app:testDebugUnitTest
      :identity-protection-sdk:testDebugUnitTest
  when: always
  artifacts:
    expire_in: 30 days
    reports:
      junit:
        - '**/build/test-results/**/TEST-*.xml'
        - '**/**/build/test-results/**/TEST-*.xml'
    when: always
  tags:
    - k8s
    - android-build

# disabling instrumentation test
#instrumentationTest:
#  image: asia.gcr.io/systems-0001/ubuntu-android-ci-sdk
#  stage: code-quality
#  script:
#    - export BUILD_NUMBER=$CI_BUILD_ID
#    - echo "$FIREBASE_JSON_CREDENTIALS" > "$(pwd)/firebase-credentials.json"
#    - ./gradlew createJacocoFtlReport
#  artifacts:
#    expire_in: 30 days
#    paths:
#      - "build/flank/"
#      - "**/build/reports/**"
#    reports:
#      junit:
#        - "build/flank/results/JUnitReport.xml"
#    when: always
#  tags:
#    - k8s
#    - android-build

release:
  image: $ANDROID_IMAGE
  stage: build
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-demo-consumer-app:assembleDebug
      :identity-protection-sdk:assembleDebug
      :identity-protection-no-op-sdk:assembleDebug
  dependencies:
    - detekt
    - unitTest
  tags:
    - android-build
    - k8s

debug:
  image: $ANDROID_IMAGE
  stage: build
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-demo-consumer-app:assembleDebug
      :identity-protection-sdk:assembleDebug -PisDebug
      :identity-protection-no-op-sdk:assembleDebug
  dependencies:
    - detekt
    - unitTest
  tags:
    - android-build
    - k8s

publishRelease:
  image: $ANDROID_IMAGE
  stage: publish
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-sdk:assembleRelease
      :identity-protection-no-op-sdk:assembleRelease
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-sdk:artifactoryPublish
      :identity-protection-no-op-sdk:artifactoryPublish
  dependencies:
    - release
  tags:
    - android-build
    - k8s

publishDebug:
  image: $ANDROID_IMAGE
  stage: publish
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-sdk:assembleRelease -PisDebug
    - >
      ./gradlew -g /cache/.gradle clean
      :identity-protection-sdk:artifactoryPublish -PisDebug
  dependencies:
    - debug
  tags:
    - android-build
    - k8s

announce-snapshot:
  image: $ANDROID_IMAGE
  stage: announce
  script:
    - ./scripts/publish_release_changelog.sh
    - ./scripts/distribute_release_changelog.sh
  dependencies:
    - publishRelease
    - publishDebug
  when: manual
  only:
    - master

dokka:
  image: $ANDROID_IMAGE
  stage: documentation
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - >
      ./gradlew -g /cache/.gradle clean dokkaHtmlMultiModule --daemon --parallel
  except:
    - triggers
  artifacts:
    paths:
      - docs/api/
    expire_in: 30 days
  tags:
    - k8s
    - android-build
  dependencies:
    - publishRelease
  only:
    - master

hugo:
  image: monachus/hugo
  stage: documentation
  script:
    - export BUILD_NUMBER=$CI_BUILD_ID
    - cd docs/web
    - hugo -D
  except:
    - triggers
  artifacts:
    paths:
      - docs/web/
    expire_in: 30 days
  tags:
    - k8s
    - android-build
  dependencies:
    - publishRelease
  only:
    - master

pages:
  stage: deploy
  script:
    - mv docs/ public/
  artifacts:
    paths:
      - public
  allow_failure: true
  dependencies:
    - dokka
    - hugo
  only:
    - master

announce:
  <<: *publish_module_template
  image: asia.gcr.io/systems-0001/ubuntu-android-ci-sdk
  stage: announce
  script:
    - ./scripts/publish_release_changelog.sh
    - ./scripts/distribute_release_changelog.sh
  dependencies:
    - publishRelease
