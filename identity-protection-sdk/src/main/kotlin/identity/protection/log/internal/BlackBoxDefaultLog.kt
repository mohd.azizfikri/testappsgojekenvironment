package identity.protection.log.internal

import android.util.Log
import identity.protection.log.BlackBoxLog.Logger
import identity.protection.model.IdentityProtection

private const val THRESHOLD = 4000

internal class BlackBoxDefaultLog : Logger {

    override fun d(message: String) {
        if (IdentityProtection.config.isDebuggable.not()) return

        if (message.length > THRESHOLD) {
            message.split(NEW_LINE_REGEX).forEach { line -> print(line) }
        } else {
            val lines: List<String> = message.split(NEW_LINE_REGEX)
            if (lines.isEmpty()) {
                print(message)
            } else {
                lines.forEach { print(it) }
            }
        }
    }

    override fun d(throwable: Throwable, message: String) {
        if (IdentityProtection.config.isDebuggable.not()) return

        print("$message --> ${Log.getStackTraceString(throwable)}")
    }

    private fun print(message: String) {
        println("BlackBox : $message")
    }

    companion object {
        val NEW_LINE_REGEX = "\n".toRegex()
    }
}
