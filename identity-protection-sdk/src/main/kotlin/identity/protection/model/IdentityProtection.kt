package identity.protection.model

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.Looper
import com.vkey.android.vguard.ActivityLifecycleHook
import com.vkey.android.vguard.LocalBroadcastManager
import com.vkey.android.vguard.MemoryConfiguration
import com.vkey.android.vguard.VGException
import com.vkey.android.vguard.VGuard
import com.vkey.android.vguard.VGuardBroadcastReceiver.ACTION_SCAN_COMPLETE
import com.vkey.android.vguard.VGuardBroadcastReceiver.VOS_READY
import com.vkey.android.vguard.VGuardFactory
import com.vkey.android.vguard.VGuardLifecycleHook
import com.vkey.securefileio.FileInputStream
import com.vkey.securefileio.FileOutputStream
import com.vkey.securefileio.SecureFileIO
import identity.protection.Firmware
import identity.protection.Firmware.NEGATIVE
import identity.protection.Firmware.POSITIVE
import identity.protection.Firmware.UNDEFINED
import identity.protection.HashType
import identity.protection.OnProtectionAnalyzerListener
import identity.protection.OnRequestScanListener
import identity.protection.OnSFIODecryptListener
import identity.protection.OnSFIODeleteFileListener
import identity.protection.OnSFIOEncryptListener
import identity.protection.OnSFIOUpdatePasswordListener
import identity.protection.SFIODecrypt
import identity.protection.SFIOEncrypt
import identity.protection.ScanAnalysis
import identity.protection.exception.IdentityProtectionExceptionMessage.ACTION_PROFILE_NOT_LOADED
import identity.protection.exception.IdentityProtectionExceptionMessage.DISABLE
import identity.protection.exception.IdentityProtectionExceptionMessage.FAILED_INITIALIZE
import identity.protection.exception.IdentityProtectionExceptionMessage.FAILED_INITIALIZE_LIFECYCLE_HOOK
import identity.protection.exception.IdentityProtectionExceptionMessage.FAILED_INITIALIZE_VGUARD
import identity.protection.exception.IdentityProtectionExceptionMessage.METHOD_THREAD
import identity.protection.exception.IdentityProtectionExceptionMessage.NOT_INITIALIZE
import identity.protection.exception.IdentityProtectionExceptionMessage.SIGNATURE_NOT_INITIALIZE
import identity.protection.exception.IdentityProtectionExceptionMessage.VOS_NOT_READY
import identity.protection.exception.IdentityProtectionIllegalAccessException
import identity.protection.exception.IdentityProtectionInitializationException
import identity.protection.exception.IdentityProtectionNullPointerException
import identity.protection.exception.IdentityProtectionSFIOException
import identity.protection.internal.BlackBoxLifecycleObserver
import identity.protection.internal.BlackBoxSignature
import identity.protection.internal.BlackBoxSignatureImpl
import identity.protection.internal.ACTION_PROFILE_LOADED
import identity.protection.internal.AppExecutor
import identity.protection.internal.AppExecutorImpl
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection.sign
import identity.protection.model.IdentityProtection.troubleshootingId
import identity.protection.responsecode.IdentityProtectionResponse
import java.io.File
import vkey.android.vos.VosWrapper
import java.io.IOException
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong

private const val VOS_IS_NOT_RUNNING = "V-OS is not running"

/**
 * A class that provide sets of APIs for
 *
 * - Gets the [troubleshootingId] of the device from V-OS
 * - [sign]ing mechanism for the given message
 * - [requestProfileChecks] to starts V-OS App Protection profile loaded when conditions are met
 * - [destroy] to perform any final cleanup
 */
object IdentityProtection {

    /**
     * [Config] is used to control the usage of Identity Protection SDK
     *
     * @param isEnabled [Boolean] set to true to enable the Identity Protection SDK
     * @param isDebuggable [Boolean] set to true for development purpose, you have to disable it for production app
     * @param strictMode [Boolean] set to true for strict release mode, this will lock VOS when critical threat detected
     * @param virtualSpaceDetection [Boolean] set to true if you expect to blocked the clone app of your application
     * @param allowsArbitraryNetworking [Boolean] set whether to allow arbitrary networking by enabling/disabling SSL pinning
     * @param timeOutInMillis [Long] threshold-time for Identity Protection SDK to trigger the callback to upstream when [requestProfileChecks] is called and time is exceeded
     * @param criticalThreats List of [Int] critical threat id, set to empty list if you won't blocked any threat detected. Depends on the value of [strictMode] param
     * @param cryptoSignature [CryptoSignature] required only when your application want to enable Signature Verification feature
     * @param onProtectionAnalyzerListener [OnProtectionAnalyzerListener] an interface class, used for analytics if required
     */
    data class Config(
        val isEnabled: Boolean = false,
        val isDebuggable: Boolean = false,
        val strictMode: Boolean = false,
        val virtualSpaceDetection: Boolean = false,
        val allowsArbitraryNetworking: Boolean = false,
        val timeOutInMillis: Long = 10_000L,
        val criticalThreats: List<Int>? = emptyList(),
        val cryptoSignature: CryptoSignature? = null,
        val onProtectionAnalyzerListener: OnProtectionAnalyzerListener? = null
    )

    /**
     * [CryptoSignature] is used to enable Signature Verification feature
     *
     * @param context [Context]
     * @param storeKey [String] Predefine alias that VKey provided along with assets generation
     * @param trustedTimeServerUrl [String] Predefine alias that VKey provided along with assets generation
     * @param hashType [HashType] SHA Type to be used for signing the message
     */
    data class CryptoSignature(
        val context: Context,

        /**
         * Predefine alias that VKey provided along with assets generation
         */
        val storeKey: String,

        /**
         * Predefine alias that VKey provided along with assets generation
         */
        val trustedTimeServerUrl: String? = null,

        /**
         * SHA Type to be used for signing the message
         */
        val hashType: HashType
    )

    /**
     * IdentityProtection configuration data class. Properties can be updated via copy
     *
     * **Example**
     * ```kotlin
     * IdentityProtection.config = IdentityProtection.config.copy(
     *      isEnabled = true,
     *      isDebuggable = false,
     *      strictMode = false,
     *      virtualSpaceDetection = true,
     *      timeOutInMillis = 10_000L,
     *      criticalThreats = listOf(1000, 3000),
     *      cryptoSignature = IdentityProtection.CryptoSignature(
     *              context = this,
     *              storeKey = "MY_STORE_KEY",
     *              hashType = HashType.SHA256
     *      ),
     *      onProtectionAnalyzerListener = object : OnProtectionAnalyzerListener {
     *           override fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
     *               // Your requirement
     *           }
     *      }
     * )
     * ```
     *
     * @return config [Config]
     */
    @Volatile
    var config: Config = Config()
        set(newConfig) {
            val previousConfig = field
            field = newConfig
            logConfigChange(previousConfig, newConfig)
        }

    /**
     * VGuardManager instance which implements the VGuard interface
     * for the host app to access its APIs.
     *
     * @return [VGuard]
     */
    @Volatile
    internal var vGuard: VGuard? = null
        private set

    /**
     * V-OSAppProtection needs to be aware of the activity lifecycle.
     * Hook it up with the [VGuardLifecycleHook] object in
     * [Application.ActivityLifecycleCallbacks.onActivityResumed]
     * [Application.ActivityLifecycleCallbacks.onActivityPaused]
     */
    @Volatile
    internal var hook: VGuardLifecycleHook? = null
        private set

    /**
     * An initialization time when the process is about to start
     *
     * set to 0 when [BlackBoxLifecycleObserver.onDestroy] is triggered
     */
    @Volatile
    internal var startTime: AtomicLong = AtomicLong(0)
        private set

    /**
     * An object to define that VOS is ready to use or not
     */
    @Volatile
    private var firmwareReturnCode: Long = -1

    /**
     * An object to define that VOS is ready to use or not
     */
    @Volatile
    private var firmware: Firmware = UNDEFINED

    /**
     * A status for [ACTION_PROFILE_LOADED]
     * true when [ACTION_PROFILE_LOADED] broadcast successful, otherwise false
     */
    @Volatile
    private var isProfileLoaded: AtomicBoolean = AtomicBoolean(false)

    /**
     * A status for initialization VGuard instance
     * true when "in-process", otherwise false
     */
    @Volatile
    internal var isSetupVGuardRunning: AtomicBoolean = AtomicBoolean(false)

    /**
     * An internal class for handling the signing mechanism that using CryptoTA internally
     *
     * @see [CryptoSignature]
     */
    @Volatile
    private var signature: BlackBoxSignature? = null

    /**
     * An interface for composition concrete implementation of executors
     */
    private val appExecutor: AppExecutor = AppExecutorImpl()

    /**
     * This API gets the V-Key SDK Version
     *
     * @return [String] If V-OS is started otherwise "V-OS is not running"
     */
    fun getVKeySDKVersion(): String = vGuard?.sdkVersion() ?: VOS_IS_NOT_RUNNING

    /**
     * This API gets the Firmware Version
     *
     * @return [String] If V-OS is started otherwise "V-OS is not running"
     */
    fun getFirmwareVersion(context: Context): String = vGuard?.let {
        VosWrapper.getInstance(context).firmwareVersion
    } ?: VOS_IS_NOT_RUNNING

    /**
     * This API gets the Processor Version
     *
     * @return [String] If V-OS is started otherwise "V-OS is not running"
     */
    fun getProcessorVersion(context: Context): String = vGuard?.let {
        VosWrapper.getInstance(context).processorVersion
    } ?: VOS_IS_NOT_RUNNING

    /**
     * This API will determine whether V-OS is started or not
     *
     * @return [Boolean] true when V-OS is started otherwise false
     */
    fun isVOSStarted(): Boolean = vGuard?.isVosStarted ?: false

    /**
     * This API gets the Firmware Return code
     *
     * @return [Long] Positive value ( >= 0 ) when V-OS is started otherwise Negative value ( < 0 )
     */
    fun getFirmwareReturnCode(): Long = firmwareReturnCode

    /**
     * This API for encrypting plain text which run in io thread and
     * sending back a cipher text to the caller using callback in main thread.
     *
     * **Usage**
     * ```kotlin
     *
     * var cipherText: ByteArray? = null
     * val input: String = "Hello World"
     * IdentityProtection.encrypt(input, object : OnSFIOEncryptListener<SFIOEncrypt.EncryptBlockOfData> {
     *      override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptBlockOfData) {
     *          when (sFioEncrypt) {
     *              is SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataSuccess -> {
     *                  cipherText = sFioEncrypt.result
     *                  // Your requirement
     *              }
     *              is SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataFailed -> {
     *                  // Your requirement
     *              }
     *          }
     *      }
     *  })
     * ```
     *
     * @param input [String] to be encrypted
     * @param onSFIOEncryptListener [OnSFIOEncryptListener] a callback completion
     *
     * @see [decrypt] for decrypt cipher text to plain text
     * @see [SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataSuccess]
     * @see [SFIOEncrypt.EncryptBlockOfData.EncryptBlockOfDataFailed]
     * @see [AppExecutor]
     */
    fun encrypt(
        input: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptBlockOfData>
    ) {
        runInIoThread {
            val result: SFIOEncrypt.EncryptBlockOfData = try {
                SecureFileIO.encryptData(input.toByteArray(Charsets.UTF_8))
                    .let(SFIOEncrypt.EncryptBlockOfData::EncryptBlockOfDataSuccess)
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIOEncrypt.EncryptBlockOfData::EncryptBlockOfDataFailed)
            }

            runInUiThread { result.let(onSFIOEncryptListener::onCompleted) }
        }
    }

    /**
     * This API for decrypting block of data which run in io thread and
     * sending back a plain text to the caller using callback in main thread.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.decrypt(cipherText, object : OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData> {
     *      override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptBlockOfData) {
     *          when (sFioDecrypt) {
     *              is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess -> {
     *                  val output = sFioDecrypt.result
     *                  // Your requirement
     *              }
     *              is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed -> {
     *                  // Your requirement
     *              }
     *           }
     *      }
     * })
     * ```
     *
     * @param cipher [ByteArray] to be decrypted
     * @param onSFIODecryptListener [OnSFIODecryptListener] a callback completion
     *
     * @see [encrypt] to encrypt plain text to cipher text
     * @see [SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess]
     * @see [SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed]
     * @see [AppExecutor]
     */
    fun decrypt(
        cipher: ByteArray,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData>
    ) {
        runInIoThread {
            val result: SFIODecrypt.DecryptBlockOfData = try {
                SecureFileIO.decryptData(cipher).toString(Charsets.UTF_8)
                    .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataSuccess)
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataFailed)
            }

            runInUiThread { result.let(onSFIODecryptListener::onCompleted) }
        }
    }

    /**
     * This API for encrypting String to File which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.encryptString(
     *      input,
     *      this.filesDir.absolutePath + "/$fileName",
     *      password,
     *      object : OnSFIOEncryptListener<SFIOEncrypt.EncryptString> {
     *          override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptString) {
     *              when (sFioEncrypt) {
     *                  is SFIOEncrypt.EncryptString.EncryptStringSuccess -> {
     *                      // Your requirement
     *                  }
     *                  is SFIOEncrypt.EncryptString.EncryptStringFailed -> {
     *                      // Your requirement
     *                  }
     *              }
     *          }
     *      }
     *  )
     * ```
     *
     * @param input [String] to be encrypted
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIOEncryptListener [OnSFIOEncryptListener] a callback completion
     *
     * @see [decryptString] to decrypt content from encrypted file
     * @see [SFIOEncrypt.EncryptString.EncryptStringSuccess]
     * @see [SFIOEncrypt.EncryptString.EncryptStringFailed]
     * @see [AppExecutor]
     */
    fun encryptString(
        input: String,
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptString>
    ) {
        runInIoThread {
            val result: SFIOEncrypt.EncryptString = try {
                // the last parameter to `true` will write the file atomically.
                SecureFileIO.encryptString(input, filePath, password, true)
                SFIOEncrypt.EncryptString.EncryptStringSuccess
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIOEncrypt.EncryptString::EncryptStringFailed)
            }

            runInUiThread { result.let(onSFIOEncryptListener::onCompleted) }
        }
    }

    /**
     * This API for decrypt content from encrypted file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.decryptString(
     *      absoluteFileName,
     *      password,
     *      object : OnSFIODecryptListener<SFIODecrypt.DecryptString> {
     *          override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptString) {
     *              when (sFioDecrypt) {
     *                  is SFIODecrypt.DecryptString.DecryptStringSuccess -> {
     *                      val output = sFioDecrypt.result
     *                      // Your requirement
     *                  }
     *                  is SFIODecrypt.DecryptString.DecryptStringFailed -> {
     *                      // Your requirement
     *                  }
     *              }
     *           }
     *      }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIODecryptListener [OnSFIODecryptListener] a callback completion
     *
     * @see [encryptString] to encrypt string into the file
     * @see [SFIODecrypt.DecryptString.DecryptStringSuccess]
     * @see [SFIODecrypt.DecryptString.DecryptStringFailed]
     * @see [AppExecutor]
     */
    fun decryptString(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptString>
    ) {
        runInIoThread {
            val result: SFIODecrypt.DecryptString = try {
                SecureFileIO.decryptString(filePath, password)
                    .let(SFIODecrypt.DecryptString::DecryptStringSuccess)
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIODecrypt.DecryptString::DecryptStringFailed)
            }

            runInUiThread { result.let(onSFIODecryptListener::onCompleted) }
        }
    }

    /**
     * This API for encrypting block of data to encrypted file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.encryptData(
     *      input,
     *      this.filesDir.absolutePath + "/$fileName",
     *      password,
     *      object : OnSFIOEncryptListener<SFIOEncrypt.EncryptData> {
     *          override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptData) {
     *              when (sFioEncrypt) {
     *                  is SFIOEncrypt.EncryptData.EncryptDataSuccess -> {
     *                      // Your requirement
     *                  }
     *                  is SFIOEncrypt.EncryptData.EncryptDataFailed -> {
     *                      // Your requirement
     *                  }
     *               }
     *           }
     *       }
     * )
     * ```
     *
     * @param input [String] to be encrypted
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIOEncryptListener [OnSFIOEncryptListener] a callback completion
     *
     * @see [decryptFile] to decrypt content from encrypted file
     * @see [SFIOEncrypt.EncryptData.EncryptDataSuccess]
     * @see [SFIOEncrypt.EncryptData.EncryptDataFailed]
     * @see [AppExecutor]
     */
    fun encryptData(
        input: String,
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptData>
    ) {
        runInIoThread {
            val result: SFIOEncrypt.EncryptData = try {
                SecureFileIO.encryptData(
                    input.toByteArray(Charsets.UTF_8),
                    filePath,
                    password,
                    true
                )
                SFIOEncrypt.EncryptData.EncryptDataSuccess
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIOEncrypt.EncryptData::EncryptDataFailed)
            }

            runInUiThread { result.let(onSFIOEncryptListener::onCompleted) }
        }
    }

    /**
     * This API for decrypting content from encrypted file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.decryptFile(
     *      absoluteFileName,
     *      password,
     *      object : OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData> {
     *          override fun onCompleted(sFioDecrypt: SFIODecrypt.DecryptBlockOfData) {
     *              when (sFioDecrypt) {
     *                  is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess -> {
     *                      val output = sFioDecrypt.result
     *                      // Your requirement
     *                  }
     *                  is SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed -> {
     *                      // Your requirement
     *                  }
     *              }
     *          }
     *      }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIODecryptListener [OnSFIODecryptListener] a callback completion
     *
     * @see [encryptData] to encrypt string into the file
     * @see [SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess]
     * @see [SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed]
     * @see [AppExecutor]
     */
    fun decryptFile(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.DecryptBlockOfData>
    ) {
        runInIoThread {
            val result: SFIODecrypt.DecryptBlockOfData = try {
                SecureFileIO.decryptFile(filePath, password).toString(Charsets.UTF_8)
                    .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataSuccess)
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIODecrypt.DecryptBlockOfData::DecryptBlockOfDataFailed)
            }

            runInUiThread { result.let(onSFIODecryptListener::onCompleted) }
        }
    }

    /**
     * This API for encrypting existing file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.encryptFile(
     *      absolutePathFile,
     *      password,
     *      object : OnSFIOEncryptListener<SFIOEncrypt.EncryptFile> {
     *          override fun onCompleted(sFioEncrypt: SFIOEncrypt.EncryptFile) {
     *              when (sFioEncrypt) {
     *                  is SFIOEncrypt.EncryptFile.EncryptFileSuccess -> {
     *                      // Your requirement
     *                  }
     *                  is SFIOEncrypt.EncryptFile.EncryptFileFailed -> {
     *                      // Your requirement
     *                  }
     *               }
     *           }
     *       }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIOEncryptListener [OnSFIOEncryptListener] a callback completion
     *
     * @see [readFromEncryptedFile] to read content from encrypted file
     * @see [SFIOEncrypt.EncryptFile.EncryptFileSuccess]
     * @see [SFIOEncrypt.EncryptFile.EncryptFileFailed]
     * @see [AppExecutor]
     */
    fun encryptFile(
        filePath: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.EncryptFile>
    ) {
        runInIoThread {
            val result: SFIOEncrypt.EncryptFile = try {
                SecureFileIO.encryptFile(filePath, password)
                SFIOEncrypt.EncryptFile.EncryptFileSuccess
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIOEncrypt.EncryptFile::EncryptFileFailed)
            }

            runInUiThread { result.let(onSFIOEncryptListener::onCompleted) }
        }
    }

    /**
     * This API for re-writing file content in existing file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.rewriteToEncryptedFile(
     *      absolutePathFile,
     *      input,
     *      password,
     *      object : OnSFIOEncryptListener<SFIOEncrypt.WriteToEncryptedFile> {
     *          override fun onCompleted(sFioEncrypt: SFIOEncrypt.WriteToEncryptedFile) {
     *              when (sFioEncrypt) {
     *                  is SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileSuccess -> {
     *                      // Your requirement
     *                  }
     *                  is SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileFailed -> {
     *                      // Your requirement
     *                  }
     *               }
     *           }
     *      }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param input [String] to be encrypted
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIOEncryptListener [OnSFIOEncryptListener] a callback completion
     *
     * @see [encryptFile] to encrypt existing file
     * @see [readFromEncryptedFile] to read content from encrypted file
     * @see [SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileSuccess]
     * @see [SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileFailed]
     * @see [AppExecutor]
     */
    fun rewriteToEncryptedFile(
        filePath: String,
        input: String,
        password: String,
        onSFIOEncryptListener: OnSFIOEncryptListener<SFIOEncrypt.WriteToEncryptedFile>
    ) {
        runInIoThread {
            val result: SFIOEncrypt.WriteToEncryptedFile = try {
                val oStream = FileOutputStream(filePath, password)
                oStream.write(input.toByteArray())
                oStream.close()
                SFIOEncrypt.WriteToEncryptedFile.WriteToEncryptedFileSuccess
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIOEncrypt.WriteToEncryptedFile::WriteToEncryptedFileFailed)
            }

            runInUiThread { result.let(onSFIOEncryptListener::onCompleted) }
        }
    }

    /**
     * This API for read file content in existing file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.readFromEncryptedFile(
     *      absoluteFileName,
     *      password,
     *      object : OnSFIODecryptListener<SFIODecrypt.ReadFromDecryptedFile> {
     *          override fun onCompleted(sFioDecrypt: SFIODecrypt.ReadFromDecryptedFile) {
     *              when (sFioDecrypt) {
     *                  is SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileSuccess -> {
     *                      // Your requirement
     *                  }
     *                  is SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileFailed -> {
     *                      // Your requirement
     *                  }
     *              }
     *           }
     *      }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param password [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param onSFIODecryptListener [OnSFIODecryptListener] a callback completion
     *
     * @see [encryptFile] to encrypt existing file
     * @see [rewriteToEncryptedFile] to re-write content from existing file
     * @see [SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileSuccess]
     * @see [SFIODecrypt.ReadFromDecryptedFile.ReadFromDecryptedFileFailed]
     * @see [AppExecutor]
     */
    fun readFromEncryptedFile(
        filePath: String,
        password: String,
        onSFIODecryptListener: OnSFIODecryptListener<SFIODecrypt.ReadFromDecryptedFile>
    ) {
        runInIoThread {
            val result: SFIODecrypt.ReadFromDecryptedFile = try {
                FileInputStream(filePath, password).readBytes().toString(Charsets.UTF_8)
                    .let(SFIODecrypt.ReadFromDecryptedFile::ReadFromDecryptedFileSuccess)
            } catch (e: IOException) {
                IdentityProtectionSFIOException(e.stackTraceToString())
                    .let(SFIODecrypt.ReadFromDecryptedFile::ReadFromDecryptedFileFailed)
            }

            runInUiThread { result.let(onSFIODecryptListener::onCompleted) }
        }
    }

    /**
     * This API for updating password which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     *
     * **Usage**
     * ```kotlin
     * IdentityProtection.updatePassword(
     *      absoluteFileName,
     *      newPassword,
     *      oldPassword,
     *      object : OnSFIOUpdatePasswordListener {
     *          override fun onSuccess() {
     *              // Your requirement
     *          }
     *          override fun onError(e: IOException) {
     *              // Your requirement
     *          }
     *      }
     * )
     * ```
     *
     * @param filePath [String] path of the file, including the .txt extension (eg: encryptedFile.txt)
     * @param newPassword [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * If you do not wish to set a password, use an empty string like "" instead
     *
     * @param oldPassword [String] unique character, A valid password must contain:
     *
     * -at least one digit
     *
     * -at least one lowercase character
     *
     * -at least one uppercase character
     *
     * -at least one symbols from !#$%^&amp;
     *
     * -at least with a length of 6 up to a maximum of 20
     *
     * @param onSFIOUpdatePasswordListener [OnSFIOUpdatePasswordListener] a callback completion
     *
     * @see [AppExecutor]
     */
    fun updatePassword(
        filePath: String,
        newPassword: String,
        oldPassword: String,
        onSFIOUpdatePasswordListener: OnSFIOUpdatePasswordListener
    ) {
        runInIoThread {
            try {
                SecureFileIO.updateFile(filePath, newPassword, oldPassword)
                runInUiThread { onSFIOUpdatePasswordListener.onSuccess() }
            } catch (e: IOException) {
                runInUiThread {
                    IdentityProtectionSFIOException(e.stackTraceToString())
                        .let(onSFIOUpdatePasswordListener::onError)
                }
            }
        }
    }

    /**
     * This API for deleting file which run in io thread and
     * trigger a callback in main thread whether it is success or not.
     * Deletes the file or directory denoted by this abstract pathname.
     * If this pathname denotes a directory,
     * then the directory must be empty in order to be deleted
     *
     * @param path [String]
     * @param onSFIODeleteFileListener [OnSFIODeleteFileListener]
     *
     * @see [AppExecutor]
     */
    fun deleteFile(
        path: String,
        onSFIODeleteFileListener: OnSFIODeleteFileListener
    ) {
        runInIoThread {
            try {
                if (File(path).delete()) {
                    runInUiThread { onSFIODeleteFileListener.onSuccess() }
                } else {
                    runInUiThread { onSFIODeleteFileListener.onError() }
                }
            } catch (e: SecurityException) {
                runInUiThread {
                    IdentityProtectionSFIOException(e.stackTraceToString())
                        .let(onSFIODeleteFileListener::onError)
                }
            } catch (e: IOException) {
                runInUiThread {
                    IdentityProtectionSFIOException(e.stackTraceToString())
                        .let(onSFIODeleteFileListener::onError)
                }
            }
        }
    }

    /**
     * This API gets the troubleshooting ID of the device from V-OS.
     *
     * @return [String]
     *
     * @throws [IdentityProtectionIllegalAccessException] If IdentityProtection is not enabled
     * or V-OS is not ready
     * @throws [IdentityProtectionNullPointerException]
     * If IdentityProtectionSDK is not initialize yet
     *
     * @see [Config]
     * @see [VGuard]
     */
    @Throws(
        IdentityProtectionIllegalAccessException::class,
        IdentityProtectionNullPointerException::class
    )
    fun troubleshootingId(): String {
        if (config.isEnabled.not()) {
            throw IdentityProtectionIllegalAccessException(DISABLE)
        }

        if (vGuard == null) {
            throw IdentityProtectionNullPointerException(NOT_INITIALIZE)
        }

        if (firmware == UNDEFINED || firmware == NEGATIVE) {
            throw IdentityProtectionIllegalAccessException(VOS_NOT_READY)
        }

        if (isProfileLoaded.get().not()) {
            throw IdentityProtectionIllegalAccessException(ACTION_PROFILE_NOT_LOADED)
        }

        return vGuard!!.troubleshootingId.orEmpty()
    }

    /**
     * Signing given a plain-text that using CryptoTA internally and return [ByteArray]
     *
     * @param message [String] that need to be signed
     *
     * @return [ByteArray] If the given message successfully signed
     * @return NULL If the given message is failed to signed
     *
     * @throws [IdentityProtectionIllegalAccessException] If IdentityProtection is not enabled
     * or V-OS is not ready or its getting called in main thread
     * @throws [IdentityProtectionNullPointerException] If IdentityProtectionSDK or Signature
     * is not initialize yet
     *
     * @see [Config]
     * @see [VGuard]
     * @see [BlackBoxSignature]
     */
    @Throws(
        IdentityProtectionIllegalAccessException::class,
        IdentityProtectionNullPointerException::class
    )
    fun sign(message: String): ByteArray? {
        if (config.isEnabled.not()) {
            throw IdentityProtectionIllegalAccessException(DISABLE)
        }

        if (vGuard == null) {
            throw IdentityProtectionNullPointerException(NOT_INITIALIZE)
        }

        if (firmware == UNDEFINED || firmware == NEGATIVE) {
            throw IdentityProtectionIllegalAccessException(VOS_NOT_READY)
        }

        if (isProfileLoaded.get().not()) {
            throw IdentityProtectionIllegalAccessException(ACTION_PROFILE_NOT_LOADED)
        }

        if (signature == null) {
            throw IdentityProtectionNullPointerException(SIGNATURE_NOT_INITIALIZE)
        }

        if (Thread.currentThread() == Looper.getMainLooper().thread) {
            throw IdentityProtectionIllegalAccessException(METHOD_THREAD)
        }

        return signature!!.sign(message)
    }

    /**
     * This API starts V-OS App Protection profile loaded when conditions are met
     *
     * The results are returned through broadcast with the broadcast intent
     * of [ACTION_PROFILE_LOADED]
     *
     * @param context [Context]
     * @param callback [OnRequestScanListener]
     *
     * @see [Config]
     * @see [VGuard]
     * @see [CryptoSignature]
     * @see [BlackBoxSignature]
     */
    fun requestProfileChecks(context: Context, callback: OnRequestScanListener) {
        if (config.isEnabled.not()) {
            BlackBoxLog.d { "requestProfileChecks#IdentityProtection SDK is disabled" }
            callback.onCompleted()
            return
        }

        if (firmware == NEGATIVE) {
            BlackBoxLog.d { "requestProfileChecks#VOSCanNotBeUsed" }
            callback.onCompleted()
            return
        }

        if (isVOSReadyToUse()) {
            BlackBoxLog.d { "requestProfileChecks#VOSReadyToUse" }
            callback.onCompleted()
            return
        }

        val startTime = System.currentTimeMillis()
        val receiver = LocalBroadcastReceiver(
            context,
            IntentFilter(ACTION_PROFILE_LOADED),
            startTime
        )
        receiver.register(callback)

        executeInitialization("requestProfileChecks", context, callback)
    }

    /**
     * Perform any final cleanup. This can happen either because the activity is finishing
     * or because the system is temporarily destroying this instance of the activity to save space.
     */
    fun destroy() {
        BlackBoxLog.d { "VGuard#Destroyed" }
        firmwareReturnCode = -1
        firmware = UNDEFINED
        setProfileLoaded(false)
        signature?.close()
        vGuard?.destroy()
        vGuard = null
        signature = null
        startTime.set(0)
    }

    /**
     * Initialize VGuard, called when onActivityCreated
     * triggered by [BlackBoxLifecycleObserver.onCreate]
     *
     * @see [Config]
     * @see [VGuard]
     */
    internal fun initializeVGuard(context: Context) {
        if (config.isEnabled.not()) {
            BlackBoxLog.d { "IdentityProtection SDK is disabled" }
            return
        }

        executeInitialization("VGuard#Initialize", context)
    }

    /**
     * Initialize Signature Verification feature.
     *
     * For better security, App should wait for [ACTION_SCAN_COMPLETE] event
     *
     * @see [Config]
     * @see [CryptoSignature]
     * @see [BlackBoxSignature]
     */
    internal fun initializeBlackBoxSignature() {
        if (signature != null || config.cryptoSignature == null) {
            return
        }

        signature = BlackBoxSignatureImpl(config.cryptoSignature!!)
    }

    /**
     * [onScanAnalyzed] Called when Identity Protection SDK found an error during
     * the initialization triggered by [VOS_READY]
     * or when the scanning process is completed triggered by [ACTION_SCAN_COMPLETE]
     */
    internal fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
        config.onProtectionAnalyzerListener?.onScanAnalyzed(scanAnalysis)
    }

    /**
     * [updateVosReady] Called when [VOS_READY] is triggered
     *
     * @param firmwareReturnCodeValue [Long]
     */
    internal fun updateVosReady(firmwareReturnCodeValue: Long) {
        fun initVGuardLifecycleHook() {
            BlackBoxLog.d { "VGuardLifecycleHook#Initialize" }
            try {
                hook = ActivityLifecycleHook(vGuard)
            } catch (e: Exception) {
                BlackBoxLog.d { "VGuardLifecycleHook#Failed" }
                throw IdentityProtectionInitializationException(FAILED_INITIALIZE_LIFECYCLE_HOOK)
            }
        }

        this.firmwareReturnCode = firmwareReturnCodeValue
        this.firmware = if (firmwareReturnCodeValue >= 0) {
            POSITIVE
        } else {
            NEGATIVE
        }

        if (firmware == POSITIVE && vGuard == null) {
            vGuard = VGuardFactory.getInstance()
            isSetupVGuardRunning.set(false)
            initVGuardLifecycleHook()
        }
    }

    /**
     * [setProfileLoaded] Called when [ACTION_PROFILE_LOADED] is triggered
     */
    internal fun setProfileLoaded(isSuccess: Boolean) {
        isProfileLoaded.set(isSuccess)
    }

    internal fun isVOSReadyToUse(): Boolean =
        firmware == POSITIVE && isProfileLoaded.get()

    private fun executeInitialization(
        message: String,
        context: Context,
        callback: OnRequestScanListener? = null
    ) {
        try {
            startInitialization(context)
        } catch (e: IdentityProtectionInitializationException) {
            BlackBoxLog.d(e) { "$message#Failed" }
            onError(e.message.orEmpty())
            callback?.onCompleted()
        }
    }

    private fun startInitialization(context: Context) {
        fun initVGuard() {
            BlackBoxLog.d { "VGuard#Initialize -- initVGuard" }
            startTime.set(System.currentTimeMillis())

            try {
                val vGuardBuilder = VGuardFactory.Builder()
                    .setDebugable(config.isDebuggable)
                    .setAllowsArbitraryNetworking(config.allowsArbitraryNetworking)
                    .setMemoryConfiguration(MemoryConfiguration.HIGH)
                    .setVGExceptionHandler {
                        BlackBoxLog.d(it) { "VGExceptionHandler" }
                        onError(it.message.orEmpty())
                    }

                VGuardFactory().getVGuard(context, vGuardBuilder)
            } catch (e: VGException) {
                BlackBoxLog.d(e) { "VGuard#Initialize -- VGException" }
                isSetupVGuardRunning.set(false)
                throw IdentityProtectionInitializationException(FAILED_INITIALIZE_VGUARD)
            } catch (e: Exception) {
                BlackBoxLog.d(e) { "VGuard#Initialize -- Exception" }
                isSetupVGuardRunning.set(false)
                throw IdentityProtectionInitializationException(FAILED_INITIALIZE)
            }
        }

        fun requestScan(vGuard: VGuard) {
            BlackBoxLog.d { "VGuard#Request Scan" }
            vGuard.requestScan()
        }

        vGuard?.let(::requestScan) ?: initVGuard()
    }

    private fun onError(code: String) {
        BlackBoxLog.d { "VGuard#Initialize#onError code: $code" }
        val message: String = IdentityProtectionResponse.getType(code) ?: code
        IdentityProtectionResponse.print(code)
        val timeTakenMillis = System.currentTimeMillis() - startTime.get()
        ScanAnalysis.Error(timeTakenMillis, message).let(::onScanAnalyzed)
    }

    /**
     * A private class to handle the scanning process when caller decided
     * to call [requestProfileChecks] explicitly
     *
     * A timer will automatically running for some period of time and
     * trigger the callback [OnRequestScanListener] when the time is exceeded or
     * scanning process is completed
     *
     * @param context [Context]
     * @param intentFilter [IntentFilter]
     * @param startTime [Long]
     *
     * @see [Config.timeOutInMillis]
     * @see [OnRequestScanListener]
     * @see [ACTION_SCAN_COMPLETE]
     * @see [ACTION_PROFILE_LOADED]
     */
    private class LocalBroadcastReceiver(
        private val context: Context,
        private val intentFilter: IntentFilter,
        private val startTime: Long
    ) {

        private var callback: OnRequestScanListener? = null
        private var executor: ExecutorService? = null

        private val receiver by lazy {
            object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    disposed(false, System.currentTimeMillis() - startTime, context)
                }
            }
        }

        private val main by lazy {
            object : Executor {

                private val mainThreadHandler = Handler(Looper.getMainLooper())

                override fun execute(command: Runnable) {
                    mainThreadHandler.post(command)
                }
            }
        }

        fun register(callback: OnRequestScanListener) {
            fun timeOutExceeded(startScan: Long): Pair<Boolean, Long> {
                val currentTime = System.currentTimeMillis()
                val timeTaken = currentTime - startScan
                return (timeTaken > config.timeOutInMillis) to timeTaken
            }

            this.callback = callback
            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter)

            executor = Executors.newCachedThreadPool()
            executor?.execute {
                var timeOut = timeOutExceeded(startTime)
                while (this.callback == null || timeOut.first.not()) {
                    timeOut = timeOutExceeded(startTime)
                }

                if (timeOut.first) {
                    main.execute { disposed(true, timeOut.second, context) }
                }
            }
        }

        /***
         * @param isTimeOut false means the invocation is from [BroadcastReceiver.onReceive]
         *        otherwise the invocation is from timeOut.
         * @param processTimeMillis
         * @param context
         */
        private fun disposed(
            isTimeOut: Boolean,
            processTimeMillis: Long,
            context: Context?
        ) {
            BlackBoxLog.d {
                "LocalBroadcastReceiver#disposed isTimeOut : $isTimeOut | " +
                        "processTimeMillis : $processTimeMillis | " +
                        "thresholdTimeMillis : ${config.timeOutInMillis}"
            }
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)

            if (isTimeOut.not()) {
                callback?.onCompleted()
            } else {
                callback?.onCompleted(
                    ScanAnalysis.ScanTimeOut(
                        processTimeMillis = processTimeMillis,
                        message = "TIMEOUT"
                    )
                )
            }

            callback = null
            executor?.shutdownNow()
            executor = null
        }
    }

    private fun logConfigChange(previousConfig: Config, newConfig: Config) {
        BlackBoxLog.d {
            val changedFields = mutableListOf<String>()
            Config::class.java.declaredFields.forEach { field ->
                field.isAccessible = true
                val previousValue = field[previousConfig]
                val newValue = field[newConfig]
                if (previousValue != newValue) {
                    changedFields += "${field.name}=$newValue"
                }
            }
            val changesInConfig = if (changedFields.isNotEmpty()) {
                changedFields.joinToString(", ")
            } else {
                "no changes"
            }

            "Updated config: Config($changesInConfig)"
        }
    }

    private fun runInIoThread(block: () -> Unit) {
        appExecutor.io.execute { block.invoke() }
    }

    private fun runInUiThread(block: () -> Unit) {
        appExecutor.ui.execute { block.invoke() }
    }
}
