package identity.protection

/**
 * An interface class for Identity Protection SDK
 * Every UI Controller which extend to [ProtectionIdentifier] will become an entry point which
 * allow the Identity Protection SDK to scan the threats
 */
interface ProtectionIdentifier
