package identity.protection.internal

import android.app.Application
import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import java.lang.ref.WeakReference

/**
 * Content providers are loaded before the application class is created.
 * [IdentityProtectionLifecycleWatcher] is used to create instance of [BlackBoxLifecycle]
 * and register the [Application.ActivityLifecycleCallbacks]
 */
internal sealed class IdentityProtectionLifecycleWatcher : ContentProvider() {
    /**
     * [MainProcess] automatically sets up the [BlackBoxLifecycle] code that runs in the main app process.
     */
    internal class MainProcess : IdentityProtectionLifecycleWatcher()

    override fun onCreate(): Boolean {
        val app = WeakReference(context!!.applicationContext as Application)
        // todo, check how long the initialized?
        BlackBoxLifecycle.init(app, BlackBoxLifecycleObserver())
        return true
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        throw AssertionError("Not yet implemented")
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        throw AssertionError("Not yet implemented")
    }

    override fun getType(uri: Uri): String? {
        throw AssertionError("Not yet implemented")
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        throw AssertionError("Not yet implemented")
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        throw AssertionError("Not yet implemented")
    }
}
