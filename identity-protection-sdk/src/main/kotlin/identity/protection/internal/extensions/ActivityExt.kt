package identity.protection.internal.extensions

import android.app.Activity

/**
 * An extension function to get the hash code value for the [Activity]
 * The general contract of `hashCode` is:
 *
 * Whenever it is invoked on the same object more than once,
 * the `hashCode` method must consistently return the same integer,
 * provided no information used in `equals` comparisons on the object is modified.
 *
 * If two objects are equal according to the `equals()` method,
 * then calling the `hashCode` method on each of the two objects
 * must produce the same integer result.
 *
 * @return [Int] hash code value for the [Activity]
 */
internal fun Activity.toHashCode(): Int = Integer.valueOf(this.hashCode())
