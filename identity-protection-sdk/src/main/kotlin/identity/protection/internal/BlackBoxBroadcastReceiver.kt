package identity.protection.internal

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.vkey.android.internal.vguard.engine.BasicThreatInfo
import com.vkey.android.vguard.VGuardBroadcastReceiver
import identity.protection.ScanAnalysis
import identity.protection.ScanAnalysis.ScanCompleted
import identity.protection.ScanAnalysis.VirtualSpaceDetected
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection
import identity.protection.responsecode.IdentityProtectionResponse
import java.util.concurrent.atomic.AtomicInteger

/**
 * A custom firmware return code that returning by [VGuardBroadcastReceiver]
 * return positive value when V-OS is started
 */
internal const val VOS_FIRMWARE_RETURN_CODE_KEY = "vkey.android.vguard.FIRMWARE_RETURN_CODE"

/**
 * A custom key to listen an event from [VGuardBroadcastReceiver]
 * It is used to determine whether VKey is safe to used
 */
internal const val ACTION_PROFILE_LOADED = "vkey.android.vguard.PROFILE_LOADED"

/**
 * An internal class that extend from [VGuardBroadcastReceiver] class
 * for receiving and handling broadcast intents sent by [Context.sendBroadcast]
 *
 * @param activity [Activity]
 */
internal class BlackBoxBroadcastReceiver(activity: Activity) : VGuardBroadcastReceiver(activity) {

    /**
     * Object to hold number of iteration when scanning process is completed
     */
    private var scanCompleteIteration: AtomicInteger = AtomicInteger(0)

    /**
     * Implement the actions to perform when a notification is received
     *
     * @param context [Context]
     * @param intent [Intent]
     */
    override fun onReceive(context: Context?, intent: Intent) {
        super.onReceive(context, intent)

        when (intent.action) {
            VOS_READY -> vosReady(intent)
            ACTION_SCAN_COMPLETE -> scanCompleted(intent)
            ACTION_PROFILE_LOADED -> profileLoaded(intent)
            VGUARD_VIRTUAL_SPACE_DETECTED -> virtualSpaceDetected(intent)
            VGUARD_STATUS -> printExtras("VGUARD_STATUS", intent)
            ACTION_FINISH -> printExtras("ACTION_FINISH", intent)
            VGUARD_MESSAGE -> printExtras("VGUARD_MESSAGE", intent)
        }
    }

    private fun vosReady(intent: Intent) {
        log(
            intent = intent,
            printBundle = { BlackBoxLog.d { "VOS_READY ===>> $it" } }
        )

        val firmwareReturnCodeValue = intent.getLongExtra(VOS_FIRMWARE_RETURN_CODE_KEY, -1)
        IdentityProtection.updateVosReady(firmwareReturnCodeValue)
    }

    private fun scanCompleted(intent: Intent) {
        /**
         * Check whether strict release mode is true and critical threats detected
         *
         * @param [hasCriticalThreats] list of [Boolean]
         *
         * @return true when strict release mode is true and critical threats detected, otherwise false
         */
        fun shouldLockVos(hasCriticalThreats: List<Boolean>): Boolean =
            IdentityProtection.config.strictMode && hasCriticalThreats.any { true }

        val iteration = scanCompleteIteration.incrementAndGet()
        val scanIteration = "ACTION_SCAN_COMPLETE_$iteration"
        printExtras(scanIteration, intent)

        val results: List<BasicThreatInfo> = intent.getParcelableArrayListExtra(SCAN_COMPLETE_RESULT) ?: emptyList()
        val criticalThreats: List<Int> = IdentityProtection.config.criticalThreats.orEmpty()
        val typeOfThreats = mutableListOf<String>()
        val hasCriticalThreats: List<Boolean> = results.map { threatInfo ->
            val threatClass: Int? = threatInfo.threatClass.toIntOrNull()
            IdentityProtectionResponse.print(threatClass)
            IdentityProtectionResponse.getType(threatClass.toString())?.let { type ->
                typeOfThreats.add(type)
            }
            threatClass in criticalThreats
        }

        BlackBoxLog.d { "${results.size} threat(s) detected" }
        val isVosLocked = shouldLockVos(hasCriticalThreats)
        if (isVosLocked) {
            BlackBoxLog.d { "Critical threat(s) detected" }
            lockVos()
        }

        val timeTakenMillis = System.currentTimeMillis() - IdentityProtection.startTime.get()
        ScanCompleted(
            processTimeMillis = timeTakenMillis,
            isSuccess = IdentityProtection.isVOSReadyToUse(),
            iteration = scanIteration,
            isVosLocked = isVosLocked,
            typeOfThreats = typeOfThreats
        ).let(IdentityProtection::onScanAnalyzed)
    }

    private fun profileLoaded(intent: Intent) {
        log(
            intent = intent,
            printBundle = { BlackBoxLog.d { "ACTION_PROFILE_LOADED ===>> $it" } }
        )

        /**
         * For better security, App should wait for profile loaded event to
         * initialize Signature Verification feature
         */
        IdentityProtection.initializeBlackBoxSignature()
        IdentityProtection.setProfileLoaded(true)

        val timeTakenMillis = System.currentTimeMillis() - IdentityProtection.startTime.get()
        ScanAnalysis.ProfileLoaded(
            processTimeMillis = timeTakenMillis,
            isSuccess = IdentityProtection.isVOSReadyToUse(),
        ).let(IdentityProtection::onScanAnalyzed)
    }

    private fun virtualSpaceDetected(intent: Intent) {
        printExtras("VGUARD_VIRTUAL_SPACE_DETECTED", intent)
        if (IdentityProtection.config.strictMode) {
            BlackBoxLog.d { "(StrictMode) Virtual Space Detected" }
            lockVos()

            val timeTakenMillis = System.currentTimeMillis() - IdentityProtection.startTime.get()
            VirtualSpaceDetected(
                processTimeMillis = timeTakenMillis,
                isSuccess = IdentityProtection.isVOSReadyToUse(),
                isVosLocked = true,
                message = "VGUARD_VIRTUAL_SPACE_DETECTED"
            ).let(IdentityProtection::onScanAnalyzed)
        }
    }

    private fun lockVos() {
        BlackBoxLog.d { "-> V-OS Locked <-" }
        IdentityProtection.vGuard?.lockVos()
    }

    private fun printExtras(info: String, intent: Intent) {
        log(
            intent = intent,
            printBundle = { BlackBoxLog.d { "$info ===>> $it" } },
            printThreat = { IdentityProtectionResponse.print(it) }
        )
    }

    private fun log(
        intent: Intent,
        printBundle: (StringBuilder) -> Unit,
        printThreat: ((Any?) -> Unit)? = null
    ) {
        val builder = StringBuilder()
        val bundle = intent.extras
        val types = mutableListOf<Any?>()
        bundle?.keySet()?.forEach { key ->
            bundle.get(key).let { value ->
                types.add(value)
                builder.append("$key = $value")
            }
        }

        printBundle.invoke(builder)
        types.forEach { printThreat?.invoke(it) }
    }
}
