package identity.protection.internal

import android.app.Activity
import android.app.Application
import android.os.Bundle
import identity.protection.model.IdentityProtection
import java.lang.ref.WeakReference

/**
 * An internal class for maintaining LifeCycle that will be use by internal.
 *
 * This call override few callbacks
 * [Application.ActivityLifecycleCallbacks.onActivityCreated]
 * [Application.ActivityLifecycleCallbacks.onActivityStarted]
 * [Application.ActivityLifecycleCallbacks.onActivityResumed]
 * [Application.ActivityLifecycleCallbacks.onActivityPaused]
 * [Application.ActivityLifecycleCallbacks.onActivityStopped]
 * [Application.ActivityLifecycleCallbacks.onActivityDestroyed]
 *
 * @param observer [LifeCycleObserver]
 *
 * @see [BlackBoxLifecycleObserver]
 */
internal class BlackBoxLifecycle private constructor(private val observer: LifeCycleObserver) {

    internal companion object {
        @Volatile
        var instance: BlackBoxLifecycle? = null
            private set

        /**
         * In order to avoid memory leak when we register the application, the usage
         * of WeakReference can be used here.
         *
         * @param app [WeakReference]
         * @param observer [BlackBoxLifecycleObserver]
         */
        @Synchronized
        fun init(app: WeakReference<Application>, observer: BlackBoxLifecycleObserver) {
            check(instance == null) { "BlackBoxLifecycle was already initialized" }
            instance = BlackBoxLifecycle(observer)
            app.get()?.registerActivityLifecycleCallbacks(object : EmptyLifeCycleCallback() {

                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                    val process = checkNotNull(instance)
                    process.onActivityCreated(activity)
                }

                override fun onActivityStarted(activity: Activity) {
                    /** NO-OP */
                }

                override fun onActivityResumed(activity: Activity) {
                    val process = checkNotNull(instance)
                    process.onActivityResumed(activity)
                }
                override fun onActivityPaused(activity: Activity) {
                    val process = checkNotNull(instance)
                    process.onActivityPaused(activity)
                }

                override fun onActivityStopped(activity: Activity) {
                    /** NO-OP */
                }

                override fun onActivityDestroyed(activity: Activity) {
                    val process = checkNotNull(instance)
                    process.onActivityDestroyed(activity)
                }
            })
        }
    }

    private fun onActivityCreated(activity: Activity) {
        isConfigEnabled { observer.onCreate(activity) }
    }

    private fun onActivityResumed(activity: Activity) {
        isConfigEnabled { observer.onResume(activity) }
    }

    private fun onActivityPaused(activity: Activity) {
        isConfigEnabled { observer.onPaused(activity) }
    }

    private fun onActivityDestroyed(activity: Activity) {
        isConfigEnabled { observer.onDestroy(activity) }
    }

    private fun isConfigEnabled(callBack: () -> Unit) {
        if (IdentityProtection.config.isEnabled)  {
            callBack.invoke()
        }
    }
}
