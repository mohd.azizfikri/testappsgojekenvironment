
package identity.protection.internal

import android.app.Activity
import android.app.Application
import android.os.Bundle

/**
 * A class for maintaining global application lifecycle.
 */
internal open class EmptyLifeCycleCallback : Application.ActivityLifecycleCallbacks {
    /**
     * Called when the Activity calls {@link Activity#onPause super.onPause()}.
     */
    override fun onActivityPaused(activity: Activity) { /*no-op*/
    }

    /**
     * Called when the Activity calls {@link Activity#onStart super.onStart()}.
     */
    override fun onActivityStarted(activity: Activity) { /*no-op*/
    }

    /**
     * Called when the Activity calls {@link Activity#onDestroy super.onDestroy()}.
     */
    override fun onActivityDestroyed(activity: Activity) { /*no-op*/
    }

    /**
     * Called when the Activity calls
     * {@link Activity#onSaveInstanceState super.onSaveInstanceState()}.
     */
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) { /*no-op*/
    }

    /**
     * Called when the Activity calls {@link Activity#onStop super.onStop()}.
     */
    override fun onActivityStopped(activity: Activity) { /*no-op*/
    }

    /**
     * Called when the Activity calls {@link Activity#onCreate super.onCreate()}.
     */
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) { /*no-op*/
    }

    /**
     * Called when the Activity calls {@link Activity#onResume super.onResume()}.
     */
    override fun onActivityResumed(activity: Activity) { /*no-op*/
    }
}
