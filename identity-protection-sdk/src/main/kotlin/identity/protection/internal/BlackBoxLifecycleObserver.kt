package identity.protection.internal

import android.app.Activity
import android.content.Context
import android.content.IntentFilter
import com.vkey.android.vguard.LocalBroadcastManager
import com.vkey.android.vguard.VGException
import com.vkey.android.vguard.VGuard
import com.vkey.android.vguard.VGuardBroadcastReceiver.ACTION_FINISH
import com.vkey.android.vguard.VGuardBroadcastReceiver.ACTION_SCAN_COMPLETE
import com.vkey.android.vguard.VGuardBroadcastReceiver.VGUARD_MESSAGE
import com.vkey.android.vguard.VGuardBroadcastReceiver.VGUARD_STATUS
import com.vkey.android.vguard.VGuardBroadcastReceiver.VGUARD_VIRTUAL_SPACE_DETECTED
import com.vkey.android.vguard.VGuardBroadcastReceiver.VOS_READY
import com.vkey.android.vguard.VGuardLifecycleHook
import identity.protection.internal.extensions.toHashCode
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection
import java.util.Hashtable
import java.util.concurrent.atomic.AtomicInteger

/**
 * An internal interface class to manage the internal lifecycle
 */
internal interface LifeCycleObserver {
    /**
     * [onCreate] Called when the Activity calls {@link Activity#onCreate super.onCreate()}.
     *
     * @param activity [Activity]
     */
    @Throws(VGException::class, Exception::class)
    fun onCreate(activity: Activity)

    /**
     * [onResume] Called when the Activity calls {@link Activity#onResume super.onResume()}.
     *
     * @param activity [Activity]
     */
    fun onResume(activity: Activity)

    /**
     * [onPaused] Called when the Activity calls {@link Activity#onPaused super.onPaused()}.
     *
     * @param activity [Activity]
     */
    fun onPaused(activity: Activity)

    /**
     * [onDestroy] Called when the Activity calls {@link Activity#onDestroy super.onDestroy()}.
     *
     * @param activity [Activity]
     */
    fun onDestroy(activity: Activity)
}

/**
 * An internal observer class to omit callbacks from the [LifeCycleObserver]
 */
internal class BlackBoxLifecycleObserver : LifeCycleObserver {

    /**
     * The number of activity who currently active. Perform final cleanup when its zero
     */
    private val activityCount: AtomicInteger = AtomicInteger(0)

    /**
     * Receive and handling broadcast intents sent by [Context.sendBroadcast]
     */
    private var blackBoxReceiver: BlackBoxBroadcastReceiver? = null

    /**
     * The collection of activity that has a responsibility to initialize [VGuard]
     * when the [VGuard] is not initialize yet
     */
    private val activityProtections = Hashtable<Int, Activity>()

    @Throws(VGException::class, Exception::class)
    override fun onCreate(activity: Activity) {
        log(activity, "onCreate")
        activityProtections[activity.toHashCode()] = activity
        activityCount.incrementAndGet()

        if (vGuard == null && IdentityProtection.isSetupVGuardRunning.get().not()) {
            IdentityProtection.isSetupVGuardRunning.set(true)
            setupVGuard(activity)
        }
    }

    override fun onResume(activity: Activity) {
        vGuard?.onResume(hook, activity)
    }

    override fun onPaused(activity: Activity) {
        vGuard?.onPause(hook)
    }

    /**
     * In android api >= 26 onDestroy would not guarantee gets called.
     *
     * but since this is a clean up resource, it should be fine to dispose reference here
     * since crashing also would dispose resource anyway, unless it is saved on bundle.
     */
    override fun onDestroy(activity: Activity) {
        val activityHashCode = activity.toHashCode()
        activityProtections[activityHashCode]?.let {
            activityProtections.remove(activityHashCode)
            activityCount.decrementAndGet()

            if (activityCount.get() == 0) {
                log(activity, "onDestroy")
                IdentityProtection.destroy()
                blackBoxReceiver?.let { getLocalBroadCastManager(activity).unregisterReceiver(it) }
            }
        }
    }

    private fun setupVGuard(activity: Activity) {
        val config: IdentityProtection.Config = IdentityProtection.config

        fun initBroadCastReceiver(activity: Activity) {
            val broadcastManager = getLocalBroadCastManager(activity)
            blackBoxReceiver = BlackBoxBroadcastReceiver(activity)

            // necessary for V-OS App Protection to finish activity safely
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(ACTION_FINISH))
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(ACTION_SCAN_COMPLETE))
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(ACTION_PROFILE_LOADED))
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(VGUARD_STATUS))
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(VGUARD_MESSAGE))
            broadcastManager.registerReceiver(blackBoxReceiver, IntentFilter(VOS_READY))

            if (config.strictMode && config.virtualSpaceDetection) {
                broadcastManager.registerReceiver(
                    blackBoxReceiver,
                    IntentFilter(VGUARD_VIRTUAL_SPACE_DETECTED)
                )
            }
        }

        log(activity, "setupVGuard")
        initBroadCastReceiver(activity)
        IdentityProtection.initializeVGuard(activity)
    }

    private val vGuard: VGuard?
        get() = IdentityProtection.vGuard

    private val hook: VGuardLifecycleHook?
        get() = IdentityProtection.hook

    private fun getLocalBroadCastManager(
        activity: Activity
    ) = LocalBroadcastManager.getInstance(activity)

    private fun log(activity: Activity, message: String) {
        BlackBoxLog.d { "BlackBoxLifecycleObserver#$message ${activity::class.java.simpleName} "}
    }
}
