package identity.protection.internal

import com.vkey.vos.signer.taInterface
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection.CryptoSignature
import vkey.android.vos.VosWrapper

/**
 * An interface class for signing mechanism that using CryptoTA internally
 */
internal interface BlackBoxSignature {
    /**
     * Signing given a plain-text that using CryptoTA internally and return [ByteArray]
     *
     * @param message [String] a message that need to be signed
     *
     * @return NULL If the given message is failed to signed otherwise [ByteArray]
     */
    fun sign(message: String): ByteArray?

    /**
     * Perform any final cleanup. This can happen either because the activity is finishing
     * or because the system is temporarily destroying this instance of the activity to save space.
     */
    fun close()
}

/**
 * An internal class that extend from [BlackBoxSignature] class
 * for handling the signing mechanism that using CryptoTA internally
 *
 * @param cryptoSignature [CryptoSignature]
 */
internal class BlackBoxSignatureImpl(
    private val cryptoSignature: CryptoSignature
) : BlackBoxSignature {

    private var vosWrapper: VosWrapper? = null
    private var cryptoTA: taInterface? = null

    init {
        vosWrapper = VosWrapper.getInstance(cryptoSignature.context)
        cryptoSignature.trustedTimeServerUrl?.let {
            vosWrapper?.setTrustedTimeServerUrl(it)
        }

        cryptoTA = taInterface.getInstance()
        open()
    }

    /**
     * Signing given a plain-text that using CryptoTA internally and return [ByteArray]
     *
     * @param message [String] a message that need to be signed
     *
     * @return NULL If the given message is failed to signed otherwise [ByteArray]
     */
    override fun sign(message: String): ByteArray? {
        val error: IntArray = intArrayOf(1)

        val result: ByteArray? = cryptoTA?.signMsg(
            message.toByteArray(),
            cryptoSignature.storeKey,
            cryptoSignature.hashType.rawValue,
            error
        )

        return when {
            error[0] < 0 -> {
                BlackBoxLog.d { "BlackBoxSignature#Failed to sign the message -> ${error[0]}" }
                null
            }
            result == null -> {
                BlackBoxLog.d { "BlackBoxSignature#Failed to sign the message -> result is null" }
                null
            }
            result.isEmpty() -> {
                BlackBoxLog.d { "BlackBoxSignature#Failed to sign the message -> result is empty" }
                null
            }
            else -> {
                result
            }
        }
    }

    override fun close() {
        cryptoTA?.unloadTA()
    }

    private fun open() {
        cryptoTA?.loadTA()
        cryptoTA?.initialize()
        cryptoTA?.processManifest(cryptoSignature.context)
    }
}
