package identity.protection

import identity.protection.SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataFailed
import identity.protection.SFIODecrypt.DecryptBlockOfData.DecryptBlockOfDataSuccess
import identity.protection.SFIODecrypt.DecryptFile.DecryptFileFailed
import identity.protection.SFIODecrypt.DecryptFile.DecryptFileSuccess
import identity.protection.SFIODecrypt.DecryptString.DecryptStringFailed
import identity.protection.SFIODecrypt.DecryptString.DecryptStringSuccess
import identity.protection.exception.IdentityProtectionSFIOException

/**
 * [SFIODecrypt] is contain an information for Identity Protection SDK when the decryption process
 * using V-Key Secure File IO Feature is success or not
 *
 * There will be multiple type of decryption
 *
 * @see [DecryptBlockOfData]
 * @see [DecryptString]
 * @see [DecryptFile]
 * @see [ReadFromDecryptedFile]
 */
sealed class SFIODecrypt {

    /**
     * [DecryptBlockOfData] is the output of the decryption process whether it is success or not
     *
     * @see [DecryptBlockOfDataSuccess]
     * @see [DecryptBlockOfDataFailed]
     */
    sealed class DecryptBlockOfData : SFIODecrypt() {

        /**
         * [DecryptBlockOfDataSuccess] triggered when decryption process is success
         *
         * @param result [String] is decrypted value
         */
        data class DecryptBlockOfDataSuccess(val result: String) : DecryptBlockOfData()

        /**
         * [DecryptBlockOfDataFailed] triggered when decryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class DecryptBlockOfDataFailed(
            val exception: IdentityProtectionSFIOException
        ) : DecryptBlockOfData()
    }

    /**
     * [DecryptString] is the output of the decryption process whether it is success or not
     *
     * @see [DecryptStringSuccess]
     * @see [DecryptStringFailed]
     */
    sealed class DecryptString : SFIODecrypt() {

        /**
         * [DecryptStringSuccess] triggered when decryption process is success
         *
         * @param result [String] is decrypted value
         */
        data class DecryptStringSuccess(val result: String) : DecryptString()

        /**
         * [DecryptStringFailed] triggered when decryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class DecryptStringFailed(
            val exception: IdentityProtectionSFIOException
        ) : DecryptString()
    }

    /**
     * [DecryptFile] is the output of the decryption process whether it is success or not
     *
     * @see [DecryptFileSuccess]
     * @see [DecryptFileFailed]
     */
    sealed class DecryptFile : SFIODecrypt() {

        /**
         * [DecryptFileSuccess] triggered when decryption process is success
         *
         * @param result [String] is decrypted value
         */
        data class DecryptFileSuccess(val result: String) : DecryptFile()

        /**
         * [DecryptFileFailed] triggered when decryption process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class DecryptFileFailed(
            val exception: IdentityProtectionSFIOException
        ) : DecryptFile()
    }

    /**
     * [ReadFromDecryptedFile] is the output of the reading a content from encrypted file
     * whether it is success or not
     *
     * @see [ReadFromDecryptedFileSuccess]
     * @see [ReadFromDecryptedFileFailed]
     */
    sealed class ReadFromDecryptedFile : SFIODecrypt() {

        /**
         * [ReadFromDecryptedFileSuccess] triggered when reading process is success
         *
         * @param result [String] is decrypted value
         */
        data class ReadFromDecryptedFileSuccess(val result: String) : ReadFromDecryptedFile()

        /**
         * [ReadFromDecryptedFileFailed] triggered when reading process is failed
         *
         * @param exception [IdentityProtectionSFIOException]
         */
        data class ReadFromDecryptedFileFailed(
            val exception: IdentityProtectionSFIOException
        ) : ReadFromDecryptedFile()
    }
}
