package identity.protection

import identity.protection.exception.IdentityProtectionSFIOException

/**
 * An interface class for Secure File IO deletion file APIs
 */
interface OnSFIODeleteFileListener {

    /**
     * [onSuccess] Called when the file or directory is successfully deleted
     */
    fun onSuccess()
    
    /**
     * [onError]  Called when the file or directory is failed to deleted due to exceptions
     * This is useful for error reporting and to diagnose why a file cannot be deleted
     *
     * @param e [IdentityProtectionSFIOException]
     */
    fun onError(e: IdentityProtectionSFIOException? = null)
}
