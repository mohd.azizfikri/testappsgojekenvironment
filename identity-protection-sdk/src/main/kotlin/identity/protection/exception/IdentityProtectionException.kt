package identity.protection.exception

import com.vkey.android.vguard.VGException
import identity.protection.internal.BlackBoxSignature
import identity.protection.internal.ACTION_PROFILE_LOADED
import identity.protection.internal.VOS_FIRMWARE_RETURN_CODE_KEY
import identity.protection.model.IdentityProtection
import identity.protection.model.IdentityProtection.Config
import java.io.IOException
import java.lang.RuntimeException

/**
 * A collection of Identity Protection SDK exceptions message
 *
 * @param message [String] a message for "why the exception arises"
 *
 * @see DISABLE
 * @see NOT_INITIALIZE
 * @see FAILED_INITIALIZE
 * @see FAILED_INITIALIZE_VGUARD
 * @see FAILED_INITIALIZE_LIFECYCLE_HOOK
 * @see SIGNATURE_NOT_INITIALIZE
 * @see VOS_NOT_READY
 * @see ACTION_PROFILE_NOT_LOADED
 */
enum class IdentityProtectionExceptionMessage(val message: String) {
    /**
     * Identity Protection SDK is not enabled.
     *
     * The exception arises due to the flag to enable Identity Protection SDK is set to false
     *
     * @see [Config]
     */
    DISABLE("IdentityProtectionSDK is not enabled"),

    /**
     * Identity Protection SDK is not initialized yet
     *
     * The exception arises due the instance of VGuard Manager is not initialized yet
     *
     * @see [IdentityProtection.vGuard]
     */
    NOT_INITIALIZE("IdentityProtectionSDK is not initialized yet"),

    /**
     * Identity Protection SDK is failed to initialized VGuard Manager
     *
     * The exception arises due the instance of VGuard Manager is failed to initialized
     *
     * @see [IdentityProtection.vGuard]
     */
    FAILED_INITIALIZE("IdentityProtectionSDK is failed to initialized"),

    /**
     * Identity Protection SDK is failed to initialized VGuard Manager triggered by [VGException]
     *
     * The exception arises due the instance of VGuard Manager is failed to initialized
     *
     * @see [IdentityProtection.vGuard]
     */
    FAILED_INITIALIZE_VGUARD("IdentityProtectionSDK is failed to initialized VGuard Manager"),

    /**
     * Identity Protection SDK is failed to initialized lifecycle hook
     *
     * The exception arises due the instance of Activity Lifecycle Hook is failed to initialized
     *
     * @see [IdentityProtection.hook]
     */
    FAILED_INITIALIZE_LIFECYCLE_HOOK("IdentityProtectionSDK is failed to initialized lifecycle hook"),

    /**
     * Signature is not initialized yet
     *
     * The exception arises due the instance of [BlackBoxSignature] is not initialized yet
     *
     * @see [IdentityProtection.signature]
     */
    SIGNATURE_NOT_INITIALIZE("Signature is not initialized yet"),

    /**
     * V-OS not Ready
     *
     * The exception arises due the [VOS_FIRMWARE_RETURN_CODE_KEY] value is < 0
     *
     * @see [IdentityProtection.vGuard]
     */
    VOS_NOT_READY("V-OS is not ready yet"),

    /**
     * Action Profile Not Loaded
     *
     * The exception arises due the [ACTION_PROFILE_LOADED] event is not coming yet
     *
     * @see [IdentityProtection.vGuard]
     */
    ACTION_PROFILE_NOT_LOADED("Action Profile Not Loaded"),

    /**
     * Method can't be call in main thread
     *
     * The exception arises when the host app calls the API in main thread
     */
    METHOD_THREAD("Method can't be call in main thread")
}

/**
 * An IdentityProtectionIllegalAccessException is thrown when an application tries
 * to reflectively create an instance (other than an array),
 * set or get a field, or invoke a method, but the currently
 * executing method does not have access to the definition of
 * the specified class, field, method or constructor.
 *
 * @param reason [IdentityProtectionExceptionMessage]
 */
class IdentityProtectionIllegalAccessException(
    reason: IdentityProtectionExceptionMessage
) : IllegalAccessException(reason.message)

/**
 * An IdentityProtectionNullPointerException is thrown when an application attempts
 * to use {@code null} in a case where an object is required.
 *
 * @param reason [IdentityProtectionExceptionMessage]
 */
class IdentityProtectionNullPointerException(
    reason: IdentityProtectionExceptionMessage
) : KotlinNullPointerException(reason.message)

/**
 * An IdentityProtectionInitializationException is thrown when an application failed
 * to initialized the SDK
 *
 * @param reason [IdentityProtectionExceptionMessage]
 */
class IdentityProtectionInitializationException(
    reason: IdentityProtectionExceptionMessage
) : RuntimeException(reason.message)

/**
 * An IdentityProtectionSFIOException is thrown when encryption decryption, updating password and
 * deleting encrypted file is failed
 */
class IdentityProtectionSFIOException(reason: String) : IOException(reason)
