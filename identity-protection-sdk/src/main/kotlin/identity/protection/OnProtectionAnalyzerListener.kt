package identity.protection

import com.vkey.android.vguard.VGuardBroadcastReceiver.ACTION_SCAN_COMPLETE
import com.vkey.android.vguard.VGuardBroadcastReceiver.VGUARD_VIRTUAL_SPACE_DETECTED
import com.vkey.android.vguard.VGuardBroadcastReceiver.VOS_READY


/**
 * An interface class for analyzer
 */
interface OnProtectionAnalyzerListener {

    /**
     * [onScanAnalyzed] Called when Identity Protection SDK found an error during
     * the initialization triggered by [VOS_READY]
     * or when the scanning process is completed triggered by [ACTION_SCAN_COMPLETE]
     * or when virtual space detected triggered by [VGUARD_VIRTUAL_SPACE_DETECTED]
     *
     * There will be multiple [onScanAnalyzed] coming due to the continuous scanning process
     *
     * @param scanAnalysis [ScanAnalysis]
     */
    fun onScanAnalyzed(scanAnalysis: ScanAnalysis)
}
