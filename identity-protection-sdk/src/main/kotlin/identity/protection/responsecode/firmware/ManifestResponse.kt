package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Manifest error code.
 */
enum class ManifestResponse(val code: Long) : Response {
    /**
     * Code: -1401
     *
     * Type: MANIFEST_INVALID_SIGN
     *
     * Cause(s):
     * - The manifest file contains an invalid signature.
     *
     * Recommendation(s):
     * - Make sure that the manifest file is not tampered with.
     */
    INVALID_SIGN(-1401) {
        override fun getResponseModel() = ResponseModel(
            code = INVALID_SIGN.code,
            type = "MANIFEST_INVALID_SIGN",
            causes = "The manifest file contains an invalid signature.",
            recommendations = "Make sure that the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1402
     *
     * Type: MANIFEST_FILE_NOT_FOUND
     *
     * Cause(s):
     * - The manifest file is not found.
     *
     * Recommendation(s):
     * - Make sure that the manifest file is included in the package.
     */
    FILE_NOT_FOUND(-1402) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_NOT_FOUND.code,
            type = "MANIFEST_FILE_NOT_FOUND",
            causes = "The manifest file is not found.",
            recommendations = "Make sure that the manifest file is included in the package."
        )
    },

    /**
     * Code: -1403
     *
     * Type: MANIFEST_INVALID_FILE
     *
     * Cause(s):
     * - The manifest file is invalid or corrupted.
     *
     * Recommendation(s):
     * - Obtain a new package with valid manifest file.
     */
    INVALID_FILE(-1403) {
        override fun getResponseModel() = ResponseModel(
            code = INVALID_FILE.code,
            type = "MANIFEST_INVALID_FILE",
            causes = "The manifest file is invalid or corrupted.",
            recommendations = "Obtain a new package with valid manifest file."
        )
    },

    /**
     * Code: -1404
     *
     * Type: MANIFEST_MEMORY_ERROR
     *
     * Cause(s):
     * - The VM failed to allocate memory.
     *
     * Recommendation(s):
     * - NONE.
     */
    MEMORY_ERROR(-1404) {
        override fun getResponseModel() = ResponseModel(
            code = MEMORY_ERROR.code,
            type = "MANIFEST_MEMORY_ERROR",
            causes = "The VM failed to allocate memory.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1405
     *
     * Type: MANIFEST_INVALID_JSON
     *
     * Cause(s):
     * - The manifest file has invalid JSON construct.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    INVALID_JSON(-1405) {
        override fun getResponseModel() = ResponseModel(
            code = INVALID_JSON.code,
            type = "MANIFEST_INVALID_JSON",
            causes = "The manifest file has invalid JSON construct.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1406
     *
     * Type: MANIFEST_INVALID_TIMESTAMP
     *
     * Cause(s):
     * - The manifest file has an invalid timestamp.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    INVALID_TIMESTAMP(-1406) {
        override fun getResponseModel() = ResponseModel(
            code = INVALID_TIMESTAMP.code,
            type = "MANIFEST_INVALID_TIMESTAMP",
            causes = "The manifest file has an invalid timestamp.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1407
     *
     * Type:
     *
     * Cause(s):
     * - The manifest file has an invalid submodule objserial field.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    INVALID_OBJSERIAL(-1407) {
        override fun getResponseModel() = ResponseModel(
            code = INVALID_OBJSERIAL.code,
            type = "MANIFEST_INVALID_OBJSERIAL",
            causes = "The manifest file has an invalid submodule objserial field.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1408
     *
     * Type: MANIFEST_BUFFER_SIZE_ERROR
     *
     * Cause(s):
     * - The manifest file has fields that require more buffer size than needed.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    BUFFER_SIZE_ERROR(-1408) {
        override fun getResponseModel() = ResponseModel(
            code = BUFFER_SIZE_ERROR.code,
            type = "MANIFEST_BUFFER_SIZE_ERROR",
            causes = "The manifest file has fields that require more buffer size than needed.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1409
     *
     * Type: MANIFEST_BAD_ARGUMENTS
     *
     * Cause(s):
     * - The function argument provided is invalid.
     *
     * Recommendation(s):
     * - Make sure that the function arguments provided are valid.
     */
    BAD_ARGUMENTS(-1409) {
        override fun getResponseModel() = ResponseModel(
            code = BAD_ARGUMENTS.code,
            type = "MANIFEST_BAD_ARGUMENTS",
            causes = "The function argument provided is invalid.",
            recommendations = "Make sure that the function arguments provided are valid."
        )
    },

    /**
     * Code: -1410
     *
     * Type:
     *
     * Cause(s):
     * - The manifest file overall signature is incorrect. This is a fatal error.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    MOD_SIGN_VERIFY_FAIL(-1410) {
        override fun getResponseModel() = ResponseModel(
            code = MOD_SIGN_VERIFY_FAIL.code,
            type = "MANIFEST_MOD_SIGN_VERIFY_FAIL",
            causes = "The manifest file overall signature is incorrect. This is a fatal error.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1411
     *
     * Type: MANIFEST_MOD_TIMESTAMP_FAIL
     *
     * Cause(s):
     * - The manifest file has the correct signature, but incorrect timestamp.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    MOD_TIMESTAMP_FAIL(-1411) {
        override fun getResponseModel() = ResponseModel(
            code = MOD_TIMESTAMP_FAIL.code,
            type = "MANIFEST_MOD_TIMESTAMP_FAIL",
            causes = "The manifest file has the correct signature, but incorrect timestamp.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1412
     *
     * Type: MANIFEST_SUBMOD_SIGN_VERIFY_FAIL
     *
     * Cause(s):
     * - The manifest file has one or more invalid submodule signature.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    SUBMOD_SIGN_VERIFY_FAIL(-1412) {
        override fun getResponseModel() = ResponseModel(
            code = SUBMOD_SIGN_VERIFY_FAIL.code,
            type = "MANIFEST_SUBMOD_SIGN_VERIFY_FAIL",
            causes = "The manifest file has one or more invalid submodule signature.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    },

    /**
     * Code: -1413
     *
     * Type: MANIFEST_SUBMOD_LOGIC_ERROR
     *
     * Cause(s):
     * - The manifest file has one or more incorrect submodule logic.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    SUBMOD_LOGIC_ERROR(-1413) {
        override fun getResponseModel() = ResponseModel(
            code = SUBMOD_LOGIC_ERROR.code,
            type = "MANIFEST_SUBMOD_LOGIC_ERROR",
            causes = "The manifest file has one or more incorrect submodule logic.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1414
     *
     * Type: MANIFEST_SUBMOD_SIGN_VERIFY_LOGIC_ERROR
     *
     * Cause(s):
     * - The manifest file has more than one submodules with either
     * invalid submodule signature or incorrect submodule logic.
     *
     * Recommendation(s):
     * - Make sure the manifest file is not tampered with.
     */
    SUBMOD_SIGN_VERIFY_LOGIC_ERROR(-1414) {
        override fun getResponseModel() = ResponseModel(
            code = SUBMOD_SIGN_VERIFY_LOGIC_ERROR.code,
            type = "MANIFEST_SUBMOD_SIGN_VERIFY_LOGIC_ERROR",
            causes = "The manifest file has more than one submodules with either " +
                    "invalid submodule signature or incorrect submodule logic.",
            recommendations = "Make sure the manifest file is not tampered with."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            INVALID_SIGN.code to INVALID_SIGN.getResponseModel(),
            FILE_NOT_FOUND.code to FILE_NOT_FOUND.getResponseModel(),
            INVALID_FILE.code to INVALID_FILE.getResponseModel(),
            MEMORY_ERROR.code to MEMORY_ERROR.getResponseModel(),
            INVALID_JSON.code to INVALID_JSON.getResponseModel(),
            INVALID_TIMESTAMP.code to INVALID_TIMESTAMP.getResponseModel(),
            INVALID_OBJSERIAL.code to INVALID_OBJSERIAL.getResponseModel(),
            BUFFER_SIZE_ERROR.code to BUFFER_SIZE_ERROR.getResponseModel(),
            BAD_ARGUMENTS.code to BAD_ARGUMENTS.getResponseModel(),
            MOD_SIGN_VERIFY_FAIL.code to MOD_SIGN_VERIFY_FAIL.getResponseModel(),
            MOD_TIMESTAMP_FAIL.code to MOD_TIMESTAMP_FAIL.getResponseModel(),
            SUBMOD_SIGN_VERIFY_FAIL.code to SUBMOD_SIGN_VERIFY_FAIL.getResponseModel(),
            SUBMOD_LOGIC_ERROR.code to SUBMOD_LOGIC_ERROR.getResponseModel(),
            SUBMOD_SIGN_VERIFY_LOGIC_ERROR.code to SUBMOD_SIGN_VERIFY_LOGIC_ERROR.getResponseModel(),
        )
    }
}
