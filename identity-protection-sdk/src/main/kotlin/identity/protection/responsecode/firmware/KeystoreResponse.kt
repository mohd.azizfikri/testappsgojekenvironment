package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Keystore response code.
 */
enum class KeystoreResponse(val code: Long) : Response {
    /**
     * Code: -301
     *
     * Type: MANIFEST_ALIAS_NOT_FOUND
     *
     * Cause(s):
     * - The given key alias is not available in the keystore.
     *
     * Recommendation(s):
     * - The given key alias is not available in the keystore.
     */
    ALIAS_NOT_FOUND(-301) {
        override fun getResponseModel() = ResponseModel(
            code = ALIAS_NOT_FOUND.code,
            type = "MANIFEST_ALIAS_NOT_FOUND",
            causes = "The given key alias is not available in the keystore.",
            recommendations = "The given key alias is not available in the keystore."
        )
    },

    /**
     * Code: -302
     *
     * Type: MANIFEST_ALIAS_ALREADY_EXISTS
     *
     * Cause(s):
     * - The given key alias already existed in the keystore.
     *
     * Recommendation(s):
     * - The given key alias already existed in the keystore.
     */
    ALIAS_ALREADY_EXISTS(-302) {
        override fun getResponseModel() = ResponseModel(
            code = ALIAS_ALREADY_EXISTS.code,
            type = "MANIFEST_ALIAS_ALREADY_EXISTS",
            causes = "The given key alias already existed in the keystore.",
            recommendations = "The given key alias already existed in the keystore."
        )
    },

    /**
     * Code: -303
     *
     * Type: MANIFEST_MAX_SIZE_REACHED
     *
     * Cause(s):
     * - The maximum size of the keystore has been reached.
     *
     * Recommendation(s):
     * - Keep the number of keystores within the limit.
     */
    MAX_SIZE_REACHED(-303) {
        override fun getResponseModel() = ResponseModel(
            code = MAX_SIZE_REACHED.code,
            type = "MANIFEST_MAX_SIZE_REACHED",
            causes = "The maximum size of the keystore has been reached.",
            recommendations = "Keep the number of keystores within the limit."
        )
    },

    /**
     * Code: -304
     *
     * Type: MANIFEST_LOAD
     *
     * Cause(s):
     * - Error loading the keystore.
     *
     * Recommendation(s):
     * - NONE.
     */
    LOAD(-304) {
        override fun getResponseModel() = ResponseModel(
            code = LOAD.code,
            type = "MANIFEST_LOAD",
            causes = "Error loading the keystore.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -305
     *
     * Type: MANIFEST_SAVE
     *
     * Cause(s):
     * - Error saving the keystore.
     *
     * Recommendation(s):
     * - NONE.
     */
    SAVE(-305) {
        override fun getResponseModel() = ResponseModel(
            code = SAVE.code,
            type = "MANIFEST_SAVE",
            causes = "Error saving the keystore.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -306
     *
     * Type: MANIFEST_BEFORE_ACTIVATION
     *
     * Cause(s):
     * - The key being used has not been activated.
     *
     * Recommendation(s):
     * - Make sure that the key being used has been activated before use.
     */
    BEFORE_ACTIVATION(-306) {
        override fun getResponseModel() = ResponseModel(
            code = BEFORE_ACTIVATION.code,
            type = "MANIFEST_BEFORE_ACTIVATION",
            causes = "The key being used has not been activated.",
            recommendations = "Make sure that the key being used has been activated before use."
        )
    },

    /**
     * Code: -307
     *
     * Type: MANIFEST_EXPIRED
     *
     * Cause(s):
     * - The key being used has expired.
     *
     * Recommendation(s):
     * - Make sure that the key being used is not expired.
     */
    EXPIRED(-307) {
        override fun getResponseModel() = ResponseModel(
            code = EXPIRED.code,
            type = "MANIFEST_EXPIRED",
            causes = "The key being used has expired.",
            recommendations = "Make sure that the key being used is not expired."
        )
    },

    /**
     * Code: -308
     *
     * Type: MANIFEST_COMPROMISED
     *
     * Cause(s):
     * - The key being used has been compromised.
     *
     * Recommendation(s):
     * - Make sure that the key being used is not compromised.
     */
    COMPROMISED(-308) {
        override fun getResponseModel() = ResponseModel(
            code = COMPROMISED.code,
            type = "MANIFEST_COMPROMISED",
            causes = "The key being used has been compromised.",
            recommendations = "Make sure that the key being used is not compromised."
        )
    },

    /**
     * Code: -309
     *
     * Type: MANIFEST_DESTROYED
     *
     * Cause(s):
     * - The key being used has been destroyed.
     *
     * Recommendation(s):
     * - Make sure that the key being used is not destroyed.
     */
    DESTROYED(-309) {
        override fun getResponseModel() = ResponseModel(
            code = DESTROYED.code,
            type = "MANIFEST_DESTROYED",
            causes = "The key being used has been destroyed.",
            recommendations = "Make sure that the key being used is not destroyed."
        )
    },

    /**
     * Code: -310
     *
     * Type: MANIFEST_DESTROYED_COMPROMISED
     *
     * Cause(s):
     * - The key being used has been destroyed or compromised.
     *
     * Recommendation(s):
     * - Make sure that the key being used is not destroyed or compromised.
     */
    DESTROYED_COMPROMISED(-310) {
        override fun getResponseModel() = ResponseModel(
            code = DESTROYED_COMPROMISED.code,
            type = "MANIFEST_DESTROYED_COMPROMISED",
            causes = "The key being used has been destroyed or compromised.",
            recommendations = "Make sure that the key being used is not destroyed or compromised."
        )
    },

    /**
     * Code: -311
     *
     * Type: MANIFEST_LOAD_INVALID_DATA
     *
     * Cause(s):
     * - Failed to load keystore with invalid buffer (having no V–OS header).
     * Invalid data is being imported to the keystore.
     *
     * Recommendation(s):
     * - Make sure that all data imported to the keystore is valid.
     */
    LOAD_INVALID_DATA(-311) {
        override fun getResponseModel() = ResponseModel(
            code = LOAD_INVALID_DATA.code,
            type = "MANIFEST_LOAD_INVALID_DATA",
            causes = "Failed to load keystore with invalid buffer (having no V–OS header). " +
                    "Invalid data is being imported to the keystore.",
            recommendations = "Make sure that all data imported to the keystore is valid."
        )
    },

    /**
     * Code: -312
     *
     * Type: MANIFEST_LOAD_INVALID_DFP
     *
     * Cause(s):
     * - Failed to load keystore.
     * The keystore was exported using an invalid DFP (or the DFP has changed).
     *
     * Recommendation(s):
     * - Make sure that the keystore is exported with an valid DFP.
     * - Make sure that the DFP is not change.
     */
    LOAD_INVALID_DFP(-312) {
        override fun getResponseModel() = ResponseModel(
            code = LOAD_INVALID_DFP.code,
            type = "MANIFEST_LOAD_INVALID_DFP",
            causes = "Failed to load keystore. " +
                    "The keystore was exported using an invalid DFP (or the DFP has changed).",
            recommendations = "Make sure that the keystore is exported with an valid DFP.\n" +
                    "- Make sure that the DFP is not change."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            ALIAS_NOT_FOUND.code to ALIAS_NOT_FOUND.getResponseModel(),
            ALIAS_ALREADY_EXISTS.code to ALIAS_ALREADY_EXISTS.getResponseModel(),
            MAX_SIZE_REACHED.code to MAX_SIZE_REACHED.getResponseModel(),
            LOAD.code to LOAD.getResponseModel(),
            SAVE.code to SAVE.getResponseModel(),
            BEFORE_ACTIVATION.code to BEFORE_ACTIVATION.getResponseModel(),
            EXPIRED.code to EXPIRED.getResponseModel(),
            COMPROMISED.code to COMPROMISED.getResponseModel(),
            DESTROYED.code to DESTROYED.getResponseModel(),
            DESTROYED_COMPROMISED.code to DESTROYED_COMPROMISED.getResponseModel(),
            LOAD_INVALID_DATA.code to LOAD_INVALID_DATA.getResponseModel(),
            LOAD_INVALID_DFP.code to LOAD_INVALID_DFP.getResponseModel(),
        )
    }
}
