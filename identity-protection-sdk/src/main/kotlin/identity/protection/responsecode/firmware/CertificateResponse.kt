package identity.protection.responsecode.firmware

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of Certificate response code.
 */
enum class CertificateResponse(val code: Long) : Response {
    /**
     * Code: -331
     *
     * Type: CERTSTORE_ALIAS_NOT_FOUND
     *
     * Cause(s):
     * - The given certificate alias is not found in the certificate store.
     *
     * Recommendation(s):
     * - Make sure that the given certificate alias is not present in the certificate store.
     */
    ALIAS_NOT_FOUND(-331) {
        override fun getResponseModel() = ResponseModel(
            code = ALIAS_NOT_FOUND.code,
            type = "CERTSTORE_ALIAS_NOT_FOUND",
            causes = "The given certificate alias is not found in the certificate store.",
            recommendations = "Make sure that the given certificate alias is present " +
                    "in the certificate store."
        )
    },

    /**
     * Code: -332
     *
     * Type: CERTSTORE_ALIAS_ALREADY_EXISTS
     *
     * Cause(s):
     * - The given certificate alias already exists in the certificate store.
     *
     * Recommendation(s):
     * - NONE.
     */
    ALIAS_ALREADY_EXISTS(-332) {
        override fun getResponseModel() = ResponseModel(
            code = ALIAS_ALREADY_EXISTS.code,
            type = "CERTSTORE_ALIAS_ALREADY_EXISTS",
            causes = "The given certificate alias already exists in the certificate store.",
            recommendations = "Make sure that the given certificate alias is not present " +
                    "in the certificate store."
        )
    },

    /**
     * Code: -333
     *
     * Type: CERTSTORE_MAX_SIZE_REACHED
     *
     * Cause(s):
     * - The maximum size of the certificate stores has been reached.
     *
     * Recommendation(s):
     * - Keep the number of certificate stores within the limit.
     */
    MAX_SIZE_REACHED(-333) {
        override fun getResponseModel() = ResponseModel(
            code = MAX_SIZE_REACHED.code,
            type = "CERTSTORE_MAX_SIZE_REACHED",
            causes = "The maximum size of the certificate stores has been reached.",
            recommendations = "Keep the number of certificate stores within the limit."
        )
    },


    /**
     * Code: -334
     *
     * Type: CERTSTORE_LOAD_E
     *
     * Cause(s):
     * - Error loading certificate store.
     *
     * Recommendation(s):
     * - NONE.
     */
    LOAD(-334) {
        override fun getResponseModel() = ResponseModel(
            code = LOAD.code,
            type = "CERTSTORE_LOAD_E",
            causes = "Error loading certificate store.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -335
     *
     * Type: CERTSTORE_SAVE_E
     *
     * Cause(s):
     * - Error saving certificate store.
     *
     * Recommendation(s):
     * - NONE.
     */
    SAVE(-335) {
        override fun getResponseModel() = ResponseModel(
            code = SAVE.code,
            type = "CERTSTORE_SAVE_E",
            causes = "Error saving certificate store.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            ALIAS_NOT_FOUND.code to ALIAS_NOT_FOUND.getResponseModel(),
            ALIAS_ALREADY_EXISTS.code to ALIAS_ALREADY_EXISTS.getResponseModel(),
            MAX_SIZE_REACHED.code to MAX_SIZE_REACHED.getResponseModel(),
            LOAD.code to LOAD.getResponseModel(),
            SAVE.code to SAVE.getResponseModel(),
        )
    }
}
