package identity.protection.responsecode.firmware

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of Trusted Storage response code.
 */
enum class TrustedStorageResponse(val code: Long) : Response {
    /**
     * Code: -1
     *
     * Type: TS_EXIST
     *
     * Cause(s):
     * - The trusted data store file, key–value pair, or encrypted derivation object already exist.
     *
     * Recommendation(s):
     * - Make sure the trusted data store file, key–value pair,
     * or encrypted derivation object does not already exist before calling the function.
     */
    EXIST(-1) {
        override fun getResponseModel() = ResponseModel(
            code = EXIST.code,
            type = "TS_EXIST",
            causes = "The trusted data store file, key–value pair, " +
                    "or encrypted derivation object already exist.",
            recommendations = "Make sure the trusted data store file, key–value pair, " +
                    "or encrypted derivation object does not already exist " +
                    "before calling the function."
        )
    },

    /**
     * Code: -2
     *
     * Type: TS_NOT_EXIST
     *
     * Cause(s):
     * - The trusted storage function is attempting to access a key–value pair
     * or encrypted derivation object which does not exist.
     *
     * Recommendation(s):
     * - Make sure that the key–value pair or encrypted derivation
     * object has already been added.
     */
    NOT_EXIST(-2) {
        override fun getResponseModel() = ResponseModel(
            code = NOT_EXIST.code,
            type = "TS_NOT_EXIST",
            causes = "The trusted storage function is attempting to access a key–value pair " +
                    "or encrypted derivation object which does not exist.",
            recommendations = "Make sure that the key–value pair or encrypted derivation " +
                    "object has already been added."
        )
    },

    /**
     * Code: -3
     *
     * Type: TS_MEMORY_FAILED
     *
     * Cause(s):
     * - The attempt to dynamically allocate memory for this function has failed.
     *
     * Recommendation(s):
     * - Make sure that unused memory is deallocated.
     */
    MEMORY_FAILED(-3) {
        override fun getResponseModel() = ResponseModel(
            code = MEMORY_FAILED.code,
            type = "TS_MEMORY_FAILED",
            causes = "The attempt to dynamically allocate memory for this function has failed.",
            recommendations = "Make sure that unused memory is deallocated."
        )
    },

    /**
     * Code: -4
     *
     * Type: TS_FILE_IO_FAILED
     *
     * Cause(s):
     * - Failed to access trusted data store/TALK/TRUST file.
     *
     * Recommendation(s):
     * - Make sure that the filename is correct.
     * - Make sure that the file exists when it is being opened.
     */
    FILE_IO_FAILED(-4) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_IO_FAILED.code,
            type = "TS_FILE_IO_FAILED",
            causes = "Failed to access trusted data store/TALK/TRUST file.",
            recommendations = "- Make sure that the filename is correct.\n" +
                    "- Make sure that the file exists when it is being opened."
        )
    },

    /**
     * Code: -5
     *
     * Type: TS_INTEGRITY_FAILED
     *
     * Cause(s):
     * - Trusted data store/TALK/TRUST file is tampered with.
     *
     * Recommendation(s):
     * - Make sure to use the correct assets.
     * - Make sure that the trusted data store/TALK/TRUST files are not corrupted.
     * - Make sure that the trusted data store/TALK/TRUST files has not been tampered with.
     */
    INTEGRITY_FAILED(-5) {
        override fun getResponseModel() = ResponseModel(
            code = INTEGRITY_FAILED.code,
            type = "TS_INTEGRITY_FAILED",
            causes = "Trusted data store/TALK/TRUST file is tampered with.",
            recommendations =
            "- Make sure to use the correct assets.\n" +
            "- Make sure that the trusted data store/TALK/TRUST files are not corrupted.\n" +
            "- Make sure that the trusted data store/TALK/TRUST files has not been tampered with."
        )
    },

    /**
     * Code: -6
     *
     * Type: TS_NOT_INITIALISED
     *
     * Cause(s):
     * - Trusted datastore/TALK/TRUST is not initialized.
     *
     * Recommendation(s):
     * - Make sure that you have initialized the trusted
     * data store/TALK/TRUST before using the function.
     */
    NOT_INITIALISED(-6) {
        override fun getResponseModel() = ResponseModel(
            code = NOT_INITIALISED.code,
            type = "TS_NOT_INITIALISED",
            causes = "Trusted datastore/TALK/TRUST is not initialized.",
            recommendations = "Make sure that you have initialized the trusted " +
                    "data store/TALK/TRUST before using the function.\n"
        )
    },

    /**
     * Code: -7
     *
     * Type: TS_BUFFER_TOO_SMALL
     *
     * Cause(s):
     * - The output parameter buffer supplied to the function is too small.
     *
     * Recommendation(s):
     * - Make sure that the output parameter provided has sufficient buffer size.
     */
    BUFFER_TOO_SMALL(-7) {
        override fun getResponseModel() = ResponseModel(
            code = BUFFER_TOO_SMALL.code,
            type = "TS_BUFFER_TOO_SMALL",
            causes = "The output parameter buffer supplied to the function is too small.",
            recommendations = "Make sure that the output parameter provided " +
                    "has sufficient buffer size."
        )
    },

    /**
     * Code: -8
     *
     * Type: TS_TRUST_KEY_RETRIEVAL_FAILED
     *
     * Cause(s):
     * - Failed to retrieve trust key.
     *
     * Recommendation(s):
     * - Make sure that the trusted storage is accessible and not corrupted.
     * - Clear trusted storage using the clearVOSTrustedStorage API
     * on V–OS Smart Token (Android) SDK.
     */
    TRUST_KEY_RETRIEVAL_FAILED(-8) {
        override fun getResponseModel() = ResponseModel(
            code = TRUST_KEY_RETRIEVAL_FAILED.code,
            type = "TS_TRUST_KEY_RETRIEVAL_FAILED",
            causes = "Failed to retrieve trust key.",
            recommendations =
            "- Make sure that the trusted storage is accessible and not corrupted.\n" +
                    "- Clear trusted storage using the clearVOSTrustedStorage API " +
                    "on V–OS Smart Token (Android) SDK."
        )
    },

    /**
     * Code: -9
     *
     * Type: TS_FILE_PARSE_FAILED
     *
     * Cause(s):
     * - Failed to parse the trust key file.
     *
     * Recommendation(s):
     * - NONE.
     */
    FILE_PARSE_FAILED(-9) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_PARSE_FAILED.code,
            type = "TS_FILE_PARSE_FAILED",
            causes = "Failed to parse the trust key file.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -10
     *
     * Type: TS_FILE_DFP_VERSION_NOT_SUPPORTED
     *
     * Cause(s):
     * - The DFP version of the trust key file is not supported.
     *
     * Recommendation(s):
     * - NONE.
     */
    FILE_DFP_VERSION_NOT_SUPPORTED(-10) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_DFP_VERSION_NOT_SUPPORTED.code,
            type = "TS_FILE_DFP_VERSION_NOT_SUPPORTED",
            causes = "The DFP version of the trust key file is not supported.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            EXIST.code to EXIST.getResponseModel(),
            NOT_EXIST.code to NOT_EXIST.getResponseModel(),
            MEMORY_FAILED.code to MEMORY_FAILED.getResponseModel(),
            FILE_IO_FAILED.code to FILE_IO_FAILED.getResponseModel(),
            INTEGRITY_FAILED.code to INTEGRITY_FAILED.getResponseModel(),
            NOT_INITIALISED.code to NOT_INITIALISED.getResponseModel(),
            BUFFER_TOO_SMALL.code to BUFFER_TOO_SMALL.getResponseModel(),
            TRUST_KEY_RETRIEVAL_FAILED.code to TRUST_KEY_RETRIEVAL_FAILED.getResponseModel(),
            FILE_PARSE_FAILED.code to FILE_PARSE_FAILED.getResponseModel(),
            FILE_DFP_VERSION_NOT_SUPPORTED.code to FILE_DFP_VERSION_NOT_SUPPORTED.getResponseModel(),
        )
    }
}
