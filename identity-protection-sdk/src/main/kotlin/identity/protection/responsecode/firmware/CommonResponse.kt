package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Common response code.
 */
enum class CommonResponse(val code: Long) : Response {
    /**
     * Code: -351
     *
     * Type: UNSUPPORTED_PADDING
     *
     * Cause(s):
     * - The padding type is not supported.
     *
     * Recommendation(s):
     * - Make sure that only the supported padding type is used.
     */
    UNSUPPORTED_PADDING(-351) {
        override fun getResponseModel() = ResponseModel(
            code = UNSUPPORTED_PADDING.code,
            type = "UNSUPPORTED_PADDING",
            causes = "The padding type is not supported.",
            recommendations = "Make sure that only the supported padding type is used."
        )
    },

    /**
     * Code: -360
     *
     * Type: UNKNOWN_STORETYPE
     *
     * Cause(s):
     * - The trusted storage type is unknown.
     *
     * Recommendation(s):
     * - NONE.
     */
    UNKNOWN_STORETYPE(-360) {
        override fun getResponseModel() = ResponseModel(
            code = UNKNOWN_STORETYPE.code,
            type = "UNKNOWN_STORETYPE",
            causes = "The trusted storage type is unknown.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -
     *
     * Type: FSM_STATE_NOT_FOUND
     *
     * Cause(s):
     * - Finite State Machine state is not found.
     *
     * Recommendation(s):
     * - NONE.
     */
    FSM_STATE_NOT_FOUND(-601) {
        override fun getResponseModel() = ResponseModel(
            code = FSM_STATE_NOT_FOUND.code,
            type = "FSM_STATE_NOT_FOUND",
            causes = "Finite State Machine state is not found.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -751
     *
     * Type: SECURE_FILE_IO
     *
     * Cause(s):
     * - SecureFileIO error.
     *
     * Recommendation(s):
     * - NONE.
     */
    SECURE_FILE_IO(-751) {
        override fun getResponseModel() = ResponseModel(
            code = SECURE_FILE_IO.code,
            type = "SECURE_FILE_IO",
            causes = "SecureFileIO error.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -901
     *
     * Type: UNSUPPORTED
     *
     * Cause(s):
     * - The mode of operation is not supported.
     *
     * Recommendation(s):
     * - NONE.
     */
    UNSUPPORTED(-901) {
        override fun getResponseModel() = ResponseModel(
            code = UNSUPPORTED.code,
            type = "UNSUPPORTED",
            causes = "The mode of operation is not supported.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -999
     *
     * Type: UNSUPPORTED_CRYPT_FUNC
     *
     * Cause(s):
     * - The cryptographic function is not supported.
     *
     * Recommendation(s):
     * - Make sure that only the supported cryptographic function is used.
     */
    UNSUPPORTED_CRYPT_FUNC(-999) {
        override fun getResponseModel() = ResponseModel(
            code = UNSUPPORTED_CRYPT_FUNC.code,
            type = "UNSUPPORTED_CRYPT_FUNC",
            causes = "The cryptographic function is not supported.",
            recommendations = "Make sure that only the supported cryptographic function is used."
        )
    },

    /**
     * Code: -1000
     *
     * Type: ADS_ID_NOT_SUPPORTED
     *
     * Cause(s):
     * - Advertising ID is not supported
     *
     * Recommendation(s):
     * - NONE.
     */
    ADS_ID_NOT_SUPPORTED(-1000) {
        override fun getResponseModel() = ResponseModel(
            code = ADS_ID_NOT_SUPPORTED.code,
            type = "ADS_ID_NOT_SUPPORTED",
            causes = "Advertising lD is not supported",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1002
     *
     * Type: ADS_ID_NOT_AVAILABLE
     *
     * Cause(s):
     * - Advertising ID is not available
     *
     * Recommendation(s):
     * - NONE.
     */
    ADS_ID_NOT_AVAILABLE(-1002) {
        override fun getResponseModel() = ResponseModel(
            code = ADS_ID_NOT_AVAILABLE.code,
            type = "ADS_ID_NOT_AVAILABLE",
            causes = "Advertising ID is not available",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1010
     *
     * Type: NULL_DBG_CHECK_CALLBACK_LOCK
     *
     * Cause(s):
     * - Null debugger check callback locking reason.
     *
     * Recommendation(s):
     * - NONE.
     */
    NULL_DBG_CHECK_CALLBACK_LOCK(-1010) {
        override fun getResponseModel() = ResponseModel(
            code = NULL_DBG_CHECK_CALLBACK_LOCK.code,
            type = "NULL_DBG_CHECK_CALLBACK_LOCK",
            causes = "Null debugger check callback locking reason.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1011
     *
     * Type: NULL_DFP_HOOKED_CALLBACK_LOCK
     *
     * Cause(s):
     * - System functions used to obtain DFP has been hooked.
     *
     * Recommendation(s):
     * - Make sure that the function is not hooked.
     */
    NULL_DFP_HOOKED_CALLBACK_LOCK(-1011) {
        override fun getResponseModel() = ResponseModel(
            code = NULL_DFP_HOOKED_CALLBACK_LOCK.code,
            type = "NULL_DFP_HOOKED_CALLBACK_LOCK",
            causes = "System functions used to obtain DFP has been hooked.",
            recommendations = "Make sure that the function is not hooked."
        )
    },

    /**
     * Code: -1013
     *
     * Type: NULL_TIME_HOOKED_CALLBACK_LOCK
     *
     * Cause(s):
     * - The system functions used to obtain time has been hooked.
     *
     * Recommendation(s):
     * - Make sure that the function is not hooked.
     */
    NULL_TIME_HOOKED_CALLBACK_LOCK(-1013) {
        override fun getResponseModel() = ResponseModel(
            code = NULL_TIME_HOOKED_CALLBACK_LOCK.code,
            type = "NULL_TIME_HOOKED_CALLBACK_LOCK",
            causes = "The system functions used to obtain time has been hooked.",
            recommendations = "Make sure that the function is not hooked."
        )
    },

    /**
     * Code: -1018
     *
     * Type: UNHANDLED_LOCK_CODE
     *
     * Cause(s):
     * - V–OS is locked because of unhandled events.
     *
     * Recommendation(s):
     * - NONE.
     */
    UNHANDLED_LOCK_CODE(-1018) {
        override fun getResponseModel() = ResponseModel(
            code = UNHANDLED_LOCK_CODE.code,
            type = "UNHANDLED_LOCK_CODE",
            causes = "V–OS is locked because of unhandled events.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1019
     *
     * Type: LIBRARY_HOOKED
     *
     * Cause(s):
     * - V–OS is locked because there is shadow hooking detected.
     *
     * Recommendation(s):
     * - Make sure that shadow hooking is not present.
     */
    LIBRARY_HOOKED(-1019) {
        override fun getResponseModel() = ResponseModel(
            code = LIBRARY_HOOKED.code,
            type = "LIBRARY_HOOKED",
            causes = "V–OS is locked because there is shadow hooking detected.",
            recommendations = "Make sure that shadow hooking is not present."
        )
    },

    /**
     * Code: -1020
     *
     * Type: TRANS_DBG_LOCK
     *
     * Cause(s):
     * - V–OS is locked because there is a debugger detected by the transient check.
     *
     * Recommendation(s):
     * - Do not use a debugger.
     * - Use a debuggable version of V–OS.
     */
    TRANS_DBG_LOCK(-1020) {
        override fun getResponseModel() = ResponseModel(
            code = TRANS_DBG_LOCK.code,
            type = "TRANS_DBG_LOCK",
            causes = "V–OS is locked because there is a debugger detected by the transient check.",
            recommendations = "- Do not use a debugger.\n" +
                    "- Use a debuggable version of V–OS."
        )
    },

    /**
     * Code: -1021
     *
     * Type: PER_DBG_LOCK
     *
     * Cause(s):
     * - There is a debugger detected by the periodic check.
     * - There is runtime tampering such as binary patching, code injection, etc.
     *
     * Recommendation(s):
     * - Do not use a debugger (note that running in Xcode is counted as using debugger).
     */
    PER_DBG_LOCK(-1021) {
        override fun getResponseModel() = ResponseModel(
            code = PER_DBG_LOCK.code,
            type = "PER_DBG_LOCK",
            causes = "- There is a debugger detected by the periodic check.\n" +
                    "- There is runtime tampering such as binary patching, code injection, etc.",
            recommendations = "- Do not use a debugger " +
                    "(note that running in Xcode is counted as using debugger).\n" +
                    "- Make sure that the correct voscodedesign.vky asset is used.\n" +
                    "- Use a debuggable version of V–OS."
        )
    },

    /**
     * Code: -1029
     *
     * Type: VKY_CODE_SIGN_TAMPERED_LOCK
     *
     * Cause(s):
     * - voscodedesign.vky has been tampered with.
     *
     * Recommendation(s):
     * - Make sure that the correct voscodedesign.vky asset is used.
     */
    VKY_CODE_SIGN_TAMPERED_LOCK(-1029) {
        override fun getResponseModel() = ResponseModel(
            code = VKY_CODE_SIGN_TAMPERED_LOCK.code,
            type = "VKY_CODE_SIGN_TAMPERED_LOCK",
            causes = "voscodedesign.vky has been tampered with.",
            recommendations = "- Make sure that the correct voscodedesign.vky asset is used."
        )
    },

    /**
     * Code: -1034
     *
     * Type: WATCH_DOG_DBG_LOCK
     *
     * Cause(s):
     * - V–OS is locked because debugger is detected.
     *
     * Recommendation(s):
     * - Do not use a debugger.
     * - Use a debuggable version of V–OS.
     */
    WATCH_DOG_DBG_LOCK(-1034) {
        override fun getResponseModel() = ResponseModel(
            code = WATCH_DOG_DBG_LOCK.code,
            type = "WATCH_DOG_DBG_LOCK",
            causes = "V–OS is locked because debugger is detected.",
            recommendations = "- Do not use a debugger.\n" +
                    "- Use a debuggable version of V–OS."
        )
    },

    /**
     * Code: -1035
     *
     * Type: UNTRUSTED_NATIVE_LIBRARY_LOCK
     *
     * Cause(s):
     * - V–OS is locked because an unknown library is detected.
     *
     * Recommendation(s):
     * - NONE.
     */
    UNTRUSTED_NATIVE_LIBRARY_LOCK(-1035) {
        override fun getResponseModel() = ResponseModel(
            code = UNTRUSTED_NATIVE_LIBRARY_LOCK.code,
            type = "UNTRUSTED_NATIVE_LIBRARY_LOCK",
            causes = "V–OS is locked because an unknown library is detected.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1038
     *
     * Type: LOCAL_LOCK_INFO_TAMPERED
     *
     * Cause(s):
     * - Local lock mechanism has been tampered with.
     *
     * Recommendation(s):
     * - Make sure that the local lock mechanism is not tampered with.
     */
    LOCAL_LOCK_INFO_TAMPERED(-1038) {
        override fun getResponseModel() = ResponseModel(
            code = LOCAL_LOCK_INFO_TAMPERED.code,
            type = "LOCAL_LOCK_INFO_TAMPERED",
            causes = "Local lock mechanism has been tampered with.",
            recommendations = "Make sure that the local lock mechanism is not tampered with."
        )
    },

    /**
     * Code: -1041
     *
     * Type: NATIVE_CODE_SEGMENT_HMAC_FAILED
     *
     * Cause(s):
     * - Failed to obtain native code segment HMAC hash.
     *
     * Recommendation(s):
     * - NONE.
     */
    NATIVE_CODE_SEGMENT_HMAC_FAILED(-1041) {
        override fun getResponseModel() = ResponseModel(
            code = NATIVE_CODE_SEGMENT_HMAC_FAILED.code,
            type = "NATIVE_CODE_SEGMENT_HMAC_FAILED",
            causes = "Failed to obtain native code segment HMAC hash.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1051
     *
     * Type: THREAT_INTELLIGENCE_INVALID_ARGS
     *
     * Cause(s):
     * The JWS verification type is not supported (only support CFO_VERIFY and CEL_VERIFY)
     * or the imported JWS string:
     *
     * - is NULL
     * - has a string length of less than 0, or
     * - has a format that does not have two dots
     *
     * Recommendation(s):
     * - Make sure the verification type is the supported type.
     * - Make sure that the format of input JWS string and its length are correct
     */
    THREAT_INTELLIGENCE_INVALID_ARGS(-1051) {
        override fun getResponseModel() = ResponseModel(
            code = THREAT_INTELLIGENCE_INVALID_ARGS.code,
            type = "THREAT_INTELLIGENCE_INVALID_ARGS",
            causes = "The JWS verification type is not supported " +
                    "(only support CFO_VERIFY and CEL_VERIFY) " +
                    "or the imported JWS string: \n" +
                    "- is NULL.\n" +
                    "- has a string length of less than 0, or.\n" +
                    "- has a format that does not have two dots",
            recommendations = "- Make sure the verification type is the supported type.\n" +
                    "- Make sure that the format of input JWS string and its length are correct."
        )
    },

    /**
     * Code: -1052
     *
     * Type: THREAT_INTELLIGENCE_PARSE_FAILED
     *
     * Cause(s):
     * - Failed to parse the input JWS string for header/payload/signature due to wrong length
     * or base64url decoding failed.
     *
     * Recommendation(s):
     * - Make sure that the format of input JWS string and its length are correct.
     */
    THREAT_INTELLIGENCE_PARSE_FAILED(-1052) {
        override fun getResponseModel() = ResponseModel(
            code = THREAT_INTELLIGENCE_PARSE_FAILED.code,
            type = "THREAT_INTELLIGENCE_PARSE_FAILED",
            causes = "Failed to parse the input JWS string for header/payload/signature " +
                    "due to wrong length or base64url decoding failed.",
            recommendations = "Make sure that the format of input JWS string and " +
                    "its length are correct."
        )
    },

    /**
     * Code: -1053
     *
     * Type: THREAT_INTELLIGENCE_INVALID_HEADER
     *
     * Cause(s):
     * - The JSON header indicates that unsupported algorithm is used
     * (now only support ES256 and RS256), the certificate hash is NULL,
     * or the certificate hash length is not 64.
     *
     * Recommendation(s):
     * - Make sure that the format of input JWS string and its length are correct.
     */
    THREAT_INTELLIGENCE_INVALID_HEADER(-1053) {
        override fun getResponseModel() = ResponseModel(
            code = THREAT_INTELLIGENCE_INVALID_HEADER.code,
            type = "THREAT_INTELLIGENCE_INVALID_HEADER",
            causes = "The JSON header indicates that unsupported algorithm is used " +
                    "(now only support ES256 and RS256), " +
                    "the certificate hash is NULL, or the certificate hash length is not 64.",
            recommendations = "Make sure that the format of input JWS string and " +
                    "its length are correct."
        )
    },

    /**
     * Code: -1046
     *
     * Type: FRIDA_DETECTED
     *
     * Cause(s):
     * - V–OS is locked because Frida is detected.
     *
     * Recommendation(s):
     * - Make sure Frida is not installed.
     */
    FRIDA_DETECTED(-1046) {
        override fun getResponseModel() = ResponseModel(
            code = FRIDA_DETECTED.code,
            type = "FRIDA_DETECTED",
            causes = "V–OS is locked because Frida is detected.",
            recommendations = "Make sure Frida is not installed."
        )
    },

    /**
     * Code: -1047
     *
     * Type: MAGISK_DETECTED
     *
     * Cause(s):
     * - V–OS is locked because Magisk is detected
     *
     * Recommendation(s):
     * - Make sure that Magisk is not installed.
     */
    MAGISK_DETECTED(-1047) {
        override fun getResponseModel() = ResponseModel(
            code = MAGISK_DETECTED.code,
            type = "MAGISK_DETECTED",
            causes = "V–OS is locked because Magisk is detected",
            recommendations = "Make sure that Magisk is not installed."
        )
    },

    /**
     * Code: -1350
     *
     * Type: IRK_IO_ERROR
     *
     * Cause(s):
     * - Error accessing IRK.
     *
     * Recommendation(s):
     * - NONE.
     */
    IRK_IO_ERROR(-1350) {
        override fun getResponseModel() = ResponseModel(
            code = IRK_IO_ERROR.code,
            type = "IRK_IO_ERROR",
            causes = "Error accessing IRK.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1360
     *
     * Type: PREFERENCE_TYPE_NOT_SUPPORTED
     *
     * Cause(s):
     * - The preference type is not supported.
     *
     * Recommendation(s):
     * - NONE.
     */
    PREFERENCE_TYPE_NOT_SUPPORTED(-1360) {
        override fun getResponseModel() = ResponseModel(
            code = PREFERENCE_TYPE_NOT_SUPPORTED.code,
            type = "PREFERENCE_TYPE_NOT_SUPPORTED",
            causes = "The preference type is not supported.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1373
     *
     * Type: TID_GET_TIDEK_FAILED
     *
     * Cause(s):
     * - Failed to generate stable troubleshooting ID (TID) encryption key.
     *
     * Recommendation(s):
     * - NONE.
     */
    TID_GET_TIDEK_FAILED(-1373) {
        override fun getResponseModel() = ResponseModel(
            code = TID_GET_TIDEK_FAILED.code,
            type = "TID_GET_TIDEK_FAILED",
            causes = "Failed to generate stable troubleshooting lD (TlD) encryption key.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            UNSUPPORTED_PADDING.code to UNSUPPORTED_PADDING.getResponseModel(),
            UNKNOWN_STORETYPE.code to UNKNOWN_STORETYPE.getResponseModel(),
            FSM_STATE_NOT_FOUND.code to FSM_STATE_NOT_FOUND.getResponseModel(),
            SECURE_FILE_IO.code to SECURE_FILE_IO.getResponseModel(),
            UNSUPPORTED.code to UNSUPPORTED.getResponseModel(),
            UNSUPPORTED_CRYPT_FUNC.code to UNSUPPORTED_CRYPT_FUNC.getResponseModel(),
            ADS_ID_NOT_SUPPORTED.code to ADS_ID_NOT_SUPPORTED.getResponseModel(),
            ADS_ID_NOT_AVAILABLE.code to ADS_ID_NOT_AVAILABLE.getResponseModel(),
            NULL_DFP_HOOKED_CALLBACK_LOCK.code to NULL_DFP_HOOKED_CALLBACK_LOCK.getResponseModel(),
            NULL_DBG_CHECK_CALLBACK_LOCK.code to NULL_DBG_CHECK_CALLBACK_LOCK.getResponseModel(),
            NULL_TIME_HOOKED_CALLBACK_LOCK.code to NULL_TIME_HOOKED_CALLBACK_LOCK.getResponseModel(),
            UNHANDLED_LOCK_CODE.code to UNHANDLED_LOCK_CODE.getResponseModel(),
            LIBRARY_HOOKED.code to LIBRARY_HOOKED.getResponseModel(),
            TRANS_DBG_LOCK.code to TRANS_DBG_LOCK.getResponseModel(),
            PER_DBG_LOCK.code to PER_DBG_LOCK.getResponseModel(),
            VKY_CODE_SIGN_TAMPERED_LOCK.code to VKY_CODE_SIGN_TAMPERED_LOCK.getResponseModel(),
            WATCH_DOG_DBG_LOCK.code to WATCH_DOG_DBG_LOCK.getResponseModel(),
            UNTRUSTED_NATIVE_LIBRARY_LOCK.code to UNTRUSTED_NATIVE_LIBRARY_LOCK.getResponseModel(),
            LOCAL_LOCK_INFO_TAMPERED.code to LOCAL_LOCK_INFO_TAMPERED.getResponseModel(),
            NATIVE_CODE_SEGMENT_HMAC_FAILED.code to NATIVE_CODE_SEGMENT_HMAC_FAILED.getResponseModel(),
            THREAT_INTELLIGENCE_INVALID_ARGS.code to THREAT_INTELLIGENCE_INVALID_ARGS.getResponseModel(),
            THREAT_INTELLIGENCE_PARSE_FAILED.code to THREAT_INTELLIGENCE_PARSE_FAILED.getResponseModel(),
            THREAT_INTELLIGENCE_INVALID_HEADER.code to THREAT_INTELLIGENCE_INVALID_HEADER.getResponseModel(),
            FRIDA_DETECTED.code to FRIDA_DETECTED.getResponseModel(),
            MAGISK_DETECTED.code to MAGISK_DETECTED.getResponseModel(),
            IRK_IO_ERROR.code to IRK_IO_ERROR.getResponseModel(),
            PREFERENCE_TYPE_NOT_SUPPORTED.code to PREFERENCE_TYPE_NOT_SUPPORTED.getResponseModel(),
            TID_GET_TIDEK_FAILED.code to TID_GET_TIDEK_FAILED.getResponseModel()
        )
    }
}
