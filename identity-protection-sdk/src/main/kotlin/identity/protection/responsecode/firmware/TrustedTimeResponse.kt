package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Trusted Time response code.
 */
enum class TrustedTimeResponse(val code: Long) : Response {
    /**
     * Code: -1100
     *
     * Type: TRUSTED_TIME_CANNOT_GENERATE_KEY
     *
     * Cause(s):
     * - Failed to generate trusted time key (TTK).
     *
     * Recommendation(s):
     * - NONE.
     */
    CANNOT_GENERATE_KEY(-1100) {
        override fun getResponseModel() = ResponseModel(
            code = CANNOT_GENERATE_KEY.code,
            type = "TRUSTED_TIME_CANNOT_GENERATE_KEY",
            causes = "Failed to generate trusted time key (TTK).",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1101
     *
     * Type: TRUSTED_TIME_CANNOT_GENERATE_TTK
     *
     * Cause(s):
     * - Failed to generate trusted time key (TTK).
     *
     * Recommendation(s):
     * - Make sure that the assets files are valid.
     */
    CANNOT_GENERATE_TTK(-1101) {
        override fun getResponseModel() = ResponseModel(
            code = CANNOT_GENERATE_TTK.code,
            type = "TRUSTED_TIME_CANNOT_GENERATE_TTK",
            causes = "Failed to generate trusted time key (TTK).",
            recommendations = "Make sure that the assets files are valid."
        )
    },

    /**
     * Code: -1102
     *
     * Type: TRUSTED_TIME_NO_CUSTOMER_ID
     *
     * Cause(s):
     * - Failed to obtain customer key when getting trusted time.
     *
     * Recommendation(s):
     * - Make sure the vkeylicensepack file is valid.
     */
    NO_CUSTOMER_ID(-1102) {
        override fun getResponseModel() = ResponseModel(
            code = NO_CUSTOMER_ID.code,
            type = "TRUSTED_TIME_NO_CUSTOMER_ID",
            causes = "Failed to obtain customer key when getting trusted time.",
            recommendations = "Make sure the vkeylicensepack file is valid."
        )
    },

    /**
     * Code: -1103
     *
     * Type: TRUSTED_TIME_CANNOT_DERIVE_PASSWORD
     *
     * Cause(s):
     * - Failed to generate trusted time customer password.
     *
     * Recommendation(s):
     * - NONE.
     */
    CANNOT_DERIVE_PASSWORD(-1103) {
        override fun getResponseModel() = ResponseModel(
            code = CANNOT_DERIVE_PASSWORD.code,
            type = "TRUSTED_TIME_CANNOT_DERIVE_PASSWORD",
            causes = "Failed to generate trusted time customer password.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1104
     *
     * Type: TRUSTED_TIME_HMAC_SHA_FAILED
     *
     * Cause(s):
     * - Failed to calculate signature while decrypting trusted time server response.
     *
     * Recommendation(s):
     * - Make sure there is no network disruption.
     * - Make sure the trusted time server is correct.
     */
    HMAC_SHA_FAILED(-1104) {
        override fun getResponseModel() = ResponseModel(
            code = HMAC_SHA_FAILED.code,
            type = "TRUSTED_TIME_HMAC_SHA_FAILED",
            causes = "Failed to calculate signature while decrypting trusted time server response.",
            recommendations = "- Make sure there is no network disruption.\n" +
                    "- Make sure the trusted time server is correct."
        )
    },

    /**
     * Code: -1105
     *
     * Type: TRUSTED_TIME_AES_CBC_FAILED
     *
     * Cause(s):
     * - Failed to decrypt time server response.
     *
     * Recommendation(s):
     * - Make sure there is no network disruption.
     * - Make sure the trusted time server is correct.
     */
    AES_CBC_FAILED(-1105) {
        override fun getResponseModel() = ResponseModel(
            code = AES_CBC_FAILED.code,
            type = "TRUSTED_TIME_AES_CBC_FAILED",
            causes = "Failed to decrypt time server response.",
            recommendations = "- Make sure there is no network disruption.\n" +
                    "- Make sure the trusted time server is correct."
        )
    },

    /**
     * Code: -1106
     *
     * Type: TRUSTED_TIME_SIGNATURE_MISMATCH
     *
     * Cause(s):
     * - There is mismatch trusted time response signature.
     *
     * Recommendation(s):
     * - Make sure the trusted time server has matching crypto mode (light or strong).
     * - Make sure the trusted time server is correct.
     */
    SIGNATURE_MISMATCH(-1106) {
        override fun getResponseModel() = ResponseModel(
            code = SIGNATURE_MISMATCH.code,
            type = "TRUSTED_TIME_SIGNATURE_MISMATCH",
            causes = "There is mismatch trusted time response signature.",
            recommendations = "- Make sure the trusted time server has matching " +
                    "crypto mode (light or strong).\n" +
                    "- Make sure the trusted time server is correct."
        )
    },

    /**
     * Code: -1107
     *
     * Type: TRUSTED_TIME_RESPONSE_BAD_FORMAT
     *
     * Cause(s):
     * - The trusted time server response format is incorrect.
     *
     * Recommendation(s):
     * - Make sure the trusted time server is correct.
     */
    RESPONSE_BAD_FORMAT(-1107) {
        override fun getResponseModel() = ResponseModel(
            code = RESPONSE_BAD_FORMAT.code,
            type = "TRUSTED_TIME_RESPONSE_BAD_FORMAT",
            causes = "The trusted time server response format is incorrect.",
            recommendations = "Make sure the trusted time server is correct."
        )
    },

    /**
     * Code: -1108
     *
     * Type: TRUSTED_TIME_SERVER_URL_TOO_LONG
     *
     * Cause(s):
     * - The trusted time server URL is too long.
     *
     * Recommendation(s):
     * - Make sure that the trusted time server URL is not more than 256 characters.
     */
    SERVER_URL_TOO_LONG(-1108) {
        override fun getResponseModel() = ResponseModel(
            code = SERVER_URL_TOO_LONG.code,
            type = "TRUSTED_TIME_SERVER_URL_TOO_LONG",
            causes = "The trusted time server URL is too long.",
            recommendations = "Make sure that the trusted time server URL is " +
                    "not more than 256 characters."
        )
    },

    /**
     * Code: -1109
     *
     * Type: TRUSTED_TIME_SERVER_URL_ALREADY_SET
     *
     * Cause(s):
     * - The trusted time server URL has already been set.
     *
     * Recommendation(s):
     * - The trusted time server URL only need to be set once. No further action necessary.
     */
    SERVER_URL_ALREADY_SET(-1109) {
        override fun getResponseModel() = ResponseModel(
            code = SERVER_URL_ALREADY_SET.code,
            type = "TRUSTED_TIME_SERVER_URL_ALREADY_SET",
            causes = "The trusted time server URL has already been set.",
            recommendations = "The trusted time server URL only need to be set once. " +
                    "No further action necessary."
        )
    },

    /**
     * Code: -1110
     *
     * Type: TRUSTED_TIME_SERVER_URL_NOT_SET
     *
     * Cause(s):
     * - The trusted time server URL not set.
     *
     * Recommendation(s):
     * - Make sure to set the trusted time server URL before calling this function.
     */
    SERVER_URL_NOT_SET(-1110) {
        override fun getResponseModel() = ResponseModel(
            code = SERVER_URL_NOT_SET.code,
            type = "TRUSTED_TIME_SERVER_URL_NOT_SET",
            causes = "The trusted time server URL not set.",
            recommendations = "Make sure to set the trusted time server URL " +
                    "before calling this function."
        )
    },

    /**
     * Code: -1111
     *
     * Type: TRUSTED_TIME_REQUEST_IN_PROGRESS
     *
     * Cause(s):
     * - Get trusted time request in progress.
     *
     * Recommendation(s):
     * - Make sure there is no network disruption.
     * - Make sure that the app does not go into background during provisioning stage.
     * - Make sure that you do not request for trusted time immediately after setting the
     * trusted time server URL. Give a 5–second gap between the two function calls.
     */
    REQUEST_IN_PROGRESS(-1111) {
        override fun getResponseModel() = ResponseModel(
            code = REQUEST_IN_PROGRESS.code,
            type = "TRUSTED_TIME_REQUEST_IN_PROGRESS",
            causes = "Get trusted time request in progress.",
            recommendations = "- Make sure there is no network disruption.\n" +
                    "- Make sure that the app does not go into background during provisioning stage.\n" +
                    "- Make sure that you do not request for trusted time immediately " +
                    "after setting the trusted time server URL. " +
                    "Give a 5–second gap between the two function calls."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            CANNOT_GENERATE_KEY.code to CANNOT_GENERATE_KEY.getResponseModel(),
            CANNOT_GENERATE_TTK.code to CANNOT_GENERATE_TTK.getResponseModel(),
            NO_CUSTOMER_ID.code to NO_CUSTOMER_ID.getResponseModel(),
            CANNOT_DERIVE_PASSWORD.code to CANNOT_DERIVE_PASSWORD.getResponseModel(),
            HMAC_SHA_FAILED.code to HMAC_SHA_FAILED.getResponseModel(),
            AES_CBC_FAILED.code to AES_CBC_FAILED.getResponseModel(),
            SIGNATURE_MISMATCH.code to SIGNATURE_MISMATCH.getResponseModel(),
            RESPONSE_BAD_FORMAT.code to RESPONSE_BAD_FORMAT.getResponseModel(),
            SERVER_URL_TOO_LONG.code to SERVER_URL_TOO_LONG.getResponseModel(),
            SERVER_URL_ALREADY_SET.code to SERVER_URL_ALREADY_SET.getResponseModel(),
            SERVER_URL_NOT_SET.code to SERVER_URL_NOT_SET.getResponseModel(),
            REQUEST_IN_PROGRESS.code to REQUEST_IN_PROGRESS.getResponseModel(),
        )
    }
}
