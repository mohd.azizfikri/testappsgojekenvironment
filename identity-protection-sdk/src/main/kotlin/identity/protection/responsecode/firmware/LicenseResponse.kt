package identity.protection.responsecode.firmware

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of License response code.
 */
enum class LicenseResponse(val code: Long) : Response {
    /**
     * Code: -1004
     *
     * Type: LICENSE_SIGNER_RETRIEVE_FAILED
     *
     * Cause(s):
     * - License check failed. Cannot retrieve app signer info from the app.
     *
     * Recommendation(s):
     * - Make sure that the correct license file is used.
     * - Make sure that the license file is generated correctly.
     */
    SIGNER_RETRIEVE_FAILED(-1004) {
        override fun getResponseModel() = ResponseModel(
            code = SIGNER_RETRIEVE_FAILED.code,
            type = "LICENSE_SIGNER_RETRIEVE_FAILED",
            causes = "License check failed. Cannot retrieve app signer info from the app.",
            recommendations = "- Make sure that the correct license file is used\n." +
                    "- Make sure that the license file is generated correctly."
        )
    },

    /**
     * Code: -1005
     *
     * Type: LICENSE_SIGNER_MISMATCH
     *
     * Cause(s):
     * - License check failed. App signer mismatch in license file.
     *
     * Recommendation(s):
     * - Make sure that the correct license file is used.
     * - Make sure that the license file is generated correctly.
     */
    SIGNER_MISMATCH(-1005) {
        override fun getResponseModel() = ResponseModel(
            code = SIGNER_MISMATCH.code,
            type = "LICENSE_SIGNER_MISMATCH",
            causes = "License check failed. App signer mismatch in license file.",
            recommendations = "- Make sure that the correct license file is used.\n" +
                    "- Make sure that the license file is generated correctly."
        )
    },

    /**
     * Code: -1006
     *
     * Type: LICENSE_APP_ID_MISMATCH
     *
     * Cause(s):
     * - License check failed. App ID mismatch in license file.
     *
     * Recommendation(s):
     * - Make sure that the correct license file is used.
     * - Make sure that the license file is generated correctly.
     */
    APP_ID_MISMATCH(-1006) {
        override fun getResponseModel() = ResponseModel(
            code = APP_ID_MISMATCH.code,
            type = "LICENSE_APP_ID_MISMATCH",
            causes = "License check failed. App ID mismatch in license file.",
            recommendations = "- Make sure that the correct license file is used.\n" +
                    "- Make sure that the license file is generated correctly."
        )
    },

    /**
     * Code: -1007
     *
     * Type: LICENSE_FILE_NOT_FOUND
     *
     * Cause(s):
     * - License check failed. License file not found.
     *
     * Recommendation(s):
     * - Make sure that the license file is provided.
     */
    FILE_NOT_FOUND(-1007) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_NOT_FOUND.code,
            type = "LICENSE_FILE_NOT_FOUND",
            causes = "License check failed. License file not found.",
            recommendations = "Make sure that the license file is provided."
        )
    },

    /**
     * Code: -1008
     *
     * Type: LICENSE_FILE_INVALID
     *
     * Cause(s):
     * - License check failed. License file format is invalid.
     *
     * Recommendation(s):
     * - Make sure that the correct license file is used.
     * - Make sure that the license file is generated correctly.
     */
    FILE_INVALID(-1008) {
        override fun getResponseModel() = ResponseModel(
            code = FILE_INVALID.code,
            type = "LICENSE_FILE_INVALID",
            causes = "License check failed. License file format is invalid.",
            recommendations = "- Make sure that the correct license file is used.\n" +
                    "- Make sure that the license file is generated correctly."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            SIGNER_RETRIEVE_FAILED.code to SIGNER_RETRIEVE_FAILED.getResponseModel(),
            SIGNER_MISMATCH.code to SIGNER_MISMATCH.getResponseModel(),
            APP_ID_MISMATCH.code to APP_ID_MISMATCH.getResponseModel(),
            FILE_NOT_FOUND.code to FILE_NOT_FOUND.getResponseModel(),
            FILE_INVALID.code to FILE_INVALID.getResponseModel()
        )
    }
}
