package identity.protection.responsecode.firmware

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of Virtual Machine response code.
 */
enum class VMResponse(val code: Long) : Response {
    /**
     * Code: -125
     *
     * Type: VM_MEMORY_E
     *
     * Cause(s):
     * - The Virtual Machine (VM) is out of memory.
     *
     * Recommendation(s):
     * - Make sure that sufficient memory is allocated to the VM.
     */
    OUT_OF_MEMORY(-125) {
        override fun getResponseModel() = ResponseModel(
            code = OUT_OF_MEMORY.code,
            type = "VM_MEMORY_E",
            causes = "The Virtual Machine (VM) is out of memory.",
            recommendations = "Make sure that sufficient memory is allocated to the VM."
        )
    },

    /**
     * Code: -173
     *
     * Type: VM_BAD_FUNC_ARG
     *
     * Cause(s):
     * - The function argument provided is invalid.
     *
     * Recommendation(s):
     * - Make sure that the function arguments provided are valid.
     */
    BAD_FUNC_ARG(-173) {
        override fun getResponseModel() = ResponseModel(
            code = BAD_FUNC_ARG.code,
            type = "VM_BAD_FUNC_ARG",
            causes = "The function argument provided is invalid.",
            recommendations = "Make sure that the function arguments provided are valid."
        )
    },

    /**
     * Code: -990
     *
     * Type: VM_RESULT_ERROR
     *
     * Cause(s):
     * - Error occurred in cryptographic function.
     *
     * Recommendation(s):
     * - NONE.
     */
    RESULT_ERROR(-990) {
        override fun getResponseModel() = ResponseModel(
            code = RESULT_ERROR.code,
            type = "VM_RESULT_ERROR",
            causes = "Error occurred in cryptographic function.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -994
     *
     * Type: VM_EXIT
     *
     * Cause(s):
     * - Module power–up self–tests failed.
     *
     * Recommendation(s):
     * - NONE.
     */
    EXIT(-994) {
        override fun getResponseModel() = ResponseModel(
            code = EXIT.code,
            type = "VM_EXIT",
            causes = "Module power–up self–tests failed.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -998
     *
     * Type: VM_RESULT_THREAT
     *
     * Cause(s):
     * - Security threat detected.
     *
     * Recommendation(s):
     * - Make sure that the threat detected are removed.
     */
    RESULT_THREAT(-998) {
        override fun getResponseModel() = ResponseModel(
            code = RESULT_THREAT.code,
            type = "VM_RESULT_THREAT",
            causes = "Security threat detected.",
            recommendations = "Make sure that the threat detected are removed."
        )
    },

    /**
     * Code: -1201
     *
     * Type: VM_INIT_JAVA_WHITE_LIST_FAILED
     *
     * Cause(s):
     * - Failed to encrypt/decrypt trusted java package whitelist.
     *
     * Recommendation(s):
     * - NONE.
     */
    INIT_JAVA_WHITE_LIST_FAILED(-1201) {
        override fun getResponseModel(): ResponseModel = ResponseModel(
            code = INIT_JAVA_WHITE_LIST_FAILED.code,
            type = "VM_INIT_JAVA_WHITE_LIST_FAILED",
            causes = "Failed to encrypt/decrypt trusted java package whitelist.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            OUT_OF_MEMORY.code to OUT_OF_MEMORY.getResponseModel(),
            BAD_FUNC_ARG.code to BAD_FUNC_ARG.getResponseModel(),
            RESULT_ERROR.code to RESULT_ERROR.getResponseModel(),
            EXIT.code to EXIT.getResponseModel(),
            RESULT_THREAT.code to RESULT_THREAT.getResponseModel(),
            INIT_JAVA_WHITE_LIST_FAILED.code to INIT_JAVA_WHITE_LIST_FAILED.getResponseModel()
        )
    }
}
