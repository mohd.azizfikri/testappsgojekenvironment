package identity.protection.responsecode.firmware

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of Data Store response code.
 */
enum class DataStoreResponse(val code: Long) : Response {
    /**
     * Code: -344
     *
     * Type: DATASTORE_LOAD_E
     *
     * Cause(s):
     * - Error loading data store.
     *
     * Recommendation(s):
     * - NONE.
     */
    LOAD(-344) {
        override fun getResponseModel() = ResponseModel(
            code = LOAD.code,
            type = "DATASTORE_LOAD_E",
            causes = "Error loading data store.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -345
     *
     * Type: DATASTORE_SAVE_E
     *
     * Cause(s):
     * - Error saving data store.
     *
     * Recommendation(s):
     * - NONE.
     */
    SAVE(-345) {
        override fun getResponseModel() = ResponseModel(
            code = SAVE.code,
            type = "DATASTORE_SAVE_E",
            causes = "Error saving data store.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            LOAD.code to LOAD.getResponseModel(),
            SAVE.code to SAVE.getResponseModel()
        )
    }
}
