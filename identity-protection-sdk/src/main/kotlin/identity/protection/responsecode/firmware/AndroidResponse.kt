package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Android response code.
 */
enum class AndroidResponse(val code: Long) : Response {
    /**
     * Code: -1025
     *
     * Type: ANDROID_NATIVE_TAMPERED_LOCK
     *
     * Cause(s):
     * - V–OS is locked because the Android native library has been tampered with.
     *
     * Recommendation(s):
     * - Make sure to use the correct voscodedesign.vky asset.
     * - Make sure that Android native library is not tampered with.
     */
    NATIVE_TAMPERED_LOCK(-1025) {
        override fun getResponseModel() = ResponseModel(
            code = NATIVE_TAMPERED_LOCK.code,
            type = "ANDROID_NATIVE_TAMPERED_LOCK",
            causes = "V–OS is locked because the Android native library has been tampered with.",
            recommendations = "- Make sure to use the correct voscodedesign.vky asset.\n" +
                    "- Make sure that Android native library is not tampered with."
        )
    },

    /**
     * Code: -1026
     *
     * Type: ANDROID_JAVA_TRACE_VALIDATION_FAILED
     *
     * Cause(s):
     * - V–OS is locked because it failed at Android call trace validation.
     *
     * Recommendation(s):
     * - Make sure the application has not been tampered with.
     * - Make sure to use only known libraries.
     * - Make sure to turn off 'Instant Run' on Android Studio.
     */
    JAVA_TRACE_VALIDATION_FAILED(-1026) {
        override fun getResponseModel() = ResponseModel(
            code = JAVA_TRACE_VALIDATION_FAILED.code,
            type = "ANDROID_JAVA_TRACE_VALIDATION_FAILED",
            causes = "V–OS is locked because it failed at Android call trace validation.",
            recommendations = "- Make sure the application has not been tampered with.\n" +
                    "- Make sure to use only known libraries.\n" +
                    "- Make sure to turn off 'Instant Run' on Android Studio."
        )
    },

    /**
     * Code: -1031
     *
     * Type: ANDROID_INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY
     *
     * Cause(s):
     * - Failed to allocate memory when processing trusted java package list during initialization.
     *
     * Recommendation(s):
     * - NONE.
     */
    INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY(-1031) {
        override fun getResponseModel() = ResponseModel(
            code = INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.code,
            type = "ANDROID_INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY",
            causes = "Failed to allocate memory when processing trusted " +
                    "java package list during initialization.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1032
     *
     * Type: ANDROID_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY
     *
     * Cause(s):
     * - Failed to allocate memory when obtaining Android java call trace.
     *
     * Recommendation(s):
     * - NONE.
     */
    JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY(-1032) {
        override fun getResponseModel() = ResponseModel(
            code = JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.code,
            type = "ANDROID_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY",
            causes = "Failed to allocate memory when obtaining Android java call trace.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1037
     *
     * Type: ANDROID_FRIDA_JAVA_HOOKING
     *
     * Cause(s):
     * - V–OS is locked because Java Frida hooking is detected.
     *
     * Recommendation(s):
     * - NONE.
     */
    FRIDA_JAVA_HOOKING(-1037) {
        override fun getResponseModel() = ResponseModel(
            code = FRIDA_JAVA_HOOKING.code,
            type = "ANDROID_FRIDA_JAVA_HOOKING",
            causes = "V–OS is locked because Java Frida hooking is detected.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -1039
     *
     * Type: ANDROID_EMULATOR_DETECTED
     *
     * Cause(s):
     * - V–OS is locked because an emulator is detected.
     *
     * Recommendation(s):
     * - Do not use V–OS with an emulator.
     */
    EMULATOR_DETECTED(-1039) {
        override fun getResponseModel() = ResponseModel(
            code = EMULATOR_DETECTED.code,
            type = "ANDROID_EMULATOR_DETECTED",
            causes = "V–OS is locked because an emulator is detected.",
            recommendations = "Do not use V–OS with an emulator."
        )
    },

    /**
     * Code: -1050
     *
     * Type: ANDROID_APP_SIGNER_CERT_HOOKED
     *
     * Cause(s):
     * - Android app signer has been tampered with.
     *
     * Recommendation(s):
     * - Make sure that the application has not been tampered with.
     */
    APP_SIGNER_CERT_HOOKED(-1050) {
        override fun getResponseModel() = ResponseModel(
            code = APP_SIGNER_CERT_HOOKED.code,
            type = "ANDROID_APP_SIGNER_CERT_HOOKED",
            causes = "Android app signer has been tampered with.",
            recommendations = "Make sure that the application has not been tampered with."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            NATIVE_TAMPERED_LOCK.code to NATIVE_TAMPERED_LOCK.getResponseModel(),
            JAVA_TRACE_VALIDATION_FAILED.code to JAVA_TRACE_VALIDATION_FAILED.getResponseModel(),
            INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.code to INIT_JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.getResponseModel(),
            JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.code to JAVA_CALL_TRACE_WITH_INSUFFICIENT_MEMORY.getResponseModel(),
            FRIDA_JAVA_HOOKING.code to FRIDA_JAVA_HOOKING.getResponseModel(),
            EMULATOR_DETECTED.code to EMULATOR_DETECTED.getResponseModel(),
            APP_SIGNER_CERT_HOOKED.code to APP_SIGNER_CERT_HOOKED.getResponseModel()
        )
    }
}