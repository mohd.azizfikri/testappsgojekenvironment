package identity.protection.responsecode.firmware

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * A class that provide a firmware response code
 */
class Firmware {

    /**
     * Collection of firmware response code
     *
     * @see [VOS_FIRMWARE_RETURN_CODE_KEY]
     */
    companion object {
        val responses: Map<Long, ResponseModel> = mapOf<Long, ResponseModel>()
            .plus(KeystoreResponse.responses)
            .plus(VMResponse.responses)
            .plus(TrustedStorageResponse.responses)
            .plus(DataStoreResponse.responses)
            .plus(CertificateResponse.responses)
            .plus(LicenseResponse.responses)
            .plus(AndroidResponse.responses)
            .plus(TrustedTimeResponse.responses)
            .plus(ManifestResponse.responses)
            .plus(CommonResponse.responses)
    }
}
