package identity.protection.responsecode.protection

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of V-OS App Protection response code
 */
enum class AppProtectionResponse(val code: Long) : Response {

    /**
     * Code: 20000
     *
     * Type: ERROR_FIRMWARE_MISSING
     *
     * Cause(s):
     * - The firmware file cannot be found.
     *
     * Recommendation(s):
     * - Make sure that the firmware file is in the /assets/ folder of your app project.
     * - Make sure that the firmware filename is firmware without file extension.
     */
    ERROR_FIRMWARE_MISSING(20000) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_FIRMWARE_MISSING.code,
            type = "ERROR_FIRMWARE_MISSING",
            causes = "The firmware file cannot be found.",
            recommendations = "- Make sure that the firmware file is in the " +
                    "/assets/ folder of your app project.\n" +
                    "- Make sure that the firmware filename is firmware without file extension."
        )
    },

    /**
     * Code: 20001
     *
     * Type: ERROR_FIRMWARE_CORRUPTED
     *
     * Cause(s):
     * - The firmware file is corrupted.
     *
     * Recommendation(s):
     * - Replace the firmware file in the /assets/ folder
     * of your app project with a new copy from V-Key.
     */
    ERROR_FIRMWARE_CORRUPTED(20001) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_FIRMWARE_CORRUPTED.code,
            type = "ERROR_FIRMWARE_CORRUPTED",
            causes = "The firmware file is corrupted.",
            recommendations = "Replace the firmware file in the /assets/ folder " +
                    "of your app project with a new copy from V-Key."
        )
    },

    /**
     * Code: 20010
     *
     * Type: ERROR_PROFILE_MISSING
     *
     * Cause(s):
     * - The profile file cannot be found.
     *
     * Recommendation(s):
     * - Make sure that the profile file is in the /assets/ folder of your app project.
     * - Make sure that the profile filename is `profile` without file extension.
     */
    ERROR_PROFILE_MISSING(20010) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_PROFILE_MISSING.code,
            type = "ERROR_PROFILE_MISSING",
            causes = "The profile file cannot be found.",
            recommendations = "- Make sure that the profile file is in the /assets/ folder" +
                    "of your app project.\n" +
                    "- Make sure that the profile filename is `profile` without file extension."
        )
    },

    /**
     * Code: 20011
     *
     * Type: ERROR_PROFILE_CORRUPTED
     *
     * Cause(s):
     * - The profile file is corrupted.
     *
     * Recommendation(s):
     * - Replace the profile file in the /assets/ folder
     * of your app project with a new copy from V-Key or download a new copy from
     * V-OS App Protection Server.
     */
    ERROR_PROFILE_CORRUPTED(20011) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_PROFILE_CORRUPTED.code,
            type = "ERROR_PROFILE_CORRUPTED",
            causes = "The profile file is corrupted.",
            recommendations = "Replace the profile file in the /assets/ folder " +
                    "of your app project with a new copy from V-Key or download a new copy from " +
                    "V-OS App Protection Server."
        )
    },

    /**
     * Code: 20012
     *
     * Type: ERROR_PROFILE_DECRYPTION
     *
     * Cause(s):
     * - The profile file cannot be decrypted.
     *
     * Recommendation(s):
     * Make sure that the profile is encrypted with the correct customer key.
     * To ensure, do the following:
     * - Make sure that the vkeylicensepack used is from the matching license
     * used to create the customer on V-OS App Protection Server
     * - Make sure that the profile is downloaded from the correct customer account
     * on V–OS App Protection Server.
     *
     */
    ERROR_PROFILE_DECRYPTION(20012) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_PROFILE_DECRYPTION.code,
            type = "ERROR_PROFILE_DECRYPTION",
            causes = "The profile file cannot be decrypted.",
            recommendations = "Make sure that the profile is encrypted with the correct " +
                    "customer key. To ensure, do the following:\n" +
                    "- Make sure that the vkeylicensepack used is from the matching license " +
                    "used to create the customer on V-OS App Protection Server.\n" +
                    "- Make sure that the profile is downloaded from the correct customer account " +
                    "on V–OS App Protection Server."
        )
    },

    /**
     * Code: 20020
     *
     * Type: ERROR_SIGNATURE_MISSING
     *
     * Cause(s):
     * - The signature file cannot be found.
     *
     * Recommendation(s):
     * - Make sure that the signature file is in the /assets/ folder of your app project.
     * - Make sure that the signature filename is signature without file extension
     */
    ERROR_SIGNATURE_MISSING(20020) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_SIGNATURE_MISSING.code,
            type = "ERROR_SIGNATURE_MISSING",
            causes = "The signature file cannot be found.",
            recommendations = "- Make sure that the signature file is in the /assets/ folder " +
                    "of your app project.\n" +
                    "- Make sure that the signature filename is signature without file extension."
        )
    },

    /**
     * Code: 20021
     *
     * Type: ERROR_SIGNATURE_CORRUPTED
     *
     * Cause(s):
     * - The signature file is corrupted.
     *
     * Recommendation(s):
     * - Replace the signature file in the /assets/ folder of your app project
     * with a new copy from V-Key.
     */
    ERROR_SIGNATURE_CORRUPTED(20021) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_SIGNATURE_CORRUPTED.code,
            type = "ERROR_SIGNATURE_CORRUPTED",
            causes = "The signature file is corrupted.",
            recommendations = "Replace the signature file in the /assets/ folder of " +
                    "your app project with a new copy from V-Key."
        )
    },

    /**
     * Code: 20022
     *
     * Type: ERROR_SIGNATURE_DECRYPTION
     *
     * Cause(s):
     * - The signature file cannot be decrypted.
     *
     * Recommendation(s):
     * - Make sure that the signature is encrypted with the correct key.
     *
     * Note: Key used to encrypt signature is encrypted by V–OS and stored in the header.
     */
    ERROR_SIGNATURE_DECRYPTION(20022) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_SIGNATURE_DECRYPTION.code,
            type = "ERROR_SIGNATURE_DECRYPTION",
            causes = "The signature file cannot be decrypted.",
            recommendations = "Make sure that the signature is encrypted with the correct key.\n" +
                    "Note: Key used to encrypt signature is encrypted by " +
                    "V–OS and stored in the header."
        )
    },

    /**
     * Code: 20023
     *
     * Type: ERROR_SIGNATURE_INVALID
     *
     * Cause(s):
     * - The signature file is not valid.
     *
     * Recommendation(s):
     * - Signature is platform dependent.
     * Make sure that the signature file is for the correct platform.
     */
    ERROR_SIGNATURE_INVALID(20023) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_SIGNATURE_INVALID.code,
            type = "ERROR_SIGNATURE_INVALID",
            causes = "The signature file is not valid.",
            recommendations = "Signature is platform dependent. " +
                    "Make sure that the signature file is for the correct platform."
        )
    },

    /**
     * Code: 20030
     *
     * Type: ERROR_LICENSE_MISSING
     *
     * Cause(s):
     * - The vkeylicensepack file cannot be found.
     *
     * Recommendation(s):
     * - Make sure that the vkeylicensepack file is in the /assets/ folder of your app project.
     * - Make sure that the license pack filename is vkeylicensepack without file extension
     */
    ERROR_LICENSE_MISSING(20030) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_LICENSE_MISSING.code,
            type = "ERROR_LICENSE_MISSING",
            causes = "The vkeylicensepack file cannot be found.",
            recommendations = "- Make sure that the vkeylicensepack file is " +
                    "in the /assets/ folder of your app project.\n" +
                    "- Make sure that the license pack filename is " +
                    "vkeylicensepack without file extension"
        )
    },

    /**
     * Code: 20031
     *
     * Type: ERROR_LICENSE_CORRUPTED
     *
     * Cause(s):
     * - The vkeylicensepack file is corrupted
     *
     * Recommendation(s):
     * - Replace the vkeylicensepack file in the /assets/ folder
     * of your app project with a new copy from V-Key.
     */
    ERROR_LICENSE_CORRUPTED(20031) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_LICENSE_CORRUPTED.code,
            type = "ERROR_LICENSE_CORRUPTED",
            causes = "The vkeylicensepack file is corrupted",
            recommendations = "Replace the vkeylicensepack file in the /assets/ folder " +
                    "of your app project with a new copy from V-Key."
        )
    },

    /**
     * Code: 20032
     *
     * Type: ERROR_LICENSE_SIGNER_CERT_MISMATCH
     *
     * Cause(s):
     * -     The signer certificate used is incorrect.
     *
     * Recommendation(s):
     * - If you are using V–OS debug version, check with V–Key to
     * confirm if the license is generated with a special fixed certificate.
     * - If you are not using V–OS debug version, make sure that the app is
     * generated and signed by the correct signer certificate.
     *
     * Note: Request for the license details from V–Key, if necessary.
     */
    ERROR_LICENSE_SIGNER_CERT_MISMATCH(20032) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_LICENSE_SIGNER_CERT_MISMATCH.code,
            type = "ERROR_LICENSE_SIGNER_CERT_MISMATCH",
            causes = "The signer certificate used is incorrect.",
            recommendations = "- If you are using V–OS debug version, check with V–Key to " +
                    "confirm if the license is generated with a special fixed certificate.\n" +
                    "- If you are not using V–OS debug version, make sure that the app is " +
                    "generated and signed by the correct signer certificate.\n" +
                    "Note: Request for the license details from V–Key, if necessary."
        )
    },

    /**
     * Code: 20033
     *
     * Type: ERROR_LICENSE_PACKAGE_NAME_MISMATCH
     *
     * Cause(s):
     * - The license package name is incorrect.
     *
     * Recommendation(s):
     * - Make sure that the package name of your app matches the package
     * name given to V–Key during the vkeylicensepack generation.
     */
    ERROR_LICENSE_PACKAGE_NAME_MISMATCH(20033) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_LICENSE_PACKAGE_NAME_MISMATCH.code,
            type = "ERROR_LICENSE_PACKAGE_NAME_MISMATCH",
            causes = "The license package name is incorrect.",
            recommendations = "Make sure that the package name of your app matches the package " +
                    "name given to V–Key during the vkeylicensepack generation."
        )
    },

    /**
     * Code: 20034
     *
     * Type: ERROR_LICENSE_VERIFICATION
     *
     * Cause(s):
     * - License verification failed unexpectedly.
     *
     * Recommendation(s):
     * - Contact V–Key for assistance.
     */
    ERROR_LICENSE_VERIFICATION(20034) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_LICENSE_VERIFICATION.code,
            type = "ERROR_LICENSE_VERIFICATION",
            causes = "License verification failed unexpectedly.",
            recommendations = "Contact V–Key for assistance."
        )
    },

    /**
     * Code: 20040
     *
     * Type: ERROR_NATIVE_LIBRARY_MISSING
     *
     * Cause(s):
     * - One or more native libraries are missing.
     *
     * Recommendation(s):
     * Make sure that the following .so files are in the correct
     * folders of your app project. The .so files are:
     * - libchecks.so
     * - libSecureFileIO.so
     * - libvosWrapperEx.so
     *
     * Note: If any of the .so file is missing, contact V-Key.
     */
    ERROR_NATIVE_LIBRARY_MISSING(20040) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_NATIVE_LIBRARY_MISSING.code,
            type = "ERROR_NATIVE_LIBRARY_MISSING",
            causes = "One or more native libraries are missing.",
            recommendations = "Make sure that the following .so files are in the correct " +
                    "folders of your app project. The .so files are:\n" +
                    "- libchecks.so\n" +
                    "- libSecureFileIO.so\n" +
                    "- libvosWrapperEx.so\n" +
                    "Note: If any of the .so file is missing, contact V-Key."
        )
    },

    /**
     * Code: 20041
     *
     * Type: ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK
     *
     * Cause(s):
     * - Unsatisfied link error in .so files
     *
     * Recommendation(s):
     * - This error happens only in GenyMotion emulator when
     * trying to load V–OS. Check and use the corresponding
     * JARs for the different architectures.
     */
    ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK(20041) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK.code,
            type = "ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK",
            causes = "Unsatisfied link error in .so files",
            recommendations = "This error happens only in GenyMotion emulator when " +
                    "trying to load V–OS. Check and use the corresponding " +
                    "JARs for the different architectures."
        )
    },

    /**
     * Code: 20050
     *
     * Type: ERROR_EMULATOR_DETECTED
     *
     * Cause(s):
     * - Emulator detected.
     *
     * Recommendation(s):
     * - Set VGuardFactory.debug to true to enable debug mode
     * during development and debugging stage.
     */
    ERROR_EMULATOR_DETECTED(20050) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_EMULATOR_DETECTED.code,
            type = "ERROR_EMULATOR_DETECTED",
            causes = "Emulator detected.",
            recommendations = "Set VGuardFactory.debug to true to enable debug mode " +
                    "during development and debugging stage."
        )
    },

    /**
     * Code: 20051
     *
     * Type: ERROR_REQUIRED_PERMISSIONS_NOT_FOUND
     *
     * Cause(s):
     * - The required permissions are not granted by the user or
     * not included in the AndroidManifest.xml file.
     *
     * Recommendation(s):
     * - Make sure that the activity
     * com.vkey.android.support.permission.VGuardPermissionActivity is
     * included in the AndroidManifest.xml file.
     * - Make sure that the app user grants the necessary permissions.
     */
    ERROR_REQUIRED_PERMISSIONS_NOT_FOUND(20051) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_REQUIRED_PERMISSIONS_NOT_FOUND.code,
            type = "ERROR_REQUIRED_PERMISSIONS_NOT_FOUND",
            causes = "The required permissions are not granted by the user or " +
                    "not included in the AndroidManifest.xml file.",
            recommendations = "- Make sure that the activity" +
                    "com.vkey.android.support.permission.VGuardPermissionActivity is " +
                    "included in the AndroidManifest.xml file.\n" +
                    "- Make sure that the app user grants the necessary permissions."
        )
    },

    /**
     * Code: 20080
     *
     * Type: ERROR_OUT_OF_MEMORY
     *
     * Cause(s):
     * - Out of memory error is detected in V–OS App Protection.
     *
     * Recommendation(s):
     * - NONE.
     */
    ERROR_OUT_OF_MEMORY(20080) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_OUT_OF_MEMORY.code,
            type = "ERROR_OUT_OF_MEMORY",
            causes = "Out of memory error is detected in V–OS App Protection.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 20035
     *
     * Type: ERROR_VOS_TRUST_STORAGE_FAILED
     *
     * Cause(s):
     * - V–OS failed with trusted storage error.
     *
     * Recommendation(s):
     * - Clear app data, E.g. call VGuard.clearVOSTrustedStorage().
     * - Uninstall and reinstall the app.
     */
    ERROR_VOS_TRUST_STORAGE_FAILED(20035) {
        override fun getResponseModel() = ResponseModel(
            code = ERROR_VOS_TRUST_STORAGE_FAILED.code,
            type = "ERROR_VOS_TRUST_STORAGE_FAILED",
            causes = "V–OS failed with trusted storage error.",
            recommendations = "- Clear app data, E.g. call VGuard.clearVOSTrustedStorage().\n" +
                    "- Uninstall and reinstall the app."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            ERROR_FIRMWARE_MISSING.code to ERROR_FIRMWARE_MISSING.getResponseModel(),
            ERROR_FIRMWARE_CORRUPTED.code to ERROR_FIRMWARE_CORRUPTED.getResponseModel(),
            ERROR_PROFILE_MISSING.code to ERROR_PROFILE_MISSING.getResponseModel(),
            ERROR_PROFILE_CORRUPTED.code to ERROR_PROFILE_CORRUPTED.getResponseModel(),
            ERROR_PROFILE_DECRYPTION.code to ERROR_PROFILE_DECRYPTION.getResponseModel(),
            ERROR_SIGNATURE_MISSING.code to ERROR_SIGNATURE_MISSING.getResponseModel(),
            ERROR_SIGNATURE_CORRUPTED.code to ERROR_SIGNATURE_CORRUPTED.getResponseModel(),
            ERROR_SIGNATURE_DECRYPTION.code to ERROR_SIGNATURE_DECRYPTION.getResponseModel(),
            ERROR_SIGNATURE_INVALID.code to ERROR_SIGNATURE_INVALID.getResponseModel(),
            ERROR_LICENSE_MISSING.code to ERROR_LICENSE_MISSING.getResponseModel(),
            ERROR_LICENSE_CORRUPTED.code to ERROR_LICENSE_CORRUPTED.getResponseModel(),
            ERROR_LICENSE_SIGNER_CERT_MISMATCH.code to ERROR_LICENSE_SIGNER_CERT_MISMATCH.getResponseModel(),
            ERROR_LICENSE_PACKAGE_NAME_MISMATCH.code to ERROR_LICENSE_PACKAGE_NAME_MISMATCH.getResponseModel(),
            ERROR_LICENSE_VERIFICATION.code to ERROR_LICENSE_VERIFICATION.getResponseModel(),
            ERROR_NATIVE_LIBRARY_MISSING.code to ERROR_NATIVE_LIBRARY_MISSING.getResponseModel(),
            ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK.code to ERROR_NATIVE_LIBRARY_UNSATISFIED_LINK.getResponseModel(),
            ERROR_EMULATOR_DETECTED.code to ERROR_EMULATOR_DETECTED.getResponseModel(),
            ERROR_REQUIRED_PERMISSIONS_NOT_FOUND.code to ERROR_REQUIRED_PERMISSIONS_NOT_FOUND.getResponseModel(),
            ERROR_OUT_OF_MEMORY.code to ERROR_OUT_OF_MEMORY.getResponseModel(),
            ERROR_VOS_TRUST_STORAGE_FAILED.code to ERROR_VOS_TRUST_STORAGE_FAILED.getResponseModel()
        )
    }
}
