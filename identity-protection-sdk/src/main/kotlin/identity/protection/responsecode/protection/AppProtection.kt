package identity.protection.responsecode.protection

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * A class that provide a V-OS App Protection response code
 */
class AppProtection {

    /**
     * Collection of V-OS App Protection response code
     */
    companion object {
        val responses: Map<Long, ResponseModel> = AppProtectionResponse.responses
    }
}
