package identity.protection.responsecode.processor

import identity.protection.responsecode.Response
import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * Collection of V-OS Processor response code.
 */
enum class VosProcessorResponse(val code: Long) : Response {
    /**
     * Code: -10001
     *
     * Type: VM_ERR_VA_FILE_CANNOT_ACCESS
     *
     * Cause(s):
     * - The host app cannot access the file.
     *
     * Recommendation(s):
     * - Make sure that the file is present.
     * - Make sure that the filename passed in the APl call is correct.
     * - Make sure that the file path passed in the APl call is correct.
     * - If on Android device, make sure that you have enabled file access permission.
     * - Try to restart the app.
     * - Try to reinstall the app.
     */
    VM_ERR_VA_FILE_CANNOT_ACCESS(-10001) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_FILE_CANNOT_ACCESS.code,
            type = "VM_ERR_VA_FILE_CANNOT_ACCESS",
            causes = "The host app cannot access the file.",
            recommendations = "- Make sure that the file is present.\n" +
                    "- Make sure that the filename passed in the APl call is correct.\n" +
                    "- Make sure that the file path passed in the APl call is correct.\n" +
                    "- If on Android device, make sure that you have enabled " +
                    "file access permission.\n" +
                    "- Try to restart the app.\n" +
                    "- Try to reinstall the app."
        )
    },

    /**
     * Code: -10002
     *
     * Type: VM_ERR_VA_FILE_FAIL_UNCOMPRESS
     *
     * Cause(s):
     * - The file cannot be uncompressed.
     * Maybe it has not been compressed, or the data is corrupted.
     *
     * Recommendation(s):
     * - Make sure that the file is compressed.
     * - Make sure that the file is not corrupted.
     */
    VM_ERR_VA_FILE_FAIL_UNCOMPRESS(-10002) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_FILE_FAIL_UNCOMPRESS.code,
            type = "VM_ERR_VA_FILE_FAIL_UNCOMPRESS",
            causes = "The file cannot be uncompressed. " +
                    "Maybe it has not been compressed, or the data is corrupted.",
            recommendations = "- Make sure that the file is compressed.\n" +
                    "- Make sure that the file is not corrupted."
        )
    },

    /**
     * Code: -10003
     *
     * Type: VM_ERR_VA_ENTRY_ALREADY_EXlST
     *
     * Cause(s):
     * - The TA you are trying to add already existed. A TA can only be added once.
     *
     * Recommendation(s):
     * - Make sure that the TA you are adding has not been added previously.
     */
    VM_ERR_VA_ENTRY_ALREADY_EXIST(-1003) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_ENTRY_ALREADY_EXIST.code,
            type = "VM_ERR_VA_ENTRY_ALREADY_EXIST",
            causes = "The TA you are trying to add already existed. A TA can only be added once.",
            recommendations = "Make sure that the TA you are adding has not been added previously."
        )
    },

    /**
     * Code: -10005
     *
     * Type: VM_ERR_VA_ENTRY_NOT_FOUND
     *
     * Cause(s):
     * - Unable to find the TA.
     *
     * Recommendation(s):
     * - Make sure that you have already added the TA.
     */
    VM_ERR_VA_ENTRY_NOT_FOUND(-10005) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_ENTRY_NOT_FOUND.code,
            type = "VM_ERR_VA_ENTRY_NOT_FOUND",
            causes = "Unable to find the TA.",
            recommendations = "Make sure that you have already added the TA."
        )
    },

    /**
     * Code: -10006
     *
     * Type: VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND
     *
     * Cause(s):
     * - The TA that you are trying to switch to cannot be found.
     *
     * Recommendation(s):
     * - Make sure that the TA has already been added.
     */
    VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND(-10006) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND.code,
            type = "VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND",
            causes = "The TA that you are trying to switch to cannot be found.",
            recommendations = "Make sure that the TA has already been added."
        )
    },

    /**
     * Code: -10007
     *
     * Type: VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND
     *
     * Cause(s):
     * - Fail to find contiguous memory space in the VM for the TA.
     *
     * Recommendation(s):
     * - Make sure that the size of the TA is within the available memory space of the VM.
     */
    VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND(-10007) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND.code,
            type = "VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND",
            causes = "Fail to find contiguous memory space in the VM for the TA.",
            recommendations = "Make sure that the size of the TA is within " +
                    "the available memory space of the VM."
        )
    },

    /**
     * Code: -10008
     *
     * Type: VM_ERR_VA_FILE_INCOMPATIBLE
     *
     * Cause(s):
     * - The firmware used is incompatible with the processor.
     *
     * Recommendation(s):
     * - Make sure that the firmware used is compatible with the processor.
     */
    VM_ERR_VA_FILE_INCOMPATIBLE(-10008) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_FILE_INCOMPATIBLE.code,
            type = "VM_ERR_VA_FILE_INCOMPATIBLE",
            causes = "The firmware used is incompatible with the processor.",
            recommendations = "Make sure that the firmware used is compatible with the processor."
        )
    },

    /**
     * Code: -10009
     *
     * Type: VM_ERR_VA_FILE_INVALID_HEADER
     *
     * Cause(s):
     * - The header data (containing version number) is invalid, corrupted, or missing.
     *
     * Recommendation(s):
     * - Make sure that the correct firmware is used.
     */
    VM_ERR_VA_FILE_INVALID_HEADER(-10009) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_FILE_INVALID_HEADER.code,
            type = "VM_ERR_VA_FILE_INVALID_HEADER",
            causes = "The header data (containing version number) is invalid, " +
                    "corrupted, or missing.",
            recommendations = "Make sure that the correct firmware is used."
        )
    },

    /**
     * Code: -10010
     *
     * Type: VM_ERR_VA_FILE_TOO_BIG
     *
     * Cause(s):
     * - The size of the TA is too big for the VM to handle.
     *
     * Recommendation(s):
     * - Reduce the size of the TA.
     */
    VM_ERR_VA_FILE_TOO_BIG(-10010) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_VA_FILE_TOO_BIG.code,
            type = "VM_ERR_VA_FILE_TOO_BIG",
            causes = "The size of the TA is too big for the VM to handle.",
            recommendations = "Reduce the size of the TA."
        )
    },

    /**
     * Code: -10104
     *
     * Type: VM_ERR_ISO_ENTRY_NOT_FOUND
     *
     * Cause(s):
     * - The kernel/TA binary entry is not found in the list.
     *
     * Recommendation(s):
     * - Make sure that the kernel/TA binary has already been added.
     */
    VM_ERR_ISO_ENTRY_NOT_FOUND(-10104) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_ISO_ENTRY_NOT_FOUND.code,
            type = "VM_ERR_ISO_ENTRY_NOT_FOUND",
            causes = "The kernel/TA binary entry is not found in the list.",
            recommendations = "Make sure that the kernel/TA binary has already been added."
        )
    },

    /**
     * Code: -10105
     *
     * Type: VM_ERR_ISO_ENTRY_ALREADY_EXIST
     *
     * Cause(s):
     * - The kernel/TA binary entry of the specified tag already existed.
     *
     * Recommendation(s):
     * - Make sure that the kernel/TA binary tag has not been used.
     */
    VM_ERR_ISO_ENTRY_ALREADY_EXIST(-10105) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_ISO_ENTRY_ALREADY_EXIST.code,
            type = "VM_ERR_ISO_ENTRY_ALREADY_EXIST",
            causes = "The kernel/TA binary entry of the specified tag already existed.",
            recommendations = "Make sure that the kernel/TA binary tag has not been used."
        )
    },

    /**
     * Code: -10301
     *
     * Type: VM_ERR_ISO_ENTRY_CREATE_FAIL
     *
     * Cause(s):
     * - Failed to create kernel/TA binary entry.
     *
     * Recommendation(s):
     * - NONE.
     */
    VM_ERR_ISO_ENTRY_CREATE_FAIL(-10301) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_ISO_ENTRY_CREATE_FAIL.code,
            type = "VM_ERR_ISO_ENTRY_CREATE_FAIL",
            causes = "Failed to create kernel/TA binary entry.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: -10302
     *
     * Type: VM_ERR_TRT_RESPONSE_FAILED
     *
     * Cause(s):
     * - Failed to get success response from the trusted time server.
     *
     * Recommendation(s):
     * - Make sure that the server can decrypt the TTK sent from V–OS.
     */
    VM_ERR_TRT_RESPONSE_FAILED(-10302) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_RESPONSE_FAILED.code,
            type = "VM_ERR_TRT_RESPONSE_FAILED",
            causes = "Failed to get success response from the trusted time server.",
            recommendations = "Make sure that the server can decrypt the TTK sent from V–OS."
        )
    },

    /**
     * Code: -10303
     *
     * Type: VM_ERR_TRT_AUTHENTICATION_FAILED
     *
     * Cause(s):
     * - Failed to authenticate with a valid user lD and/or password with the trusted time server.
     *
     * Recommendation(s):
     * - Make sure that the valid user lD and password are used.
     */
    VM_ERR_TRT_AUTHENTICATION_FAILED(-10303) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_AUTHENTICATION_FAILED.code,
            type = "VM_ERR_TRT_AUTHENTICATION_FAILED",
            causes = "Failed to authenticate with a valid user lD and/or " +
                    "password with the trusted time server.",
            recommendations = "Make sure that the valid user lD and password are used."
        )
    },

    /**
     * Code: -10304
     *
     * Type: VM_ERR_TRT_SERVER_FAILED
     *
     * Cause(s):
     * - The trusted time server response with a 5xx HTTP status code.
     *
     * Recommendation(s):
     * - Make sure that the trusted time server is up and running.
     * - Make sure that the request to the trusted time server is valid.
     */
    VM_ERR_TRT_SERVER_FAILED(-10304) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_SERVER_FAILED.code,
            type = "VM_ERR_TRT_SERVER_FAILED",
            causes = "The trusted time server response with a 5xx HTTP status code.",
            recommendations = "- Make sure that the trusted time server is up and running.\n" +
                    "- Make sure that the request to the trusted time server is valid."
        )
    },

    /**
     * Code: -10305
     *
     * Type: VM_ERR_TRT_RESPONSE_BAD_FORMAT
     *
     * Cause(s):
     * - The format of the response from the trusted time server is not the expected format.
     *
     * Recommendation(s):
     * - Make sure the format of the response from the trusted time server is correct.
     * - Make sure that the request to the trusted time server is valid.
     */
    VM_ERR_TRT_RESPONSE_BAD_FORMAT(-10305) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_RESPONSE_BAD_FORMAT.code,
            type = "VM_ERR_TRT_RESPONSE_BAD_FORMAT",
            causes = "The format of the response from the trusted time server is not the expected format.",
            recommendations = "- Make sure the format of the response from " +
                    "the trusted time server is correct.\n" +
                    "- Make sure that the request to the trusted time server is valid."
        )
    },

    /**
     * Code: -10306
     *
     * Type: VM_ERR_TRT_REQUEST_TIMEOUT
     *
     * Cause(s):
     * - The request to the trusted time server is timed out.
     *
     * Recommendation(s):
     * - Make sure that the trusted time server is up and running.
     * - Make sure that a valid network connection is present.
     * - Make sure that the request to the trusted time server is valid.
     */
    VM_ERR_TRT_REQUEST_TIMEOUT(-10306) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_REQUEST_TIMEOUT.code,
            type = "VM_ERR_TRT_REQUEST_TIMEOUT",
            causes = "The request to the trusted time server is timed out.",
            recommendations = "- Make sure that the trusted time server is up and running.\n" +
                    "- Make sure that a valid network connection is present.\n" +
                    "- Make sure that the request to the trusted time server is valid."
        )
    },

    /**
     * Code: -10307
     *
     * Type: VM_ERR_TRT_UNKNOWN_ERROR
     *
     * Cause(s):
     * - The trusted time server returns an unknown error.
     *
     * Recommendation(s):
     * - Make sure that the trusted time server is up and running.
     * - Make sure that the request to the trusted time server is valid.
     */
    VM_ERR_TRT_UNKNOWN_ERROR(-10307) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_TRT_UNKNOWN_ERROR.code,
            type = "VM_ERR_TRT_UNKNOWN_ERROR",
            causes = "The trusted time server returns an unknown error.",
            recommendations = "- Make sure that the trusted time server is up and running.\n" +
                    "- Make sure that the request to the trusted time server is valid."
        )
    },

    /**
     * Code: -10401
     *
     * Type: VM_ERR_CANNOT_VALIDATE_TIME
     *
     * Cause(s):
     * - Unable to validate the time returned from the trusted time server.
     *
     * Recommendation(s):
     * - Make sure that the device has not been tampered (debugged, etc.) with.
     */
    VM_ERR_CANNOT_VALIDATE_TIME(-10401) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_CANNOT_VALIDATE_TIME.code,
            type = "VM_ERR_CANNOT_VALIDATE_TIME",
            causes = "Unable to validate the time returned from the trusted time server.",
            recommendations = "Make sure that the device has not been tampered " +
                    "(debugged, etc.) with."
        )
    },

    /**
     * Code: -10901
     *
     * Type: VM_ERR_INSUFFICIENT_MEMORY
     *
     * Cause(s):
     * - V–OS Processor has insufficient device memory allocated for its operations.
     *
     * Recommendation(s):
     * - Check the host app memory usage on the device.
     * If the majority of the device memory usage is from V–OS,
     * there may be an issue with the V–OS Processor that you use.
     * Make sure that the correct V–OS Processor is used.
     */
    VM_ERR_INSUFFICIENT_MEMORY(-10901) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_INSUFFICIENT_MEMORY.code,
            type = "VM_ERR_INSUFFICIENT_MEMORY",
            causes = "V–OS Processor has insufficient device memory allocated for its operations.",
            recommendations = "Check the host app memory usage on the device. " +
                    "If the majority of the device memory usage is from V–OS, " +
                    "there may be an issue with the V–OS Processor that you use. " +
                    "Make sure that the correct V–OS Processor is used."
        )
    },

    /**
     * Code: -10902
     *
     * Type: VM_ERR_BAD_ARGUMENTS
     *
     * Cause(s):
     * - Invalid arguments are supplied to the function call.
     *
     * Recommendation(s):
     * - Make sure that the arguments supplied to the function call are valid.
     */
    VM_ERR_BAD_ARGUMENTS(-10902) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_BAD_ARGUMENTS.code,
            type = "VM_ERR_BAD_ARGUMENTS",
            causes = "Invalid arguments are supplied to the function call.",
            recommendations = "Make sure that the arguments supplied " +
                    "to the function call are valid."
        )
    },

    /**
     * Code: -10903
     *
     * Type: VM_ERR_INVALID_HANDLE
     *
     * Cause(s):
     * - The vmHandler is NULL. The V-OS might have not been started successfully
     *
     * Recommendation(s):
     * - Make sure that V–OS is started successfully before any operations are executed.
     */
    VM_ERR_INVALID_HANDLE(-10903) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_INVALID_HANDLE.code,
            type = "VM_ERR_INVALID_HANDLE",
            causes = "The vmHandler is NULL. The V-OS might have not been started successfully",
            recommendations = "Make sure that V–OS is started successfully " +
                    "before any operations are executed."
        )
    },

    /**
     * Code: -10904
     *
     * Type: VM_ERR_UNSUPPORTED_CRYPTO_FUNC
     *
     * Cause(s):
     * - An unsupported cryptographic function is detected.
     *
     * Recommendation(s):
     * - Make sure that only supported cryptographic function is executed.
     */
    VM_ERR_UNSUPPORTED_CRYPTO_FUNC(-10904) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_UNSUPPORTED_CRYPTO_FUNC.code,
            type = "VM_ERR_UNSUPPORTED_CRYPTO_FUNC",
            causes = "An unsupported cryptographic function is detected.",
            recommendations = "Make sure that only supported cryptographic function is executed."
        )
    },

    /**
     * Code: -11100
     *
     * Type: VM_ERR_MTLS_ENTRY_NOT_FOUND
     *
     * Cause(s):
     * - The mTLS certificate and password are not found or not compiled into the kernel.
     *
     * Recommendation(s):
     * - Provide mTLS certificate and password for assets generation.
     * - Do not access mTLS certificate and password.
     */
    VM_ERR_MTLS_ENTRY_NOT_FOUND(-11100) {
        override fun getResponseModel() = ResponseModel(
            code = VM_ERR_MTLS_ENTRY_NOT_FOUND.code,
            type = "VM_ERR_MTLS_ENTRY_NOT_FOUND",
            causes = "The mTLS certificate and password are not found or not compiled into the kernel.",
            recommendations = "- Provide mTLS certificate and password for assets generation.\n" +
                    "- Do not access mTLS certificate and password."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            VM_ERR_VA_FILE_CANNOT_ACCESS.code to VM_ERR_VA_FILE_CANNOT_ACCESS.getResponseModel(),
            VM_ERR_VA_FILE_FAIL_UNCOMPRESS.code to VM_ERR_VA_FILE_FAIL_UNCOMPRESS.getResponseModel(),
            VM_ERR_VA_ENTRY_ALREADY_EXIST.code to VM_ERR_VA_ENTRY_ALREADY_EXIST.getResponseModel(),
            VM_ERR_VA_ENTRY_NOT_FOUND.code to VM_ERR_VA_ENTRY_NOT_FOUND.getResponseModel(),
            VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND.code to VM_ERR_VA_ENTRY_SWITCH_NOT_FOUND.getResponseModel(),
            VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND.code to VM_ERR_VA_ENTRY_BLOCKSPACE_NOT_FOUND.getResponseModel(),
            VM_ERR_VA_FILE_INCOMPATIBLE.code to VM_ERR_VA_FILE_INCOMPATIBLE.getResponseModel(),
            VM_ERR_VA_FILE_INVALID_HEADER.code to VM_ERR_VA_FILE_INVALID_HEADER.getResponseModel(),
            VM_ERR_VA_FILE_TOO_BIG.code to VM_ERR_VA_FILE_TOO_BIG.getResponseModel(),
            VM_ERR_ISO_ENTRY_NOT_FOUND.code to VM_ERR_ISO_ENTRY_NOT_FOUND.getResponseModel(),
            VM_ERR_ISO_ENTRY_ALREADY_EXIST.code to VM_ERR_ISO_ENTRY_ALREADY_EXIST.getResponseModel(),
            VM_ERR_ISO_ENTRY_CREATE_FAIL.code to VM_ERR_ISO_ENTRY_CREATE_FAIL.getResponseModel(),
            VM_ERR_TRT_RESPONSE_FAILED.code to VM_ERR_TRT_RESPONSE_FAILED.getResponseModel(),
            VM_ERR_TRT_AUTHENTICATION_FAILED.code to VM_ERR_TRT_AUTHENTICATION_FAILED.getResponseModel(),
            VM_ERR_TRT_SERVER_FAILED.code to VM_ERR_TRT_SERVER_FAILED.getResponseModel(),
            VM_ERR_TRT_RESPONSE_BAD_FORMAT.code to VM_ERR_TRT_RESPONSE_BAD_FORMAT.getResponseModel(),
            VM_ERR_TRT_REQUEST_TIMEOUT.code to VM_ERR_TRT_REQUEST_TIMEOUT.getResponseModel(),
            VM_ERR_TRT_UNKNOWN_ERROR.code to VM_ERR_TRT_UNKNOWN_ERROR.getResponseModel(),
            VM_ERR_CANNOT_VALIDATE_TIME.code to VM_ERR_CANNOT_VALIDATE_TIME.getResponseModel(),
            VM_ERR_INSUFFICIENT_MEMORY.code to VM_ERR_INSUFFICIENT_MEMORY.getResponseModel(),
            VM_ERR_BAD_ARGUMENTS.code to VM_ERR_BAD_ARGUMENTS.getResponseModel(),
            VM_ERR_INVALID_HANDLE.code to VM_ERR_INVALID_HANDLE.getResponseModel(),
            VM_ERR_UNSUPPORTED_CRYPTO_FUNC.code to VM_ERR_UNSUPPORTED_CRYPTO_FUNC.getResponseModel(),
            VM_ERR_MTLS_ENTRY_NOT_FOUND.code to VM_ERR_MTLS_ENTRY_NOT_FOUND.getResponseModel()
        )
    }
}
