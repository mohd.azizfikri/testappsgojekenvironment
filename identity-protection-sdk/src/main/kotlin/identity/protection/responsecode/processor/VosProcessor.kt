package identity.protection.responsecode.processor

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * A class that provide a V-OS Processor response code
 */
class VosProcessor {

    /**
     * Collection of V-OS Processor response code
     */
    companion object {
        val responses: Map<Long, ResponseModel> = VosProcessorResponse.responses
    }
}
