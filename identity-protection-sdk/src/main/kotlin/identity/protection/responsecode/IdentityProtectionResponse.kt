package identity.protection.responsecode

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.firmware.Firmware
import identity.protection.log.BlackBoxLog
import identity.protection.model.IdentityProtection
import identity.protection.responsecode.processor.VosProcessor
import identity.protection.responsecode.protection.AppProtection
import identity.protection.responsecode.smarttoken.SmartToken
import identity.protection.responsecode.threat.Threat

/**
 * An internal interface class to get the [ResponseModel]
 * and getting more information for the given response code
 */
internal interface Response {
    fun getResponseModel(): ResponseModel
}

/**
 * A class that provide sets of APIs for
 *
 * - [print] the information for the given code
 * - [getType] of response code
 */
object IdentityProtectionResponse {

    private val threatResponses = Threat.responses
    private val firmwareResponses = Firmware.responses
    private val vosProcessorResponses = VosProcessor.responses
    private val vosAppProtectionResponses = AppProtection.responses
    private val smartTokenResponses = SmartToken.responses
    private val responses: Map<Long, ResponseModel> = mapOf<Long, ResponseModel>()
        .plus(threatResponses)
        .plus(firmwareResponses)
        .plus(vosProcessorResponses)
        .plus(vosAppProtectionResponses)
        .plus(smartTokenResponses)

    data class ResponseModel(
        /**
         * A response code.
         */
        val code: Long,

        /**
         * A type of response.
         */
        val type: String,

        /**
         * A reasons why the response code comes up.
         */
        val causes: String,

        /**
         * A suggestion or proposal as to the best course of action.
         */
        val recommendations: String,
    )

    /**
     * A helper function to get more information for the given code
     *
     * @param code [Any]
     */
    internal fun print(code: Any?) {
        if (IdentityProtection.config.isDebuggable.not() || code == null) return

        when (code) {
            is Long -> print(code)
            is Int -> print(code.toLong())
            is String -> runCatching { print(code.toLong()) }
        }
    }

    /**
     * A helper function to get type of response code
     *
     * @param code [String]
     *
     * @see [ResponseModel]
     */
    internal fun getType(code: String): String? {
        if (code.isBlank()) return null

        return runCatching {
            getResponseModel(code.toLong())?.type
        }.getOrNull()
    }

    /**
     * A helper function to get more information for the given code
     *
     * @param code [Long]
     */
    private fun print(code: Long) {
        getResponseModel(code)?.let {
            BlackBoxLog.d { "Code: ${it.code}" }
            BlackBoxLog.d { "Type: ${it.type}" }
            BlackBoxLog.d { "Cause(s): \n${it.causes}" }
            BlackBoxLog.d { "Recommendations(s): \n${it.recommendations}" }
        } ?: BlackBoxLog.d { "Can not find the information for the given code: [$code]" }
    }

    private fun getResponseModel(code: Long): ResponseModel? = responses[code]
}
