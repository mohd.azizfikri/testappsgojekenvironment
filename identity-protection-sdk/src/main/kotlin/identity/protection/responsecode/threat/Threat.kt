package identity.protection.responsecode.threat

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * A class that provide a threat response code
 */
class Threat {

    /**
     * Collection of threat response code
     */
    companion object {
        val responses: Map<Long, ResponseModel> = ThreatResponse.responses
    }
}
