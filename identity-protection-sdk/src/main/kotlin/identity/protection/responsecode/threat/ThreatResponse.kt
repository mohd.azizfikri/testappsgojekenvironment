package identity.protection.responsecode.threat

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Threat response code.
 */
enum class ThreatResponse(val code: Long) : Response {
    /**
     * Code: 1000
     *
     * Type: ROOT_OR_JAIL_BROKEN
     *
     * Cause(s):
     * - Root / Jailbroken.
     *
     * Recommendation(s):
     * - NONE.
     */
    ROOT_OR_JAIL_BROKEN(1000) {
        override fun getResponseModel() = ResponseModel(
            code = ROOT_OR_JAIL_BROKEN.code,
            type = "ROOT_OR_JAIL_BROKEN",
            causes = "Root / Jailbroken.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 2000
     *
     * Type: REMOTE_ADMINISTRATION_TOOLS
     *
     * Cause(s):
     * - Remote Administration Tools.
     *
     * Recommendation(s):
     * - NONE.
     */
    REMOTE_ADMINISTRATION_TOOLS(2000) {
        override fun getResponseModel() = ResponseModel(
            code = REMOTE_ADMINISTRATION_TOOLS.code,
            type = "REMOTE_ADMINISTRATION_TOOLS",
            causes = "Remote Administration Tools.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 3000
     *
     * Type: APPLICATION_TAMPERING
     *
     * Cause(s):
     * - Application Tampering.
     *
     * Recommendation(s):
     * - NONE.
     */
    APPLICATION_TAMPERING(3000) {
        override fun getResponseModel() = ResponseModel(
            code = APPLICATION_TAMPERING.code,
            type = "APPLICATION_TAMPERING",
            causes = "Application Tampering.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 4000
     *
     * Type: RUNTIME_TAMPERING
     *
     * Cause(s):
     * - Runtime Tampering.
     *
     * Recommendation(s):
     * - NONE.
     */
    RUNTIME_TAMPERING(4000) {
        override fun getResponseModel() = ResponseModel(
            code = RUNTIME_TAMPERING.code,
            type = "RUNTIME_TAMPERING",
            causes = "Runtime Tampering.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 5000
     *
     * Type: LIBRARIES_TAMPERING
     *
     * Cause(s):
     * - Libraries Tampering.
     *
     * Recommendation(s):
     * - NONE.
     */
    LIBRARIES_TAMPERING(5000) {
        override fun getResponseModel() = ResponseModel(
            code = LIBRARIES_TAMPERING.code,
            type = "LIBRARIES_TAMPERING",
            causes = "Libraries Tampering.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 6000
     *
     * Type: MALWARE
     *
     * Cause(s):
     * - Malware.
     *
     * Recommendation(s):
     * - NONE.
     */
    MALWARE(6000) {
        override fun getResponseModel() = ResponseModel(
            code = MALWARE.code,
            type = "MALWARE",
            causes = "Malware.",
            recommendations = "NONE."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            ROOT_OR_JAIL_BROKEN.code to ROOT_OR_JAIL_BROKEN.getResponseModel(),
            REMOTE_ADMINISTRATION_TOOLS.code to REMOTE_ADMINISTRATION_TOOLS.getResponseModel(),
            APPLICATION_TAMPERING.code to APPLICATION_TAMPERING.getResponseModel(),
            RUNTIME_TAMPERING.code to RUNTIME_TAMPERING.getResponseModel(),
            LIBRARIES_TAMPERING.code to LIBRARIES_TAMPERING.getResponseModel(),
            MALWARE.code to MALWARE.getResponseModel()
        )
    }
}
