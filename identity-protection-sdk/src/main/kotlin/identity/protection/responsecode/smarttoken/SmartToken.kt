package identity.protection.responsecode.smarttoken

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel

/**
 * A class that provide a Smart Token response code
 */
class SmartToken {

    /**
     * Collection of Smart Token response code
     */
    companion object {
        val responses: Map<Long, ResponseModel> = SmartTokenResponse.responses
    }
}
