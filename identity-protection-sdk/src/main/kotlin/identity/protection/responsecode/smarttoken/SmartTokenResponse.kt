package identity.protection.responsecode.smarttoken

import identity.protection.responsecode.IdentityProtectionResponse.ResponseModel
import identity.protection.responsecode.Response

/**
 * Collection of Smart Token response code
 */
enum class SmartTokenResponse(val code: Long) : Response {

    /**
     * Code: 40200
     *
     * Type: VTAP_SETUP_SUCCESS
     *
     * Cause(s):
     * - Successfully set up V–OS Smart Token.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_SETUP_SUCCESS(40200) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SETUP_SUCCESS.code,
            type = "VTAP_SETUP_SUCCESS",
            causes = "Successfully set up V–OS Smart Token.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40202
     *
     * Type: VGUARD_FAILED
     *
     * Cause(s):
     *
     * The V–OS App Protection setup operation failed
     * due to one or more of the following reasons:
     * - Missing/corrupted profile and/or signature.
     * - Profile and/or signature decryption error.
     * - Invalid signature.
     * - Missing native library (for SDK version < 3.6.1.6).
     * - Unsatisfied link error in .so files (for SDK version < 3.6.1.6).
     * - Emulator detected.
     *
     * Recommendation(s):
     * - Make sure that the profile, firmware, and signature
     * files are correct and in the /assets/ folder of your app project.
     * - Make sure that profile is downloaded from the correct customer ID
     * in V-OS App Protection Server.
     * - If emulator is running, set VGuardFactory.debug = true.
     *
     * Note: This error should only happen during development.
     */
    VGUARD_FAILED(40202) {
        override fun getResponseModel() = ResponseModel(
            code = VGUARD_FAILED.code,
            type = "VGUARD_FAILED",
            causes = "The V–OS App Protection setup operation failed " +
                    "due to one or more of the following reasons:\n" +
                    "- Missing/corrupted profile and/or signature.\n" +
                    "- Profile and/or signature decryption error.\n" +
                    "- Invalid signature.\n" +
                    "- Missing native library (for SDK version < 3.6.1.6).\n" +
                    "- Unsatisfied link error in .so files (for SDK version < 3.6.1.6).\n" +
                    "- Emulator detected.",
            recommendations = "- Make sure that the profile, firmware, and signature " +
                    "files are correct and in the /assets/ folder of your app project.\n" +
                    "- Make sure that profile is downloaded from the correct customer ID " +
                    "in V-OS App Protection Server.\n" +
                    "- If emulator is running, set VGuardFactory.debug = true.\n" +
                    "Note: This error should only happen during development."
        )
    },

    /**
     * Code: 40203
     *
     * Type: VOS_FAILED
     *
     * Cause(s):
     * V–OS failed to start due to one or more of the following reasons:
     * - Missing/corrupted firmware and/or license.
     * - Signer certificate in license mismatch.
     * - Package name in license mismatch.
     * - Error in license verification.
     *
     * Recommendation(s):
     * - Make sure that the firmware, and license
     * files are correct and in the /assets/ folder of your app project.
     * - Make sure that the app is signed by the correct private key whose
     * public key is used to generate the license.
     * - Make sure that the app package name/bundle ID matches with the one
     * given for license generation.
     *
     * Note: This error should only happen during development.
     */
    VOS_FAILED(40203) {
        override fun getResponseModel() = ResponseModel(
            code = VOS_FAILED.code,
            type = "VOS_FAILED",
            causes = "V–OS failed to start due to one or more of the following reasons:\n" +
                    "- Missing/corrupted firmware and/or license.\n" +
                    "- Signer certificate in license mismatch.\n" +
                    "- Package name in license mismatch.\n" +
                    "- Error in license verification.",
            recommendations = "- Make sure that the firmware, and license " +
                    "files are correct and in the /assets/ folder of your app project.\n" +
                    "- Make sure that the app is signed by the correct private key whose " +
                    "public key is used to generate the license.\n" +
                    "- Make sure that the app package name/bundle ID matches with the one " +
                    "given for license generation.\n" +
                    "Note: This error should only happen during development."
        )
    },

    /**
     * Code: 40204
     *
     * Type: VTAP_TOKEN_LOAD_FAILED
     *
     * Cause(s):
     *
     * Smart tokens cannot be loaded due to one or more of the following reasons:
     * - V–OS is locked due to security issues.
     * - Failed to load V–OS Smart Token TA because
     * vtapta.bin file is missing/corrupted in the app files directory.
     * - Failed to load V–OS Smart Token TA because
     * the TA has already been loaded.
     * - TA secure store error.
     *
     * Recommendation(s):
     * - Make sure that V-OS is not locked due to voscodedesign.vky file is not mismatch.
     * - Make sure that the vtap.bin file is available in the app files directory.
     * - Make sure that the DFP# is not changed in the device.
     *
     * Note: This error should only happen during development.
     */
    VTAP_TOKEN_LOAD_FAILED(40204) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_TOKEN_LOAD_FAILED.code,
            type = "VTAP_TOKEN_LOAD_FAILED",
            causes = "Smart tokens cannot be loaded due to one or more of the following reasons:\n" +
                    "- V–OS is locked due to security issues.\n" +
                    "- Failed to load V–OS Smart Token TA because " +
                    "vtapta.bin file is missing/corrupted in the app files directory.\n" +
                    "- Failed to load V–OS Smart Token TA because " +
                    "the TA has already been loaded.\n" +
                    "- TA secure store error.\n",
            recommendations = "- - Make sure that V-OS is not locked due to voscodedesign.vky " +
                    "file is not mismatch.\n" +
                    "- Make sure that the vtap.bin file is available in " +
                    "the app files directory.\n" +
                    "- Make sure that the DFP# is not changed in the device.\n" +
                    "Note: This error should only happen during development."
        )
    },

    /**
     * Code: 40205
     *
     * Type: VTAP_SETUP_FAILED
     *
     * Cause(s):
     *
     * The V–OS Smart Token setup operation failed due to one or more of the following reasons:
     * - Failed to set trusted time server URL because V–OS is locked due to threats detected
     * by V–OS App Protection.
     * - Security issue such as voscodedesign.vky file mismatch.
     *
     * Recommendation(s):
     * - Make sure that there is no threat on the device.
     * - Make sure that the device is not locked by verifying the troubleshooting ID of the device
     * in V–OS App Protection Server. If so, unlock the device.
     *
     * Note: voscodedesign.vky file is only needed for Android.
     */
    VTAP_SETUP_FAILED(40205) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SETUP_FAILED.code,
            type = "VTAP_SETUP_FAILED",
            causes = "The V–OS Smart Token setup operation failed due to one or more of " +
                    "the following reasons:\n" +
                    "- Failed to set trusted time server URL because V–OS is locked due to " +
                    "threats detected by V–OS App Protection.\n" +
                    "- Security issue such as voscodedesign.vky file mismatch.\n",
            recommendations = "- Make sure that there is no threat on the device.\n" +
                    "- Make sure that the device is not locked by verifying the " +
                    "troubleshooting ID of the device in V–OS App Protection Server. " +
                    "If so, unlock the device.\n" +
                    "Note: voscodedesign.vky file is only needed for Android."
        )
    },

    /**
     * Code: 40206
     *
     * Type: VGUARD_PERMISSION_NOT_GRANTED
     *
     * Cause(s):
     * - The permissions required by V–OS App Protection is not granted.
     *
     * Recommendation(s):
     * - Make sure that the permissions are granted.
     * - Make sure that
     *
     * ```kotlin
     * <activity
     *      android:name="com.vkey.android.support.permission.VGuardPermissionActivity"
     *      android:theme="@android:style/Theme.Translucent.NoTitleBar" />
     * ```
     *
     * is added to the AndroidManifest.xml file
     */
    VGUARD_PERMISSION_NOT_GRANTED(40206) {
        override fun getResponseModel() = ResponseModel(
            code = VGUARD_PERMISSION_NOT_GRANTED.code,
            type = "VGUARD_PERMISSION_NOT_GRANTED",
            causes = "The permissions required by V–OS App Protection is not granted.",
            recommendations = "- Make sure that the activity " +
                    "com.vkey.android.support.permission.VGuardPermissionActivity is " +
                    "included in the AndroidManifest.xml file."
        )
    },

    /**
     * Code: 40207
     *
     * Type: VOS_FAILED_WITH_TRUST_STORAGE_ERROR
     *
     * Cause(s):
     * - NONE.
     *
     * Recommendation(s):
     * - NONE.
     */
    VOS_FAILED_WITH_TRUST_STORAGE_ERROR(40207) {
        override fun getResponseModel() = ResponseModel(
            code = VOS_FAILED_WITH_TRUST_STORAGE_ERROR.code,
            type = "VOS_FAILED_WITH_TRUST_STORAGE_ERROR",
            causes = "NONE.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40300
     *
     * Type: VTAP_WHITE_LISTED_DEVICE
     *
     * Cause(s):
     * - The current device model is a whitelisted device model.
     * You may allow the user to continue using the app.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_WHITE_LISTED_DEVICE(40300) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_WHITE_LISTED_DEVICE.code,
            type = "VTAP_WHlIE_LISTED_DEVICE",
            causes = "The current device model is a whitelisted device model. " +
                    "You may allow the user to continue using the app.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40301
     *
     * Type: VTAP_BLACK_LISTED_DEVICE
     *
     * Cause(s):
     * - The current device model is blacklisted.
     * You can verify this by comparing the device model with the list of blacklisted device models
     * in the V–OS Smart Token database.
     * If you receive this error, you should not allow the user to continue using the app on
     * the device.
     * If you wish to allow the user to continue using the app,
     * unexpected behavior on V–OS Smart Token may occur.
     * You should inform the user that the device model is not supported.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_BLACK_LISTED_DEVICE (40301) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_BLACK_LISTED_DEVICE.code,
            type = "VTAP_BLACK_LISTED_DEVICE",
            causes = "The current device model is blacklisted. " +
                    "You can verify this by comparing the device model with the list of blacklisted " +
                    "device models in the V–OS Smart Token database. " +
                    "If you receive this error, " +
                    "you should not allow the user to continue using the app on the device. " +
                    "If you wish to allow the user to continue using the app, " +
                    "unexpected behavior on V–OS Smart Token may occur. " +
                    "You should inform the user that the device model is not supported.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40302
     *
     * Type: VTAP_GREY_LISTED_DEVICE
     *
     * Cause(s):
     * - The current device model is neither a blacklisted nor whitelisted.
     * You need to call the sendDeviceInfo API so that the device information can be
     * captured and analyzed.
     * You may allow the user to continue using the device
     * provided you have informed the user about the risk.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_GREY_LISTED_DEVICE (40302) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_GREY_LISTED_DEVICE.code,
            type = "VTAP_GREY_LISTED_DEVICE",
            causes = "The current device model is neither a blacklisted nor whitelisted. " +
                    "You need to call the sendDeviceInfo API so that the device information can be " +
                    "captured and analyzed. " +
                    "You may allow the user to continue using the device " +
                    "provided you have informed the user about the risk.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40303
     *
     * Type: VTAP_GET_DEVICE_LIST_FAILED
     *
     * Cause(s):
     * The get device list web service call failed due to one or more of the following reasons:
     *
     * - V–OS Smart Token Server is down or not responding
     * (HTTP 500 or any other HTTP response code other than HTTP 200).
     * - Client authentication fail
     * (not able to get customer ID when V–OS is not running).
     *
     * Recommendation(s):
     * - Make sure that the V–OS Smart Token Server is running and responsive.
     * - Make sure that the setupVTap API call was successful.
     */
    VTAP_GET_DEVICE_LIST_FAILED(40303) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_GET_DEVICE_LIST_FAILED.code,
            type = "VTAP_GET_DEVICE_LIST_FAILED",
            causes = "The get device list web service call failed due to one or more " +
                    "of the following reasons:\n" +
                    "- V–OS Smart Token Server is down or not responding " +
                    "(HTTP 500 or any other HTTP response code other than HTTP 200).\n" +
                    "- Client authentication fail" +
                    "(not able to get customer ID when V–OS is not running).\n",
            recommendations = "- Make sure that the V–OS Smart Token Server is " +
                    "running and responsive.\n" +
                    "- Make sure that the setupVTap API call was successful."
        )
    },

    /**
     * Code: 40400
     *
     * Type: VTAP_SEND_DEVICE_INFO_SUCCESS
     *
     * Cause(s):
     * - The device information (platform, model, OS) along with customer ID, DFP#, greylist,
     * and status has been sent successfully.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_SEND_DEVICE_INFO_SUCCESS(40400) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SEND_DEVICE_INFO_SUCCESS.code,
            type = "VTAP_SEND_DEVICE_INFO_SUCCESS",
            causes = "The device information (platform, model, OS) along with customer ID, " +
                    "DFP#, greylist, and status has been sent successfully.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40401
     *
     * Type: VTAP_SEND_DEVICE_INFO_FAILED
     *
     * Cause(s):
     *
     * The Send Device Information web service call failed due to one or
     * more of the following reasons:
     * - V–OS Smart Token Server is down or not responding
     * (HTTP 500 or any other HTTP response code other than HTTP 200).
     * - Client authentication fail
     * (not able to get customer ID when V–OS is not running).
     *
     * Recommendation(s):
     * - Make sure that the V–OS Smart Token Server is running and responsive.
     * - Make sure that the setupVTap API call was successful.
     */
    VTAP_SEND_DEVICE_INFO_FAILED(40401) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SEND_DEVICE_INFO_FAILED.code,
            type = "VTAP_SEND_DEVICE_INFO_FAILED",
            causes = "The Send Device Information web service call failed due to one or \n" +
                    "more of the following reasons:\n" +
                    "- V–OS Smart Token Server is down or not responding " +
                    "(HTTP 500 or any other HTTP response code other than HTTP 200)." +
                    "- Client authentication fail" +
                    "(not able to get customer ID when V–OS is not running).",
            recommendations = "- Make sure that the V–OS Smart Token Server is " +
                    "running and responsive.\n" +
                    "- Make sure that the setupVTap API call was successful."
        )
    },

    /**
     * Code: 40502
     *
     * Type: VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS
     *
     * Cause(s):
     * - The troubleshooting log (with DFP# and JSON data generated)
     * has been sent to the server successfully and received the HTTP 200 response.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS(40502) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS.code,
            type = "VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS",
            causes = "The troubleshooting log (with DFP# and JSON data generated) " +
                    "has been sent to the server successfully and received the HTTP 200 response.",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40503
     *
     * Type: VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED
     *
     * Cause(s):
     *
     * The send troubleshooting log web service call failed due to one or
     * more of the following reasons:
     * - Problem occurred during DFP# generation or DFP# is missing.
     * - Problem occurred during JSON data generation.
     * - POST request error in server.
     * - Did not receive HTTP 200 response from server
     *
     * Recommendation(s):
     * - Check DFP#.
     * - Check server connection.
     */
    VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED(40503) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED.code,
            type = "VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED",
            causes = "The send troubleshooting log web service call failed due to one or " +
                    "more of the following reasons:\n" +
                    "- Problem occurred during DFP# generation or DFP# is missing.\n" +
                    "- Problem occurred during JSON data generation.\n" +
                    "- POST request error in server.\n" +
                    "- Did not receive HTTP 200 response from server.",
            recommendations = "- Check DFP#.\n" +
                    "- Check server connection."
        )
    },

    /**
     * Code: 40600
     *
     * Type: VTAP_TOKEN_DOWNLOAD_SUCCESS
     *
     * Cause(s):
     *
     * The token firmware is downloaded and loaded successfully. This operation includes:
     * - Downloaded token firmware from V-OS Provisioning Server.
     * - Loaded the downloaded firmware using loadTokenFirmware API.
     * - Sent acknowledgement to V-OS Provisioning Server.
     *
     * Recommendation(s):
     * - NONE.
     */
    VTAP_TOKEN_DOWNLOAD_SUCCESS(40600) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_TOKEN_DOWNLOAD_SUCCESS.code,
            type = "VTAP_TOKEN_DOWNLOAD_SUCCESS",
            causes = "The token firmware is downloaded and loaded successfully. " +
                    "This operation includes:\n" +
                    "- Downloaded token firmware from V-OS Provisioning Server.\n" +
                    "- Loaded the downloaded firmware using loadTokenFirmware API.\n" +
                    "- Sent acknowledgement to V-OS Provisioning Server.\n",
            recommendations = "NONE."
        )
    },

    /**
     * Code: 40601
     *
     * Type: VTAP_ERROR_INVALID_PROVISIONING_INFO
     *
     * Cause(s):
     *
     * The provisioning info obtained is invalid due to one or more of the following reasons:
     * - The provisioning info array size is not two (token serial and APIN).
     * - The provisioning info array is NULL or empty.
     *
     * Recommendation(s):
     * - Make sure that the input parameters are correct.
     */
    VTAP_ERROR_INVALID_PROVISIONING_INFO(40601) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_ERROR_INVALID_PROVISIONING_INFO.code,
            type = "VTAP_ERROR_INVALID_PROVISIONING_INFO",
            causes = "The provisioning info obtained is invalid due to one or " +
                    "more of the following reasons:\n" +
                    "- The provisioning info array size is not two (token serial and APIN).\n" +
                    "- The provisioning info array is NULL or empty.\n",
            recommendations = "Make sure that the input parameters are correct."
        )
    },

    /**
     * Code: 40602
     *
     * Type: VTAP_ERROR_INVALID_TOKEN_SERIAL
     *
     * Cause(s):
     *
     * The token serial in the provisioning info obtained is
     * invalid due to one or more of the following reasons:
     * - The token serial is NULL or empty.
     * - The token serial length is not equal to 10.
     *
     * Recommendation(s):
     * - Make sure that the input parameters are correct.
     */
    VTAP_ERROR_INVALID_TOKEN_SERIAL(40602) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_ERROR_INVALID_TOKEN_SERIAL.code,
            type = "VTAP_ERROR_INVALID_TOKEN_SERIAL",
            causes = "The token serial in the provisioning info obtained is " +
                    "invalid due to one or more of the following reasons:\n" +
                    "- The token serial is NULL or empty.\n" +
                    "- The token serial length is not equal to 10.",
            recommendations = "Make sure that the input parameters are correct."
        )
    },

    /**
     * Code: 40603
     *
     * Type: VTAP_ERROR_INVALID_ACTIVATION_PIN
     *
     * Cause(s):
     *
     * The activation PIN (APIN) in the provisioning info obtained is
     * invalid due to one or more of the following reasons:
     * - The APIN is NULL or empty.
     * - The APIN length is not equal to 16.
     *
     * Recommendation(s):
     * - Make sure that the input parameters are correct.
     */
    VTAP_ERROR_INVALID_ACTIVATION_PIN(40603) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_ERROR_INVALID_ACTIVATION_PIN.code,
            type = "VTAP_ERROR_INVALID_ACTIVATION_PIN",
            causes = "The activation PIN (APIN) in the provisioning info obtained is " +
                    "invalid due to one or more of the following reasons:\n" +
                    "- The APIN is NULL or empty.\n" +
                    "- The APIN length is not equal to 16.",
            recommendations = "Make sure that the input parameters are correct."
        )
    },

    /**
     * Code: 40604
     *
     * Type: VTAP_TOKEN_DOWNLOAD_FAILED
     *
     * Cause(s):
     *
     * The token firmware download operation failed due to one or
     * more of the following reasons:
     * - V–OS Provisioning Server is down
     * (HTTP 500 response or no response).
     * - Client authentication failed
     * (not able to get customer ID when V–OS is not running).
     *
     * Recommendation(s):
     * - Check server connection.
     * - Make sure that the setupVTap API call was successful.
     */
    VTAP_TOKEN_DOWNLOAD_FAILED(40604) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_TOKEN_DOWNLOAD_FAILED.code,
            type = "VTAP_TOKEN_DOWNLOAD_FAILED",
            causes = "The token firmware download operation failed due to one or " +
                    "more of the following reasons:\n" +
                    "- V–OS Provisioning Server is down " +
                    "(HTTP 500 response or no response).\n" +
                    "- Client authentication failed " +
                    "(not able to get customer ID when V–OS is not running).",
            recommendations = "- Check server connection.\n" +
                    "- Make sure that the setupVTap API call was successful."
        )
    },

    /**
     * Code: 40605
     *
     * Type: VTAP_TOKEN_NOT_FOUND
     *
     * Cause(s):
     *
     * The token firmware download operation failed due to one or
     * more of the following reasons:
     * - Received HTTP 204 from V–OS Provisioning Server.
     * - The firmware requested is not found or already provisioned.
     * - Invalid token serial.
     *
     * Recommendation(s):
     * - Make sure that the token exists in database of V–OS Provisioning Server.
     * - Make sure that the token is not already provisioned.
     * - Make sure that the token serial is correct.
     */
    VTAP_TOKEN_NOT_FOUND(40605) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_TOKEN_NOT_FOUND.code,
            type = "VTAP_TOKEN_NOT_FOUND",
            causes = "The token firmware download operation failed due to one or " +
                    "more of the following reasons:\n" +
                    "- Received HTTP 204 from V–OS Provisioning Server.\n" +
                    "- The firmware requested is not found or already provisioned.\n" +
                    "- Invalid token serial.",
            recommendations = "- Make sure that the token exists in database " +
                    "of V–OS Provisioning Server.\n" +
                    "- Make sure that the token is not already provisioned.\n" +
                    "- Make sure that the token serial is correct."
        )
    },

    /**
     * Code: 40606
     *
     * Type: VTAP_TOKEN_BAD_REQUEST
     *
     * Cause(s):
     *
     * The token firmware download operation failed due to one or
     * more of the following reasons:
     * - Received HTTP 400 from V–OS Provisioning.
     * - Server Bad request.
     * - Invalid DFP#.
     * - Invalid customer ID.
     *
     * Recommendation(s):
     * - Make sure that setupVTap API call was successful and
     * returned valid DFP# and Customer ID.
     * - Check the database of V–OS Provisioning Server and
     * logs for the availability of the customer ID and firmware requested.
     */
    VTAP_TOKEN_BAD_REQUEST(40606) {
        override fun getResponseModel() = ResponseModel(
            code = VTAP_TOKEN_BAD_REQUEST.code,
            type = "VTAP_TOKEN_BAD_REQUEST",
            causes = "The token firmware download operation failed due to one or " +
                    "more of the following reasons:\n" +
                    "- Received HTTP 400 from V–OS Provisioning.\n" +
                    "- Server Bad request.\n" +
                    "- Invalid DFP#.\n" +
                    "- Invalid customer ID.",
            recommendations = "- Make sure that setupVTap AI call was successful and" +
                    "returned valid DFP# and Customer ID.\n" +
                    "- Check the database of V–OS Provisioning Server and " +
                    "logs for the availability of the customer ID and firmware requested."
        )
    };

    companion object {
        val responses: Map<Long, ResponseModel> = mapOf(
            VTAP_SETUP_SUCCESS.code to VTAP_SETUP_SUCCESS.getResponseModel(),
            VGUARD_FAILED.code to VGUARD_FAILED.getResponseModel(),
            VOS_FAILED.code to VOS_FAILED.getResponseModel(),
            VTAP_TOKEN_LOAD_FAILED.code to VTAP_TOKEN_LOAD_FAILED.getResponseModel(),
            VTAP_SETUP_FAILED.code to VTAP_SETUP_FAILED.getResponseModel(),
            VGUARD_PERMISSION_NOT_GRANTED.code to VGUARD_PERMISSION_NOT_GRANTED.getResponseModel(),
            VOS_FAILED_WITH_TRUST_STORAGE_ERROR.code to VOS_FAILED_WITH_TRUST_STORAGE_ERROR.getResponseModel(),
            VTAP_WHITE_LISTED_DEVICE.code to VTAP_WHITE_LISTED_DEVICE.getResponseModel(),
            VTAP_BLACK_LISTED_DEVICE.code to VTAP_BLACK_LISTED_DEVICE.getResponseModel(),
            VTAP_GREY_LISTED_DEVICE.code to VTAP_GREY_LISTED_DEVICE.getResponseModel(),
            VTAP_GET_DEVICE_LIST_FAILED.code to VTAP_GET_DEVICE_LIST_FAILED.getResponseModel(),
            VTAP_SEND_DEVICE_INFO_SUCCESS.code to VTAP_SEND_DEVICE_INFO_SUCCESS.getResponseModel(),
            VTAP_SEND_DEVICE_INFO_FAILED.code to VTAP_SEND_DEVICE_INFO_FAILED.getResponseModel(),
            VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS.code to VTAP_SEND_TROUBLESHOOTING_LOGS_SUCCESS.getResponseModel(),
            VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED.code to VTAP_SEND_TROUBLESHOOTING_LOGS_FAILED.getResponseModel(),
            VTAP_TOKEN_DOWNLOAD_SUCCESS.code to VTAP_TOKEN_DOWNLOAD_SUCCESS.getResponseModel(),
            VTAP_ERROR_INVALID_PROVISIONING_INFO.code to VTAP_ERROR_INVALID_PROVISIONING_INFO.getResponseModel(),
            VTAP_ERROR_INVALID_TOKEN_SERIAL.code to VTAP_ERROR_INVALID_TOKEN_SERIAL.getResponseModel(),
            VTAP_ERROR_INVALID_ACTIVATION_PIN.code to VTAP_ERROR_INVALID_ACTIVATION_PIN.getResponseModel(),
            VTAP_TOKEN_DOWNLOAD_FAILED.code to VTAP_TOKEN_DOWNLOAD_FAILED.getResponseModel(),
            VTAP_TOKEN_NOT_FOUND.code to VTAP_TOKEN_NOT_FOUND.getResponseModel(),
            VTAP_TOKEN_BAD_REQUEST.code to VTAP_TOKEN_BAD_REQUEST.getResponseModel()
        )
    }
}
