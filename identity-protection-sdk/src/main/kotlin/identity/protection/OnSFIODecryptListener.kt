package identity.protection

/**
 * An interface class for Secure File IO decryption APIs
 */
interface OnSFIODecryptListener<T: SFIODecrypt> {

    /**
     * [onCompleted] Called when decryption process is completed, whether it is success or
     * failed due to an exceptions
     *
     * @param sFioDecrypt [SFIODecrypt] an object to determine type of decryption
     *
     * @see [SFIODecrypt.DecryptBlockOfData]
     * @see [SFIODecrypt.DecryptString]
     * @see [SFIODecrypt.DecryptFile]
     * @see [SFIODecrypt.ReadFromDecryptedFile]
     */
    fun onCompleted(sFioDecrypt: T)
}
