package identity.protection

/**
 * An interface class for Secure File IO encryption APIs
 */
interface OnSFIOEncryptListener<T: SFIOEncrypt> {

    /**
     * [onCompleted] Called when encryption process is completed, whether it is success or
     * failed due to an exceptions
     *
     * @param sFioEncrypt [SFIOEncrypt] an object to determine type of encryption
     *
     * @see [SFIOEncrypt.EncryptBlockOfData]
     * @see [SFIOEncrypt.EncryptString]
     * @see [SFIOEncrypt.EncryptData]
     * @see [SFIOEncrypt.EncryptFile]
     * @see [SFIOEncrypt.WriteToEncryptedFile]
     */
    fun onCompleted(sFioEncrypt: T)
}
