package identity.protection

import com.vkey.android.vguard.VGuardBroadcastReceiver.ACTION_SCAN_COMPLETE
import identity.protection.internal.ACTION_PROFILE_LOADED
import identity.protection.model.IdentityProtection.requestProfileChecks

/**
 * An interface class for [requestProfileChecks]
 */
interface OnRequestScanListener {

    /**
     * [onCompleted] Called when Identity Protection SDK receive an event from [ACTION_PROFILE_LOADED]
     * triggered by [requestProfileChecks]
     *
     * @param scanAnalysis [ScanAnalysis] NULL when found an exception
     * or Identity Protection SDK is disabled
     * or there is at least once scan completed triggered from [ACTION_SCAN_COMPLETE]
     */
    fun onCompleted(scanAnalysis: ScanAnalysis? = null)
}
