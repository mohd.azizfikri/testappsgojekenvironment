package identity.protection.model

import android.app.Application
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import identity.protection.HashType
import identity.protection.OnProtectionAnalyzerListener
import identity.protection.OnRequestScanListener
import identity.protection.ScanAnalysis
import identity.protection.log.BlackBoxLog
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Ignore
@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, sdk = [Build.VERSION_CODES.P])
class IdentityProtectionTest {

    val context: Application = ApplicationProvider.getApplicationContext<Application>()
    val config = IdentityProtection.config.copy(
        isEnabled = true,
        isDebuggable = true,
        strictMode = false,
        virtualSpaceDetection = false,
        timeOutInMillis = 10_000L,
        criticalThreats = emptyList(),
        cryptoSignature = IdentityProtection.CryptoSignature(
            context = context,
            storeKey = "Gojek_CA_RSA_v1_20201116",
            hashType = HashType.SHA256
        ),
        onProtectionAnalyzerListener = object : OnProtectionAnalyzerListener {
            override fun onScanAnalyzed(scanAnalysis: ScanAnalysis) {
                BlackBoxLog.d { "HostApp -> $scanAnalysis" }
            }
        }
    )

    @Test
    fun `request profile checks`() {
        IdentityProtection.config = config

        IdentityProtection.requestProfileChecks(
            context,
            object : OnRequestScanListener {
                /**
                 * [onCompleted] Called when Identity Protection SDK receive an event from [ACTION_SCAN_COMPLETE]
                 * triggered by [requestScan]
                 *
                 * @param scanAnalysis [ScanAnalysis] NULL when found an exception
                 * or Identity Protection SDK is disabled
                 * or there is at least once scan completed triggered from [ACTION_SCAN_COMPLETE]
                 */
                override fun onCompleted(scanAnalysis: ScanAnalysis?) {

                }
            }
        )
    }
}