import plugin.AndroidLibraryConfigurationPlugin

plugins {
    id("com.android.library")
    kotlin("android")
}

apply<AndroidLibraryConfigurationPlugin>()
apply("$rootDir/gradle/publish-artifact-task.gradle")
//apply("$rootDir/gradle/script-ext.gradle")

ext {
    set("name", sdkName())
    set("publish", true)
    set("version", /*ext.get("gitVersionName")*/"")
}

android {
    buildTypes {
        getByName("debug") {
            isTestCoverageEnabled = true
            isDebuggable = true
        }

        getByName("release") {
            isTestCoverageEnabled = false
            isDebuggable = false
        }
    }

    defaultConfig {
        ndk {
            abiFilters.addAll(
                mutableListOf(
                    "x86",
                    "armeabi-v7a",
                    "arm64-v8a",
                    "x86_64"
                )
            )
        }
    }

    lintOptions {
        isAbortOnError = false
    }

    testOptions {
        unitTests.all {
            it.failFast = false
            it.jvmArgs("-noverify")
        }

        unitTests {
            isIncludeAndroidResources = true
        }
    }
}

dependencies {
    implementation(deps.kotlin.stdlib.core)
    implementation(deps.android.gson)
    implementation(deps.android.keepSafe.getKeepSafe)

    compileOnly(files("libs/protection.aar"))
    compileOnly(files("libs/cryptota.aar"))
    compileOnly(files("libs/securefileio.aar"))
    compileOnly(files("libs/processor.aar"))



//    if (isDebug()) {
//        println("Adding debug dependencies vkey")
//        deps.vkey.debug.list.forEach { compileOnly(files(it)) }
//    }
//    else {
//        println("Adding release dependencies vkey")
//        deps.vkey.release.list.forEach { implementation(it) }
//    }




    compileOnly(deps.android.support.annotation)

    testImplementation(deps.android.test.junit)
    testImplementation(deps.android.test.core)
    testImplementation(deps.android.test.roboelectric)

}

/**
 * A convenience function to determine SDK variants.
 *
 * **Example:**
 * ```
 * // for assemble and publish release variant just omit `-PisDebug`
 * // so that we can apply like so
 *
 * ./gradlew :identity-protection-sdk:assembleRelease &&
 * ./gradlew :identity-protection-sdk:publishToMavenLocal
 *
 * // otherwise for debug we need to add `-PisDebug`
 *
 * ./gradlew :identity-protection-sdk:assembleRelease -PisDebug &&
 * ./gradlew :identity-protection-sdk:publishToMavenLocal -PisDebug
 * ```
 *
 * @return true if -PisDebug exist otherwise false
 */
fun isDebug(): Boolean {
    return project.ext.has("isDebug")
}

/**
 * Generate SDK aar name.
 *
 * @return "identity-protection" if no `-PisDebug` otherwise "identity-protection-dev"
 */
fun sdkName(): String {
    val name = "identity-protection"
    return if (isDebug()) "$name-dev" else name
}
