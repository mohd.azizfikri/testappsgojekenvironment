/*
 * Copyright 2019, GO-JEK Tech (http://gojek.tech)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import plugin.DetektConfigurationPlugin
import plugin.isAndroidAppProject
import plugin.isAndroidProject
import plugin.isJavaProject
import plugin.isKotlinProject

plugins {
    id(ScriptPlugins.infrastructure)
    id("org.jetbrains.dokka") version "1.4.0"

    // this is for firebase test lab
    // disabling at the moment
    // id("com.gojek.ftl")
}

apply(plugin = "binary-compatibility-validator")
apply("${rootDir}/buildSrc/hook/hook.gradle")

buildscript {
    repositories {
        google()
        jcenter()
        maven { setUrl("https://maven-central-asia.storage-download.googleapis.com/repos/central/data") }
        maven { setUrl("https://plugins.gradle.org/m2/") }
        maven { setUrl("http://artifactory-gojek.golabs.io/artifactory/gojek-jars") }
        maven { setUrl("https://kotlin.bintray.com/kotlinx") }
        mavenCentral()
        mavenLocal()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:${versions.agp}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${versions.kotlin}")
        classpath("org.jfrog.buildinfo:build-info-extractor-gradle:${versions.jfrogBuildInfoExtractor}")
        classpath("com.android.tools.build.jetifier:jetifier-processor:${versions.jetifierProcessor}")
        classpath("androidx.benchmark:benchmark-gradle-plugin:${versions.benchmarkGradlePlugin}")
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:${versions.dokkaGradlePlugin}")
        classpath("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${versions.detekt}")
        classpath("org.jetbrains.kotlinx:binary-compatibility-validator:0.2.3")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { setUrl("https://maven-central-asia.storage-download.googleapis.com/repos/central/data") }
        maven { setUrl("https://plugins.gradle.org/m2/") }
        maven { setUrl("http://artifactory-gojek.golabs.io/artifactory/gojek-jars") }
        mavenCentral()
        mavenLocal()
    }
}

tasks.dokkaHtmlMultiModule.configure {
    outputDirectory.set(rootDir.resolve("docs/api"))
    documentationFileName.set("README.md")
}

subprojects {
    apply<DetektConfigurationPlugin>()
    apply(plugin = "org.jetbrains.dokka")

    if (isAndroidAppProject()) {
        println("the ${project.name} is android app module")
    } else if (isAndroidProject()) {
        println("the ${project.name} is android module")
    } else if (isJavaProject()) {
        println("the ${project.name} is java module")
    } else if (isKotlinProject()) {
        println("the ${project.name} is kotlin module")
    } else {
//        throw IllegalArgumentException("we're not found type for ${project.name}")
    }
}

val clean by tasks.creating(Delete::class) {
    delete(rootProject.buildDir)
    delete("${rootDir}/buildSrc/build")
    delete("${rootDir}/identity-protection-demo-consumer-app/build")
    delete("${rootDir}/identity-protection-sdk/build")
    delete("${rootDir}/identity-protection-no-op-sdk/build")
}
