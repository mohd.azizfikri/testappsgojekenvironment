# PR title should start with either WIP or [Enhancement] or [Feature] or [Refactoring] or [Bug]
def checkForTitle
  failure "PR title doesn't follow the allowed condition.\
  \nSample Example:\
  \n`[Enhancement]:[DEVX-123] : PR Title`\
  \n`[Feature]:[DEVX-123] : PR Title`\
  \n`[Refactoring]:[DEVX-123] : PR Title`\
  \n`[Bug]:[DEVX-123] : PR Title`" if !gitlab.mr_title.start_with?('WIP','[Enhancement]:[DEVX-','[Feature]:[DEVX-','[Refactoring]:[DEVX-','[Bug]:[DEVX-')
end

# Make it more obvious that there are no assignee
def checkForAssignee
  warn "This MR does not have any assignees yet." unless gitlab.mr_json["assignee"]
end

# PR description shouldn't be empty
def checkForDescription
  failure "Please provide a summary in the Merge Request description" if gitlab.mr_body.length < 10
end

# PR labels shouldn't be empty
def checkForLabels
  failure "Please add a valid label to this Merge Request" if gitlab.mr_labels.empty?
end

checkForTitle()
checkForAssignee()
checkForDescription()
checkForLabels()