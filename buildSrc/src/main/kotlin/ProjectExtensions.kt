import org.gradle.api.Project
import org.gradle.api.logging.LogLevel

fun Project.print(text: String) {
    project.logger.log(
        LogLevel.WARN,
        """${project.name}
           $text 
        """.trimIndent()
    )
}