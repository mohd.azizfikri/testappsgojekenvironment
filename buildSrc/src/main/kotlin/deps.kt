/*
 * Copyright 2019, GO-JEK Tech (http://gojek.tech)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused", "ClassName")

object versions {
    const val jacoco = "0.8.4"
    const val detekt = "1.2.0"

    const val kotlin = "1.5.10"
    const val agp = "4.1.2"
    const val jetifierProcessor = "1.0.0-beta08"
    const val jfrogBuildInfoExtractor = "4.11.0"
    const val navigation = "2.1.0-rc01"
    const val coroutines = "1.3.2"
    const val lifecycle = "2.0.0"
    const val retrofit = "2.6.2"
    const val groovy = "2.5.7"
    const val benchmarkGradlePlugin = "1.0.0"
    const val dokkaGradlePlugin = "1.4.0"
    const val nodeGradlePlugin = "2.2.0"
    const val bintrayGradlePlugin = "1.8.4"
}

object BuildPlugins {
    object Versions {
        const val gradleVersion = "6.5"
    }
}

object ScriptPlugins {
    const val infrastructure = "scripts.infrastructure"
}

object deps {
    object android {
        const val gson = "com.google.code.gson:gson:2.8.6"
        const val benchmark = "androidx.benchmark:benchmark-junit4:1.0.0"

        object google {
            const val playServiceAuth = "com.google.android.gms:play-services-auth:17.0.0"
        }

        object build {
            const val compileSdkVersion = 30
            const val minSdkVersion = 21
            const val sampleMinSdkVersion = 21
            const val targetSdkVersion = 30
        }

        object lifecycle {
            const val livedata = "androidx.lifecycle:lifecycle-livedata:${versions.lifecycle}"
            const val test = "androidx.arch.core:core-testing:${versions.lifecycle}"
        }

        object support {
            const val annotation = "androidx.annotation:annotation:1.1.0"
            const val support = "com.android.support:support-v4:28.0.0"
        }

        object core {
            const val coreKtx = "androidx.core:core-ktx:1.1.0"
        }

        object firebase {
            const val config = "com.google.firebase:firebase-config:17.0.0"
        }

        object room {
            const val room = "androidx.room:room-runtime:2.2.3"
            const val roomCompiler = "androidx.room:room-compiler:2.2.3"
            const val roomTesting = "androidx.room:room-testing:2.2.3"
            const val roomKtx = "androidx.room:room-ktx:2.2.3"
        }

        object dagger {
            const val dagger = "com.google.dagger:dagger:2.25.4"
            const val compiler = "com.google.dagger:dagger-compiler:2.25.4"

            const val assisted = "com.squareup.inject:assisted-inject-annotations-dagger2:0.5.2"
            const val assistedCompiler =
                "com.squareup.inject:assisted-inject-processor-dagger2:0.5.2"
        }

        object keepSafe {
            const val getKeepSafe = "com.getkeepsafe.relinker:relinker:1.4.4"
        }

        object test {
            const val core = "androidx.test:core:1.3.0"
            const val coreKtx = "androidx.test:core-ktx:1.2.0"
            const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
            const val junit = "junit:junit:4.12"
            const val runner = "androidx.test:runner:1.2.0"
            const val roboelectric = "org.robolectric:robolectric:4.5.1"
            const val mockito = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
            const val junitExt = "androidx.test.ext:junit:1.1.1"
        }
    }

    object vkey {

//        object debug {
//            val list = listOf(
//                "identity-protection-sdk:protection",
//                "identity-protection-sdk:cryptota",
//                "identity-protection-sdk:securefileio",
//                "identity-protection-sdk:processor"
//            )
//        }

//        object debug {
//            val list = listOf(
//                "com.vkey:vkey-cryptota-dev:4.8.2.24",
//                "com.vkey:vkey-processor-dev:4.8.4.65-gojekhfx.2-SNAPSHOT",
//                "com.vkey:vkey-protection-dev:3.0.72",
//                "com.vkey:vkey-securefileio-dev:4.7.7.8",
//                "com.vkey:vkey-cuckoofilter4j:1.0.2"
//            )
//        }

//        object release {
//            val list = listOf(
//                "com.vkey:vkey-cryptota-prod:4.8.2.24",
//                "com.vkey:vkey-processor-prod:4.8.4.65-gojekhfx.2-SNAPSHOT",
//                "com.vkey:vkey-protection-prod:3.0.72",
//                "com.vkey:vkey-securefileio-prod:4.7.7.8",
//                "com.vkey:vkey-cuckoofilter4j:1.0.2"
//            )
//        }
    }

    object kotlin {
        object stdlib {
            const val core = "org.jetbrains.kotlin:kotlin-stdlib:${versions.kotlin}"
            const val jdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${versions.kotlin}"
        }

        object coroutines {
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${versions.coroutines}"
            const val android =
                "org.jetbrains.kotlinx:kotlinx-coroutines-android:${versions.coroutines}"
            const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${versions.coroutines}"
        }
    }

    object rx {
        const val java = "io.reactivex.rxjava2:rxjava:2.2.15"
        const val android = "io.reactivex.rxjava2:rxandroid:2.1.1"
    }

    object detekt {
        const val lint = "io.gitlab.arturbosch.detekt:detekt-formatting:${versions.detekt}"
        const val cli = "io.gitlab.arturbosch.detekt:detekt-cli:${versions.detekt}"
    }

    object square {
        const val okhttp = "com.squareup.okhttp3:okhttp:3.12.1"
        const val retrofit = "com.squareup.retrofit2:retrofit:2.6.2"
        const val mockWebserver = "com.squareup.okhttp3:mockwebserver:3.12.1"
        const val retrofitGson = "com.squareup.retrofit2:converter-gson:2.6.1"
    }
}
