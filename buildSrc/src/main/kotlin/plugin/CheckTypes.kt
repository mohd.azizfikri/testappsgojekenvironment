package plugin

import org.gradle.api.Project

fun Project.isJavaProject(): Boolean {
    val isJava = plugins.hasPlugin("java")
    val isJavaLibrary = plugins.hasPlugin("java-library")
    val isJavaGradlePlugin = plugins.hasPlugin("java-gradle-plugin")
    return isJava || isJavaLibrary || isJavaGradlePlugin
}

fun Project.isAndroidAppProject(): Boolean {
    return plugins.hasPlugin("com.android.application")
}

fun Project.isAndroidProject(): Boolean {
    val isAndroidLibrary = plugins.hasPlugin("com.android.library")
    val isAndroidTest = plugins.hasPlugin("com.android.test")
    val isAndroidFeature = plugins.hasPlugin("com.android.feature")
    val isAndroidInstantApp = plugins.hasPlugin("com.android.instantapp")
    return isAndroidLibrary || isAndroidTest || isAndroidFeature || isAndroidInstantApp
}

fun Project.isKotlinProject(): Boolean {
    val isKotlin = plugins.hasPlugin("kotlin")
    val isKotlinPlatformCommon = plugins.hasPlugin("kotlin-platform-common")
    val isKotlinPlatformJvm = plugins.hasPlugin("kotlin-platform-jvm")
    val isKotlinPlatformJs = plugins.hasPlugin("kotlin-platform-js")
    return isKotlin || isKotlinPlatformCommon || isKotlinPlatformJvm || isKotlinPlatformJs
}