package plugin.ftl

import org.gradle.api.Project
import org.gradle.kotlin.dsl.create

open class FtlPluginDelegate() {

    fun apply(target: Project) {
        val extension = target.extensions.create<FtlGradleExtension>(
            FTL_CONFIG, target.objects
        )
    }


    companion object {
        const val FTL_CONFIG = "ftl"
    }

}

