package plugin.ftl

import com.android.build.gradle.internal.tasks.factory.dependsOn
import com.osacky.flank.gradle.FladlePluginDelegate
import com.osacky.flank.gradle.FlankGradleExtension
import com.osacky.flank.gradle.YamlConfigWriterTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.TaskProvider
import org.gradle.kotlin.dsl.apply
import org.gradle.kotlin.dsl.getByType
import org.gradle.kotlin.dsl.register
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension
import org.gradle.testing.jacoco.tasks.JacocoReport
import versions
import java.io.File

open class FtlConfigurationPlugin : Plugin<Project> {

    override fun apply(root: Project) {
        check(root.parent == null) {
            "Ftl must be applied in the root project in order to configure subprojects."
        }
        FladlePluginDelegate().apply(root)
        root.subprojects {
            FtlPluginDelegate().apply(project)
        }
        configureFladleExtension(root)
        val ftlConfigureTask = root.tasks.register("configureFtl", ConfigureFtlTask::class.java)
        val buildTargets = configureBuildTargetsTask(root)
        val configureFtlJacocoReports = configureFtlJacocoReports(root)
        ftlConfigureTask.dependsOn(buildTargets)
        root.afterEvaluate {
            root.tasks.named("printYml").configure {
                dependsOn(ftlConfigureTask)
            }
            root.tasks.withType(
                YamlConfigWriterTask::class.java
            ).configureEach {
                dependsOn(ftlConfigureTask)
            }
        }

    }

    private fun configureFtlJacocoReports(root: Project) {
        root.afterEvaluate {
            root.subprojects {
                if (project.extensions.getByType(FtlGradleExtension::class.java).enabled) {
                    val reportTask = project.createFtlJacocoReports()
                    reportTask.dependsOn(root.tasks.named("runFlank"))
                }
            }
        }
    }

    private fun configureBuildTargetsTask(root: Project): TaskProvider<Task> {
        val buildTargets = root.tasks.register("buildTargets")
        root.afterEvaluate {
            root.subprojects {
                pluginManager.withPlugin("com.android.application") {
                    buildTargets.dependsOn(
                        project.tasks.named("assembleDebug"),
                        project.tasks.named("assembleDebugAndroidTest")
                    )
                }
                pluginManager.withPlugin("com.android.library") {
                    if (project.extensions.getByType(FtlGradleExtension::class.java).enabled) {
                        buildTargets.dependsOn(
                            project.tasks.named("assembleDebugAndroidTest")
                        )
                    }
                }
            }
        }
        return buildTargets
    }

    private fun configureFladleExtension(root: Project) {
        val flankGradleExtension = root.extensions.getByType(
            FlankGradleExtension::class
        )
        with(flankGradleExtension) {
            environmentVariables = mapOf(
                "coverage" to "true",
                "coverageFilePath" to "/sdcard/coverage.ec"
            )
            projectId = "consumer-app-staging"
            serviceAccountCredentials.set(
                root.layout.projectDirectory.file(
                    "firebase-credentials.json"
                )
            )
            useOrchestrator = true
            directoriesToPull = listOf("/sdcard/")
            filesToDownload = listOf(".*/sdcard/[^/]+\\.ec\$")
            localResultsDir.set("${root.buildDir.path}/flank/results")
            recordVideo = true
            devices = listOf(
                mapOf("model" to "sailfish", "version" to "28")
            )
            performanceMetrics = true

        }
    }

    fun Project.createFtlJacocoReports(): TaskProvider<JacocoReport> {
        apply<JacocoPlugin>()

        extensions.getByType(JacocoPluginExtension::class.java).toolVersion = versions.jacoco
        return tasks.register<JacocoReport>("createJacocoFtlReport") {
            val debugTree = fileTree("${project.buildDir}/tmp/kotlin-classes/debug")
            val mainSrc = "${project.projectDir}/src/main/kotlin"

            sourceDirectories.setFrom(files(mainSrc))
            classDirectories.setFrom(files(debugTree))

            executionData.setFrom(fileTree(rootProject.buildDir).apply {
                setIncludes(
                    setOf("flank/results/**/**/*.ec")
                )
            })
            reports {
                html.isEnabled = true
                html.destination = File("${project.buildDir}/reports/ftlJcocoTestReport.xml")
            }
        }
    }

}







