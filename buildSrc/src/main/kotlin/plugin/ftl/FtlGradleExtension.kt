package plugin.ftl

import groovy.lang.Closure
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.model.ObjectFactory
import javax.inject.Inject

open class FtlGradleExtension @Inject constructor(objects: ObjectFactory) :
    FtlConfig {

    override var enabled: Boolean = false
    val configs: NamedDomainObjectContainer<FtlConfigImpl> = objects.domainObjectContainer(
        FtlConfigImpl::class.java
    ) {
        FtlConfigImpl(
            name = it,
            enabled = enabled
        )
    }


    fun configs(closure: Closure<*>) {
        configs.configure(closure)
    }

}

data class FtlConfigImpl(
    internal var name: String,
    override var enabled: Boolean
) :
    FtlConfig

interface FtlConfig {
    var enabled: Boolean
}