package plugin.ftl

import com.android.build.gradle.AppExtension
import com.android.build.gradle.LibraryExtension
import com.osacky.flank.gradle.FlankGradleExtension
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.getByType

/**
 * Based heavily on the [com.osacky.flank.gradle.FulladlePlugin] with additional functionality.
 * - Adds the ability to opt-in for Firebase Test Lab (FTL) based testing using a gradle dsl.
 *
 * **Example:**
 * ```kotlin
 * ftl{
 *   instrumentationTestEnabled = true
 * }
 * ```
 * - Builds only those modules as a part of `runFlank` that want to support FTL tests.
 */
open class ConfigureFtlTask : DefaultTask() {

    @TaskAction
    fun ftlTask() {
        val root = project
        val flankGradleExtension = root.extensions.getByType(FlankGradleExtension::class.java)
        root.subprojects(
            object : Action<Project> {
                override fun execute(sub: Project) {
                    with(sub) {
                        pluginManager.withPlugin(
                            "com.android.application"
                        ) {

                            val appExtension = extensions.getByType<AppExtension>()
                            // Only configure the first test variant per module.
                            // Does anyone test morse than one variant per module?
                            var addedTestsForModule = false
                            print("running configuration for modules")
                            // TODO deal with ignored/filtered variants
                            appExtension.testVariants.configureEach testVariant@{
                                if (addedTestsForModule) {
                                    return@testVariant
                                }
                                val appVariant = testedVariant
                                appVariant.outputs.configureEach app@{

                                    this@testVariant.outputs.configureEach test@{
                                        // TODO is this racy?
                                        // If the debugApk isn't yet set, let's use this one.
                                        if (!flankGradleExtension.debugApk.isPresent) {
                                            flankGradleExtension.debugApk.set(
                                                root.provider { this@app.outputFile.absolutePath })
                                        } else {
                                            // Otherwise, let's just add it to the list.
                                            flankGradleExtension.additionalTestApks.add(
                                                root.provider {
                                                    "- app: ${this@app.outputFile}"
                                                }
                                            )
                                        }
                                        // If the instrumentation apk isn't yet set, let's use this one.
                                        if (!flankGradleExtension.instrumentationApk.isPresent) {
                                            flankGradleExtension.instrumentationApk.set(
                                                root.provider { this@test.outputFile.absolutePath })
                                        } else {
                                            // Otherwise, let's just add it to the list.
                                            flankGradleExtension.additionalTestApks.add(
                                                root.provider {
                                                    "  test: ${this@test.outputFile}"
                                                }
                                            )
                                        }
                                        addedTestsForModule = true
                                        return@test
                                    }
                                }
                            }

                        }
                        pluginManager.withPlugin("com.android.library") {

                            val ftlEnabled = project.extensions.getByType(
                                FtlGradleExtension::class.java
                            ).enabled
                            if (ftlEnabled) {
                                val library = extensions.getByType<LibraryExtension>()
                                library.testVariants.configureEach {
                                    if (file("$projectDir/src/androidTest").exists()) {
                                        outputs.configureEach {
                                            flankGradleExtension.additionalTestApks.add(
                                                root.provider {
                                                    "- test: $outputFile"
                                                }
                                            )
                                        }
                                    } else {
                                        println(
                                            "ignoring variant for ${this.name} in $projectDir"
                                        )
                                    }
                                }
                            }
                        }

                    }
                }
            }
        )

    }
}

fun Project.print(text: String) {
    project.logger.log(
        LogLevel.WARN,
        """${project.name}
           $text 
        """.trimIndent()
    )
}
