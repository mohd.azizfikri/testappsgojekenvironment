/*
 * Copyright 2019, GO-JEK Tech (http://gojek.tech)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.api.tasks.testing.Test

class AndroidLibraryConfigurationPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.plugins.apply("com.android.library")
        project.plugins.apply("org.jetbrains.kotlin.android")
        project.plugins.apply("org.jetbrains.kotlin.android.extensions")
        
        project.configureAndroid()
        project.configureAndroidExperimental()
        project.configureUnitTest()
        project.configureJacocoAndroid()
        project.configureDetekt()

        project.afterEvaluate {
            // Enable compilation in a separate daemon process
            // this would make faster build
            project.tasks.withType(JavaCompile::class.java) {
                options.isIncremental = true
                options.isFork = true
            }

            // Running tests in separate process
            project.tasks.withType(Test::class.java) {
                val maxCount = gradle.startParameter.maxWorkerCount
                maxParallelForks = if (maxCount < 2) 1 else maxCount / 2
                setForkEvery(50)
            }
        }
    }
}